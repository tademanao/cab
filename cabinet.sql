-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  Dim 23 fév. 2020 à 21:46
-- Version du serveur :  5.7.24
-- Version de PHP :  7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `cabinet`
--

-- --------------------------------------------------------

--
-- Structure de la table `analyses`
--

DROP TABLE IF EXISTS `analyses`;
CREATE TABLE IF NOT EXISTS `analyses` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `libelle_analyse` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prix_analyse` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `analyses`
--

INSERT INTO `analyses` (`id`, `libelle_analyse`, `prix_analyse`, `created_at`, `updated_at`) VALUES
(1, 'gycemie', 500, '2020-02-20 21:25:13', '2020-02-20 21:25:52'),
(2, 'uree', 550, '2020-02-20 21:25:26', '2020-02-20 21:25:26');

-- --------------------------------------------------------

--
-- Structure de la table `analyse_consultation`
--

DROP TABLE IF EXISTS `analyse_consultation`;
CREATE TABLE IF NOT EXISTS `analyse_consultation` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `consultation_id` int(10) UNSIGNED NOT NULL,
  `analyse_id` int(10) UNSIGNED NOT NULL,
  `resultat` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantite_analyse` int(11) NOT NULL,
  `date_analyse` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `analyse_consultation_consultation_id_foreign` (`consultation_id`),
  KEY `analyse_consultation_analyse_id_foreign` (`analyse_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `antecedents`
--

DROP TABLE IF EXISTS `antecedents`;
CREATE TABLE IF NOT EXISTS `antecedents` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type_antecedent_id` int(10) UNSIGNED NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `patient_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `antecedents_type_antecedent_id_index` (`type_antecedent_id`),
  KEY `antecedents_patient_id_index` (`patient_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `antecedents`
--

INSERT INTO `antecedents` (`id`, `type_antecedent_id`, `description`, `patient_id`, `created_at`, `updated_at`) VALUES
(5, 2, 'bb', 1, '2020-02-22 19:23:01', '2020-02-22 19:23:01');

-- --------------------------------------------------------

--
-- Structure de la table `constantes`
--

DROP TABLE IF EXISTS `constantes`;
CREATE TABLE IF NOT EXISTS `constantes` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `temperature` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `poids` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `taille` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tension_bras_gauche` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tension_bras_droit` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `poul` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_constante` date NOT NULL,
  `patient_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `constantes_patient_id_index` (`patient_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `consultations`
--

DROP TABLE IF EXISTS `consultations`;
CREATE TABLE IF NOT EXISTS `consultations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `interrogatoire` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `examen_clinique` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `diagnostic` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `examen_paraclinique` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `traitement` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_consultation` date NOT NULL,
  `type_consultation_id` int(10) UNSIGNED NOT NULL,
  `medecin_id` int(10) UNSIGNED NOT NULL,
  `patient_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `consultations_type_consultation_id_index` (`type_consultation_id`),
  KEY `consultations_medecin_id_index` (`medecin_id`),
  KEY `consultations_patient_id_index` (`patient_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `consultations`
--

INSERT INTO `consultations` (`id`, `interrogatoire`, `examen_clinique`, `diagnostic`, `examen_paraclinique`, `traitement`, `date_consultation`, `type_consultation_id`, `medecin_id`, `patient_id`, `created_at`, `updated_at`) VALUES
(1, 'bbb', 'bbb', 'bbbb', 'bbbb', 'bbb', '2020-02-20', 8, 1, 1, '2020-02-20 20:24:24', '2020-02-20 20:37:50'),
(2, 'nnn', 'll', 'mn', 'pp', 'll', '2020-02-20', 7, 1, 1, '2020-02-20 20:41:19', '2020-02-20 20:41:19');

-- --------------------------------------------------------

--
-- Structure de la table `consultation_produit_soin`
--

DROP TABLE IF EXISTS `consultation_produit_soin`;
CREATE TABLE IF NOT EXISTS `consultation_produit_soin` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `consultation_id` int(10) UNSIGNED NOT NULL,
  `soin_id` int(10) UNSIGNED NOT NULL,
  `produit_id` int(10) UNSIGNED NOT NULL,
  `quantite_produit` int(11) NOT NULL,
  `date_soin` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `consultation_produit_soin_consultation_id_foreign` (`consultation_id`),
  KEY `consultation_produit_soin_produit_id_foreign` (`produit_id`),
  KEY `consultation_produit_soin_soin_id_foreign` (`soin_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `infirmieres`
--

DROP TABLE IF EXISTS `infirmieres`;
CREATE TABLE IF NOT EXISTS `infirmieres` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `medecins`
--

DROP TABLE IF EXISTS `medecins`;
CREATE TABLE IF NOT EXISTS `medecins` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `medecins`
--

INSERT INTO `medecins` (`id`, `created_at`, `updated_at`) VALUES
(1, '2020-02-20 18:07:32', '2020-02-20 18:07:32');

-- --------------------------------------------------------

--
-- Structure de la table `medecin_patient`
--

DROP TABLE IF EXISTS `medecin_patient`;
CREATE TABLE IF NOT EXISTS `medecin_patient` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `medecin_id` int(10) UNSIGNED NOT NULL,
  `patient_id` int(10) UNSIGNED NOT NULL,
  `objet` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `heure` time NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `medecin_patient_medecin_id_foreign` (`medecin_id`),
  KEY `medecin_patient_patient_id_foreign` (`patient_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `medicaments`
--

DROP TABLE IF EXISTS `medicaments`;
CREATE TABLE IF NOT EXISTS `medicaments` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `libelle_medicament` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `medicaments`
--

INSERT INTO `medicaments` (`id`, `libelle_medicament`, `created_at`, `updated_at`) VALUES
(1, 'paracetamol', '2020-02-20 21:54:38', '2020-02-20 21:54:38'),
(2, 'doliprane', '2020-02-20 21:54:54', '2020-02-20 21:55:45');

-- --------------------------------------------------------

--
-- Structure de la table `medicament_ordonnance`
--

DROP TABLE IF EXISTS `medicament_ordonnance`;
CREATE TABLE IF NOT EXISTS `medicament_ordonnance` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `medicament_id` int(10) UNSIGNED NOT NULL,
  `ordonnance_id` int(10) UNSIGNED NOT NULL,
  `posologie` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `medicament_ordonnance_medicament_id_foreign` (`medicament_id`),
  KEY `medicament_ordonnance_ordonnance_id_foreign` (`ordonnance_id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `medicament_ordonnance`
--

INSERT INTO `medicament_ordonnance` (`id`, `medicament_id`, `ordonnance_id`, `posologie`, `created_at`, `updated_at`) VALUES
(10, 2, 8, 'hjgkj', '2020-02-21 18:52:32', '2020-02-21 18:52:32'),
(9, 1, 8, 'ty6', '2020-02-21 18:51:39', '2020-02-21 18:51:39'),
(8, 1, 8, 'kll', '2020-02-21 18:51:12', '2020-02-21 18:51:12');

-- --------------------------------------------------------

--
-- Structure de la table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=112 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(88, '2014_10_12_000000_create_users_table', 1),
(89, '2014_10_12_100000_create_password_resets_table', 1),
(90, '2018_12_19_113111_create_medecins_table', 1),
(91, '2018_12_19_113303_create_infirmieres_table', 1),
(92, '2018_12_28_223042_create_secretaires_table', 1),
(93, '2019_01_08_081928_create_consultations_table', 1),
(94, '2019_01_08_082128_create_constantes_table', 1),
(95, '2019_01_08_082424_create_patients_table', 1),
(96, '2019_01_10_075610_create_medecin_patient_table', 1),
(97, '2020_01_20_173717_create_roles_table', 1),
(98, '2020_01_21_073004_create_permissions_table', 1),
(99, '2020_01_21_085820_create_permission_role_table', 1),
(100, '2020_02_17_085701_create_techniciens_table', 1),
(101, '2020_02_17_103720_create_antecedents_table', 1),
(102, '2020_02_19_102147_create_soins_table', 1),
(103, '2020_02_19_102450_create_produits_table', 1),
(104, '2020_02_19_102624_create_type_antecedents_table', 1),
(105, '2020_02_19_102651_create_type_consultations_table', 1),
(106, '2020_02_19_102723_create_medicaments_table', 1),
(107, '2020_02_19_102814_create_ordonnances_table', 1),
(108, '2020_02_19_102915_create_analyses_table', 1),
(109, '2020_02_19_213543_create_medicament_ordonnance', 1),
(110, '2020_02_19_214217_create_analyse_consultation', 1),
(111, '2020_02_19_214434_create_consultation_produit_soin', 1);

-- --------------------------------------------------------

--
-- Structure de la table `ordonnances`
--

DROP TABLE IF EXISTS `ordonnances`;
CREATE TABLE IF NOT EXISTS `ordonnances` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `date_ordonnance` date NOT NULL,
  `consultation_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ordonnances_consultation_id_index` (`consultation_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `ordonnances`
--

INSERT INTO `ordonnances` (`id`, `date_ordonnance`, `consultation_id`, `created_at`, `updated_at`) VALUES
(8, '2020-02-21', 2, '2020-02-21 18:51:12', '2020-02-21 18:51:12');

-- --------------------------------------------------------

--
-- Structure de la table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `patients`
--

DROP TABLE IF EXISTS `patients`;
CREATE TABLE IF NOT EXISTS `patients` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nom` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prenom` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `adresse` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sexe` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profession` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_naissance` date NOT NULL,
  `date_inclusion` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `patients`
--

INSERT INTO `patients` (`id`, `nom`, `prenom`, `adresse`, `sexe`, `phone`, `profession`, `email`, `date_naissance`, `date_inclusion`, `created_at`, `updated_at`) VALUES
(1, 'TADEMANA', 'Hombabé', 'Quartier: DJIDJOLE', 'M', '+22890325862', 'etudiant', 'tademanao92.ot@gmail.com', '2020-02-20', '2020-02-20', '2020-02-20 14:52:35', '2020-02-20 14:52:35');

-- --------------------------------------------------------

--
-- Structure de la table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
CREATE TABLE IF NOT EXISTS `permission_role` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `role_id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permission_role_role_id_foreign` (`role_id`),
  KEY `permission_role_permission_id_foreign` (`permission_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `produits`
--

DROP TABLE IF EXISTS `produits`;
CREATE TABLE IF NOT EXISTS `produits` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `libelle_produit` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prix_produit` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `produits`
--

INSERT INTO `produits` (`id`, `libelle_produit`, `prix_produit`, `created_at`, `updated_at`) VALUES
(1, 'ghh', 123, '2020-02-20 21:42:53', '2020-02-20 21:43:07');

-- --------------------------------------------------------

--
-- Structure de la table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `roles`
--

INSERT INTO `roles` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'super-admin', '2020-02-20 18:06:24', '2020-02-20 18:06:24'),
(2, 'admin', '2020-02-20 18:06:42', '2020-02-20 18:06:42');

-- --------------------------------------------------------

--
-- Structure de la table `secretaires`
--

DROP TABLE IF EXISTS `secretaires`;
CREATE TABLE IF NOT EXISTS `secretaires` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `soins`
--

DROP TABLE IF EXISTS `soins`;
CREATE TABLE IF NOT EXISTS `soins` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `libelle_soin` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prix_soin` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `soins`
--

INSERT INTO `soins` (`id`, `libelle_soin`, `description`, `prix_soin`, `created_at`, `updated_at`) VALUES
(1, 'massage', 'gg', 1000, '2020-02-20 21:01:18', '2020-02-20 21:04:09'),
(2, 'piqure', 'cool', 500, '2020-02-20 21:02:07', '2020-02-20 21:02:07');

-- --------------------------------------------------------

--
-- Structure de la table `techniciens`
--

DROP TABLE IF EXISTS `techniciens`;
CREATE TABLE IF NOT EXISTS `techniciens` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `type_antecedents`
--

DROP TABLE IF EXISTS `type_antecedents`;
CREATE TABLE IF NOT EXISTS `type_antecedents` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `libelle` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `type_antecedents`
--

INSERT INTO `type_antecedents` (`id`, `libelle`, `created_at`, `updated_at`) VALUES
(2, 'familiaux', '2020-02-20 11:52:58', '2020-02-20 11:52:58'),
(3, 'chirurgicaux', '2020-02-20 11:53:07', '2020-02-20 11:53:07'),
(4, 'autres', '2020-02-20 14:35:48', '2020-02-20 14:35:48');

-- --------------------------------------------------------

--
-- Structure de la table `type_consultations`
--

DROP TABLE IF EXISTS `type_consultations`;
CREATE TABLE IF NOT EXISTS `type_consultations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `libelle` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `prix_consultation` double NOT NULL,
  `date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `type_consultations`
--

INSERT INTO `type_consultations` (`id`, `libelle`, `description`, `prix_consultation`, `date`, `created_at`, `updated_at`) VALUES
(11, 'chirurgie 20-02-2020', 'vvvvb', 5000, '2020-02-20', '2020-02-20 19:38:46', '2020-02-20 19:38:46'),
(10, 'urgence', NULL, 0, '2020-02-20', '2020-02-20 19:38:21', '2020-02-20 19:38:21'),
(7, 'generaliste 20-02-2020', 'cccccccc', 2000, '2020-02-20', '2020-02-20 19:33:22', '2020-02-20 19:39:44'),
(8, 'ophtamologue 20-02-2020', 'vvffff', 3000, '2020-02-20', '2020-02-20 19:33:52', '2020-02-20 19:33:52'),
(9, 'suivit', NULL, 0, '2020-02-20', '2020-02-20 19:38:04', '2020-02-20 19:38:04');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `firstname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'actif',
  `sexe` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `userable_id` int(10) UNSIGNED NOT NULL,
  `userable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_username_unique` (`username`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_role_id_index` (`role_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `username`, `firstname`, `lastname`, `status`, `sexe`, `phone`, `address`, `email`, `role_id`, `password`, `userable_id`, `userable_type`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'olivier', 'Hombabé', 'ABASSA', 'actif', 'M', '90325861', 'Quartier: DJIDJOLE', 'olivier@yahoo.fr', 1, '$2y$10$rOlXnrTHEHclOjYPQZWJ2e4B9cg9QUwOfqJgGg04k1nVQ/vE4Ipr6', 1, 'App\\Medecin', NULL, '2020-02-20 18:07:32', '2020-02-20 18:07:32');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
