<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsultationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consultations', function (Blueprint $table) {
            $table->increments('id');
            $table->longtext('interrogatoire');
            $table->longtext('examen_clinique');
            $table->longtext('diagnostic');
            $table->longtext('examen_paraclinique');
            $table->date('date_rdv');
            $table->date('date_consultation');
            $table->boolean('facture')->default(0);
            $table->integer('type_consultation_id')->unsigned()->index();
            $table->integer('medecin_id')->unsigned()->index();
            $table->integer('patient_id')->unsigned()->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consultations');
    }
}
