<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsultationProduitSoin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consultation_produit_soin', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('consultation_id')->unsigned();
            $table->integer('soin_id')->unsigned();
            $table->integer('produit_id')->unsigned();
            $table->foreign('consultation_id')->references('id')->on('consultations')
                        ->onDelete('cascade')
                        ->onUpdate('cascade');
            $table->foreign('produit_id')->references('id')->on('produits')
                        ->onDelete('cascade')
                        ->onUpdate('cascade');            
            $table->foreign('soin_id')->references('id')->on('soins')
                        ->onDelete('cascade')
                        ->onUpdate('cascade');      
            $table->integer('quantite_produit');
            $table->double('montant_produit');
            $table->double('montant_soin');
            $table->date('date_soin');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consultation_produit_soin');
    }
}
