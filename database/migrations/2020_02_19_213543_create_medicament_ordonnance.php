<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMedicamentOrdonnance extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medicament_ordonnance', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('medicament_id')->unsigned();
            $table->integer('ordonnance_id')->unsigned();
            $table->foreign('medicament_id')->references('id')->on('medicaments')
                        ->onDelete('cascade')
                        ->onUpdate('cascade');
            $table->foreign('ordonnance_id')->references('id')->on('ordonnances')
                        ->onDelete('cascade')
                        ->onUpdate('cascade');      
            $table->string('posologie');
            $table->date('date_ordonnance');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medicament_ordonnance');
    }
}
