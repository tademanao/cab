<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMedecinPatientTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medecin_patient', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('medecin_id')->unsigned();
            $table->integer('patient_id')->unsigned();
            $table->foreign('medecin_id')->references('id')->on('medecins')
                        ->onDelete('cascade')
                        ->onUpdate('cascade');
            $table->foreign('patient_id')->references('id')->on('patients')
                        ->onDelete('cascade')
                        ->onUpdate('cascade');      
            $table->string('objet');
            $table->date('date');  
            $table->time('heure');                
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medecin_patient');
    }
}
