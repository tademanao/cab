<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnalyseConsultation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('analyse_consultation', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('consultation_id')->unsigned();
            $table->integer('analyse_id')->unsigned();
            $table->foreign('consultation_id')->references('id')->on('consultations')
                        ->onDelete('cascade')
                        ->onUpdate('cascade');
            $table->foreign('analyse_id')->references('id')->on('analyses')
                        ->onDelete('cascade')
                        ->onUpdate('cascade');      
            $table->longtext('resultat')->nullable();
            $table->integer('quantite_analyse');
            $table->double('montant');
            $table->date('date_analyse');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('analyse_consultation');
    }
}
