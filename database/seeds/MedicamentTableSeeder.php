<?php

use Illuminate\Database\Seeder;
use App\Medicament;

class MedicamentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         Medicament::create([
		'libelle_medicament' => 'DOLIPRANE[1000MG] cp',
		]);

         Medicament::create([
		'libelle_medicament' => 'BIOFENAC[100MG] cp',
		]);

		 Medicament::create([
		'libelle_medicament' => 'BACTRIME FORTE[160MG] cp',
		]);

		  Medicament::create([
		'libelle_medicament' => 'FLAMOXINE[1000MG] cp',
		]);

		 Medicament::create([
		'libelle_medicament' => 'CAC[100MG] cp Effervescent',
		]);
 
		 Medicament::create([
		'libelle_medicament' => 'FLAGYL[500MG] cp',
		]);

		 Medicament::create([
		'libelle_medicament' => 'CALCIUM[500MG] sachet',
		]);

		 Medicament::create([
		'libelle_medicament' => 'ROCMALINE amp buvable',
		]);

		Medicament::create([
		'libelle_medicament' => 'POTENCIATOR[5g] amp buvable',
		]);

		Medicament::create([
		'libelle_medicament' => 'ZAMOX[500MG] sachet',
		]);

		Medicament::create([
		'libelle_medicament' => 'PARAFIZZ[500MG] cp effervescent',
		]); 
    }
}
