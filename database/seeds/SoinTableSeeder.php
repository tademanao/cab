<?php

use Illuminate\Database\Seeder;
use App\Soin;


class SoinTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         Soin::create([
		'libelle_soin' => 'Injection',
		'prix_soin' => 300,
		]);

         Soin::create([
		'libelle_soin' => 'Perfusion',
		'prix_soin' => 3000,
		]);

         Soin::create([
		'libelle_soin' => 'suture',
		'prix_soin' => 500,
		]);

        Soin::create([
		'libelle_soin' => 'Pansement',
		'prix_soin' => 500,
		]);
    }
}
