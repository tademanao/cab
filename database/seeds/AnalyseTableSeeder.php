<?php

use Illuminate\Database\Seeder;
use App\Analyse;

class AnalyseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         Analyse::create([
		'libelle_analyse' => 'Coproculture',
		'prix_analyse' => 5000,
		]);

         Analyse::create([
		'libelle_analyse' => 'Créatinine',
		'prix_analyse' => 2000,
		]);

         Analyse::create([
		'libelle_analyse' => 'Goutte Epaisse',
		'prix_analyse' => 2000,
		]);

        Analyse::create([
		'libelle_analyse' => 'NFS/Plaquettes',
		'prix_analyse' => 5000,
		]);

		 Analyse::create([
		'libelle_analyse' => 'FSH',
		'prix_analyse' => 1200,
		]); 

		 Analyse::create([
		'libelle_analyse' => 'Albuminémie',
		'prix_analyse' => 2000,
		]);

		 Analyse::create([
		'libelle_analyse' => 'Protéine',
		'prix_analyse' => 3500,
		]);

		 Analyse::create([
		'libelle_analyse' => 'Urée',
		'prix_analyse' => 1500,
		]);

		 Analyse::create([
		'libelle_analyse' => 'Vitesse de sédimentation',
		'prix_analyse' => 1500,
		]); 

		//   Analyse::create([
		// 'libelle_analyse' => 'Protéine',
		// 'prix_analyse' => 3500,
		// ]);

		//  Analyse::create([
		// 'libelle_analyse' => 'Urée',
		// 'prix_analyse' => 1500,
		// ]);

		//  Analyse::create([
		// 'libelle_analyse' => 'Vitesse de sédimentation',
		// 'prix_analyse' => 1500,
		// ]); 
    }
}
