<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('users')->insert([
            'username' => 'tademanao',
            'email' => 'tademanao92.ot@gmail.com',
            'firstname'=>'admin',
            'lastname'=>'admin',
            'status'=>'actif',
            'sexe'=>'M',
            'phone'=>'90325862',
            'address'=>'djidjole',
            'role'=> 'Admin',
            'userable_id'=>1,
            'userable_type'=>'App\Medecin',
            'password' => bcrypt('password'),
        ]);
    }
}
