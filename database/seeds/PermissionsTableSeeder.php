<?php

use Illuminate\Database\Seeder;
use App\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {  
    	// roles permission
        Permission::create([
		'name' => 'role-list',
		]);
		Permission::create([
		'name' => 'role-create',
		]);
		 Permission::create([
		'name' => 'role-edit',
		]);
		Permission::create([
		'name' => 'role-delete',
		]);

		//analyse permission
		Permission::create([
		'name' => 'analyse-list',
		]);
		Permission::create([
		'name' => 'analyse-create',
		]);
		 Permission::create([
		'name' => 'analyse-edit',
		]);
		Permission::create([
		'name' => 'analyse-delete',
		]);

		//antecedent permission
		Permission::create([
		'name' => 'antecedent-list',
		]);
		Permission::create([
		'name' => 'antecedent-create',
		]);
		 Permission::create([
		'name' => 'antecedent-edit',
		]);
		Permission::create([
		'name' => 'antecedent-delete',
		]);

		//constante permission
		Permission::create([
		'name' => 'constante-list',
		]);
		Permission::create([
		'name' => 'constante-create',
		]);
		 Permission::create([
		'name' => 'constante-edit',
		]);
		Permission::create([
		'name' => 'constante-delete',
		]);

		//consultation permission
		Permission::create([
		'name' => 'consultation-list',
		]);
		Permission::create([
		'name' => 'consultation-create',
		]);
		Permission::create([
		'name' => 'consultation-edit',
		]);
		Permission::create([
		'name' => 'consultation-delete',
		]);

		//infirmiere permission
		Permission::create([
		'name' => 'infirmiere-list',
		]);
		Permission::create([
		'name' => 'infirmiere-create',
		]);
		Permission::create([
		'name' => 'infirmiere-edit',
		]);
		Permission::create([
		'name' => 'infirmiere-delete',
		]);

		//medecin permission
		Permission::create([
		'name' => 'medecin-list',
		]);
		Permission::create([
		'name' => 'medecin-create',
		]);
		Permission::create([
		'name' => 'medecin-edit',
		]);
		Permission::create([
		'name' => 'medecin-delete',
		]);

		//medicament permission
		Permission::create([
		'name' => 'medicament-list',
		]);
		Permission::create([
		'name' => 'medicament-create',
		]);
		Permission::create([
		'name' => 'medicament-edit',
		]);
		Permission::create([
		'name' => 'medicament-delete',
		]);

		//patient permission
		Permission::create([
		'name' => 'patient-list',
		]);
		Permission::create([
		'name' => 'patient-create',
		]);
		Permission::create([
		'name' => 'patient-edit',
		]);
		Permission::create([
		'name' => 'patient-delete',
		]);

		//produit permission
		Permission::create([
		'name' => 'produit-list',
		]);
		Permission::create([
		'name' => 'produit-create',
		]);
		Permission::create([
		'name' => 'produit-edit',
		]);
		Permission::create([
		'name' => 'produit-delete',
		]);

		//secretaire permission
		Permission::create([
		'name' => 'secretaire-list',
		]);
		Permission::create([
		'name' => 'secretaire-create',
		]);
		Permission::create([
		'name' => 'secretaire-edit',
		]);
		Permission::create([
		'name' => 'secretaire-delete',
		]);

		//soin permission
		Permission::create([
		'name' => 'soin-list',
		]);
		Permission::create([
		'name' => 'soin-create',
		]);
		Permission::create([
		'name' => 'soin-edit',
		]);
		Permission::create([
		'name' => 'soin-delete',
		]);

		//typeConsultation permission
		Permission::create([
		'name' => 'typeConsultation-list',
		]);
		Permission::create([
		'name' => 'typeConsultation-create',
		]);
		Permission::create([
		'name' => 'typeConsultation-edit',
		]);
		Permission::create([
		'name' => 'typeConsultation-delete',
		]);




















    }
}
