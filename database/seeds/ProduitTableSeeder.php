<?php

use Illuminate\Database\Seeder;
use App\Produit;

class ProduitTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         Produit::create([
		'libelle_produit' => 'QUININE AMP INJ 2ML',
		'prix_produit' => 700,
		]);

         Produit::create([
		'libelle_produit' => 'BANDE A GAZE',
		'prix_produit' => 200,
		]);

         Produit::create([
		'libelle_produit' => 'METRONIDAZOLE 100ML INJ',
		'prix_produit' => 1750,
		]);

        Produit::create([
		'libelle_produit' => 'NACL 1AMP',
		'prix_produit' => 500,
		]);

		 Produit::create([
		'libelle_produit' => 'RINGER LACTATE 500ML',
		'prix_produit' => 960,
		]); 

		 Produit::create([
		'libelle_produit' => 'SERUM GLUCOSE 10% 500ML',
		'prix_produit' => 1500,
		]);

		 Produit::create([
		'libelle_produit' => 'SERUM GLUCOSE ISOTONIQUE 5% 500ML',
		'prix_produit' => 600,
		]);

		 Produit::create([
		'libelle_produit' => 'SERUM SALE ISOTONIQUE 5% 500ML',
		'prix_produit' => 1500,
		]);

		 Produit::create([
		'libelle_produit' => 'CATHLON 23G',
		'prix_produit' => 1800,
		]); 

		  Produit::create([
		'libelle_produit' => 'PERFUSEUR',
		'prix_produit' => 700,
		]);

		 Produit::create([
		'libelle_produit' => 'SERINGUE 10ML',
		'prix_produit' => 200,
		]); 

		 Produit::create([
		'libelle_produit' => 'SERINGUE 2ML',
		'prix_produit' => 175,
		]);

		 Produit::create([
		'libelle_produit' => 'SERINGUE 5ML',
		'prix_produit' => 125,
		]);

		 Produit::create([
		'libelle_produit' => 'BANDE ELASTIQUE',
		'prix_produit' => 800,
		]);

		 Produit::create([
		'libelle_produit' => 'KCL AMP',
		'prix_produit' => 220,
		]); 
    }
}
