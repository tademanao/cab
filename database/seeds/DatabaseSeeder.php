<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        $this->call(ProduitTableSeeder::class);
        $this->call(AnalyseTableSeeder::class);
        $this->call(MedecinTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(MedicamentTableSeeder::class);
        $this->call(SoinTableSeeder::class);

    }
}
