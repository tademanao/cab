<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



//liste des rdv
Route::get('rendez_vous','ConsultationController@rendezVous')->name('rendez_vous');
Route::get('rdv','ConsultationController@rdv')->name('rdv');
Route::get('rdv/{id}/edit','ConsultationController@editRdv')->name('rdv_edit');
Route::put('rdv/{id}/update','ConsultationController@updateRdv')->name('rdv_update');

//get les constantes d'un patient

Route::get('/getConstante/{id}','PatientController@getConstante')->name('constante');

//get les consultations d'un patient

Route::get('/getConsultation/{id}','PatientController@getConsultation')->name('consultation');

//facturation

Route::get('consultation/{id}/facture','ConsultationController@facture')->name('facture');

//historique des prix de consultation
Route::get('/historique','TypeConsultationController@historique')->name('historique');

 //gestion du user  profil
    Route::get('/profil', 'ProfilController@index')->name('profil');
    Route::put('/profil/{id}/update', 'ProfilController@update')->name('profil.update');
    Route::post('/profil/change_password', 'ProfilController@change_password')->name('password');

// gestion ordonnance d'une consultation
Route::get('consultation/{id}/ordonnance','ConsultationController@ordonnance')->name('ordonnance');
Route::post('consultation/ordonnance','ConsultationController@postOrdonnance')->name('ordonnance_post');
Route::get('consultation/edit/{id}/ordonnance','ConsultationController@editOrdonnance')->name('ordonnance_edit');
Route::put('consultation/update/{id}/ordonnance','ConsultationController@updateOrdonnance')->name('ordonnance_update');
Route::get('consultation/delete/{id}/ordonnance','ConsultationController@destroyOrdonnance')->name('ordonnance_destroy');

//gestion soin d'une consultation
Route::get('consultation/{id}/soin','ConsultationController@soin')->name('soin');
Route::post('consultation/soin','ConsultationController@postSoin')->name('soin_post');
Route::get('consultation/edit/{id}/soin','ConsultationController@editSoin')->name('soin_edit');
Route::put('consultation/update/{id}/soin','ConsultationController@updateSoin')->name('soin_update');
Route::get('consultation/delete/{id}/soin','ConsultationController@destroySoin')->name('soin_destroy');


//gestion analyse d'une consultation
Route::get('consultation/{id}/analyse','ConsultationController@analyse')->name('analyse');
Route::post('consultation/analyse','ConsultationController@postAnalyse')->name('analyse_post');
Route::get('consultation/edit/{id}/analyse','ConsultationController@editAnalyse')->name('analyse_edit');
Route::put('consultation/update/{id}/analyse','ConsultationController@updateAnalyse')->name('analyse_update');
Route::get('consultation/delete/{id}/analyse','ConsultationController@destroyAnalyse')->name('analyse_destroy');

//liste des etats
Route::get('etat','ConsultationController@etat')->name('etat');
Route::post('consultation/date/pdf','ConsultationController@pdfConsultationParDate')->name('consultation_date_pdf');
Route::post('consultation/periode/pdf','ConsultationController@pdfConsultationParPeriode')->name('consultation_periode_pdf');
Route::post('patient/periode/pdf','PatientController@pdfPatientParPeriode')->name('patient_periode_pdf');
Route::post('consultation/patient/pdf','ConsultationController@pdfConsultationParPatient')->name('consultation_patient_pdf');
Route::post('consultation/patient/periode/pdf','ConsultationController@pdfConsultationParPatientPeriode')->name('consultation_patient_periode_pdf');
Route::post('consultation/ordonnance/pdf','ConsultationController@pdfOrdonnance')->name('consultation_ordonnance_pdf');
Route::get('consultation/{id}/facture/pdf','ConsultationController@pdfFacture')->name('consultation_facture_pdf');

Route::resource('role','RoleController');
Route::resource('permission','PermissionController');
Route::resource('medecin','MedecinController');
Route::get('medecin/destroy/{id}','MedecinController@destroy');
Route::resource('infirmiere','InfirmiereController');
Route::get('infirmiere/destroy/{id}','InfirmiereController@destroy');
Route::resource('secretaire','SecretaireController');
Route::get('secretaire/destroy/{id}','SecretaireController@destroy');
Route::resource('patient','PatientController');
Route::get('patient/destroy/{id}','PatientController@destroy');
// Route::resource('antecedent','AntecedentController');
// Route::resource('technicien','TechnicienController');
Route::resource('consultation','ConsultationController');
Route::get('consultation/destroy/{id}','ConsultationController@destroy');
Route::resource('constante','ConstanteController');
Route::get('constante/destroy/{id}','ConstanteController@destroy');
Route::resource('typeConsultation','TypeConsultationController');
Route::get('typeConsultation/destroy/{id}','TypeConsultationController@destroy');
// Route::resource('typeAntecedent','TypeAntecedentController');
Route::resource('soin','SoinController');
Route::get('soin/destroy/{id}','SoinController@destroy');
Route::resource('analyse','AnalyseController');
Route::get('analyse/destroy/{id}','AnalyseController@destroy');
Route::resource('produit','ProduitController');
Route::get('produit/destroy/{id}','ProduitController@destroy');
Route::resource('medicament','MedicamentController');
Route::get('medicament/destroy/{id}','MedicamentController@destroy');


// Route::resource('user','UserController');
Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
