
<!DOCTYPE html>
<html>
  <head>
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <style>
    table#container {
      width:100%; 
      border: 1px solid black;
      text-align:center;
    }

    th.bord,td.bord{
      border: 1px solid black;
   
      ;    }
  


   
</style>

  </head>
  <body>

      <table id ="en-tete" style="width: 100%" >
    
       <tbody>

        <tr>

            <td valign="top">
              Cabinet Médical <span><strong>LE SAUVEUR</strong></span> <br> 
            Dr <strong>{{$consultation->medecin->user->lastname }} {{ $consultation->medecin->user->firstname }}</strong><br>
            Medecine Générale<br>
            Tel: 22-25-91-20- Cel: 90-71-46-83<br>
            Face Pharmacie de Djidjolé Avenue de Pya <br>
            BP: 80535 - Lomé-TOGO

            </td>

            <td align="right" valign="top">
            <strong >Date: {{  date('d-m-Y',strtotime($consultation->date_consultation))}}</strong><br>
            <strong>Autorisation n° 29/89 MSPASCF</strong><br><br><br><br><br><br>
            <strong>Patient: {{ $consultation->patient->nom }} {{ $consultation->patient->prenom }}</strong> 
            
            </td>

        </tr>
       </tbody>
    </table><br/>


     <!--  <table >
    
       <tbody>

        <tr>

            <td>
              <strong>Patient : {{ $consultation->patient->nom }} {{ $consultation->patient->prenom }}</strong>            
            </td>

            <td align="right" valign="top">
            <strong>Date: {{ date('d/m/Y') }}</strong> 
            </td>

        </tr>
       </tbody>
      </table><br/> -->

   
<!-- 
    <div style="float: right;" >
     <strong>Patient : {{ $consultation->patient->nom }} {{ $consultation->patient->prenom }}</strong> 
    </div><br/></br>
    <div style="float: left;" >
     <strong>Patient : {{ $consultation->patient->nom }} {{ $consultation->patient->prenom }}</strong> 
    </div><br/></br> -->
 
<!--  <tr><p style="float: right;">{{ date('d-m-Y') }}</p></tr> -->

   
   <h2><center><u>Facture N°{{ $consultation->id }}</u></center></h2>
      
     <table id="container" cellpadding="0" cellspacing="0">

       <thead>
          <tr>
             <th class="bord">Libelle</th>
             <th class="bord">Quantite</th>
             <th class="bord">Prix unitaire</th>
             <th class="bord">Total</th>
          </tr>
        </thead>
    
       <tbody>
        <tr>
          <td colspan="4" style="background-color: silver; border-bottom: 1px;">
            Consultation
          </td>          
        </tr>
        <tr>
              <td class="bord">Consultation</td>
              <td class="bord">1</td>
              <td class="bord">{{$consultation->typeConsultation->prix_consultation}}</td>
              <td class="bord">{{$consultation->typeConsultation->prix_consultation}}</td>
        </tr>
        <tr>
          <td align="right" class="bord" colspan="3"><strong>Total Consultation:&nbsp;&nbsp;</strong></td>
          <td class="bord"><strong>{{$consultation->typeConsultation->prix_consultation}}</strong></td>
        </tr>
         <tr>
          <td colspan="4" style="background-color: silver; border-bottom: 1px;">
            Analyses
          </td>          
        </tr>



        @foreach($analyses as $analyse)
         <tr>
              <td class="bord">{{$analyse->libelle_analyse}}</td>
              <td class="bord">{{$analyse->quantite_analyse}}</td>
              <td class="bord">{{$analyse->prix_analyse}}</td>
              <td class="bord">{{$analyse->montant}}</td>
         </tr>
        @endforeach
        <tr>
          <td align="right" class="bord" colspan="3"><strong>Total Analyse:&nbsp;&nbsp;</strong></td>
          <td class="bord"><strong>{{$montant_analyse}}</strong></td>
        </tr>
         <tr>
          <td colspan="4" style="background-color: silver; border-bottom: 1px;">
            Soins
          </td>          
        </tr>

          @if($soins == null)

             <tr>
              <td class="bord"></td>
              <td class="bord"></td>
              <td class="bord"></td>
              <td class="bord"></td>
             </tr>

          @else

             @foreach($soins as $soin)
         <tr>
              <td class="bord">{{$soin->libelle_soin}}</td>
              <td class="bord">1</td>
              <td class="bord">{{$soin->prix_soin}}</td>
              <td class="bord">{{$soin->prix_soin}}</td>
         </tr>
        @endforeach

          @endif
          <tr>
            <td align="right" class="bord" colspan="3"><strong>Total Soin:&nbsp;&nbsp;</strong></td>
            <td class="bord"><strong>{{$montant_total_soin}}</strong></td>
          </tr>
           <tr>
          <td colspan="4" style="background-color: silver; border-bottom: 1px;">
            Produits & Consommables
          </td>          
        </tr>
         @foreach($produits as $produit)
         <tr>
              <td class="bord">{{$produit->libelle_produit}}</td>
              <td class="bord">{{$produit->quantite_produit}}</td>
              <td class="bord">{{$produit->prix_produit}}</td>
              <td class="bord">{{$produit->montant_produit}}</td>
         </tr>
        @endforeach
         <tr>
         
            <td align="right" class="bord" colspan="3"><strong>Total Produits & Consommables:&nbsp;&nbsp;</strong></td>
            <td class="bord"><strong>{{$montant_total_produit}}</strong></td>
          </tr>
          <tr>
            <td align="right"  colspan="3" class="bord"><strong>Total à payer:&nbsp;&nbsp;</strong></td>
            <td class="bord" ><strong>{{$totaux}}</strong></td>
          </tr>
       
       
     
       </tbody>
    </table>

  </body>
</html>
                   
          

