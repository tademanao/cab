@extends("template")
  
  @section('content')

  @include('flash')

<div class="row">
          <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Ordonnance</h3>

                </div><!-- /.box-header -->
                <!-- form start -->
               {!!Form::open(['method'=>'put','url'=>route('ordonnance_update',$ordonnance->id)]) !!}
                  <div class="box-body">
                    
                  <!--  <div class="form-group col-md-6 {!! $errors->has('date_ordonnance')?'has-error': '' !!}">
                            {!!Form::label('date_ordonnance','date')!!}
                            {!! Form::input('date','date_ordonnance', date('Y-m-d'),['class'=>'form-control','id'=>'date_ordonnance','placeholder'=>'', 'row'=>8]) !!}
                            {!! $errors->first('date_ordonnance','<small class="help-block"><strong>:message</strong></small>') !!}
                   </div>  --> 

                   <div class="form-group col-md-6">

                          {!!Form::label('medicament_id', 'medicaments')!!}
                           {!! Form::select('medicament_id',$medicaments,$ordonnance->medicament_id, ['class'=>'selectpicker form-control', 'data-live-search'=>'true', 'data-live-search-style'=>'startsWith' ]) !!}
                           
                   </div>

                    <div class="form-group col-md-6 {!! $errors->has('posologie')?'has-error': '' !!}">
                            {!!Form::label('posologie','posologie')!!}
                            {!! Form::text('posologie',$ordonnance->posologie, ['class'=>'form-control','id'=>'posologie','placeholder'=>'']) !!}
                            {!! $errors->first('posologie','<small class="help-block"><strong>:message</strong></small>') !!}
                    </div>
                  
                    
                 </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Envoyer</button> <a class="btn btn-primary" href="{{ route('ordonnance',$consultation) }}">Retour</a>
                  </div>

                  {!! Form::close() !!}
               </div><!-- /.box -->
                         
          </div>
           
</div>

 

@stop