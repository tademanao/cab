@extends("template")
  
  @section('content')

  @include('flash')

<div class="row">
          <div class="col-md-8">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">soin</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
               {!!Form::open(['method'=>'pot','url'=>('/consultation/soin')]) !!}
                  <div class="box-body">
                    
                  <div class="form-group col-md-6" style="display: none;">
                    {!!Form::label('consultation','consultation')  !!}
                    {!!Form::text('id',$consultation->id,['class'=>'form-control','rows'=>'8'])  !!}
                   </div> 

                  <!--  <div class="form-group col-md-6 {!! $errors->has('date_soin')?'has-error': '' !!}">
                            {!!Form::label('date_soin','date')!!}
                            {!! Form::input('date','date_soin', date('Y-m-d'),['class'=>'form-control','id'=>'date_soin','placeholder'=>'', 'row'=>8]) !!}
                            {!! $errors->first('date_soin','<small class="help-block"><strong>:message</strong></small>') !!}
                   </div>  --> 

                   <div class="form-group col-md-6">

                           {!!Form::label('soin_id', 'soins')!!}
                           {!! Form::select('soin_id',$soins,null, ['class'=>'selectpicker form-control', 'data-live-search'=>'true', 'data-live-search-style'=>'startsWith' ]) !!} 
                           
                   </div>

                   <div class="form-group col-md-6">

                           {!!Form::label('produit_id', 'produit')!!}
                           {!! Form::select('produit_id',$produits,null, ['class'=>'selectpicker form-control','data-live-search'=>'true']) !!}    
                   </div>

                    <div class="form-group col-md-6 {!! $errors->has('quantite_produit')?'has-error': '' !!}">
                            {!!Form::label('quantite_produit','quantite produit')!!}
                            {!! Form::input('number','quantite_produit',null, ['class'=>'form-control','id'=>'quantite_produit','min'=>1,'placeholder'=>'','required'=>true]) !!}
                            {!! $errors->first('quantite_produit','<small class="help-block"><strong>:message</strong></small>') !!}
                    </div>
                    
                 </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Envoyer</button> &nbsp;<a class="btn btn-primary" href="{{ route('consultation.create',$consultation)}}">Retour <i class="fa fa-index"></i></a>
                  </div>

                  {!! Form::close() !!}
               </div><!-- /.box -->
                         
          </div>

          <div class="col-md-12">
            <div class="box box-primary">
                  <div class="box-header">
                    <h3 class="box-title"></h3>
                  </div><!-- /.box-header -->
                  <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                      <thead>
                        <tr>
                           <th>Code</th>
                           <th>Soin</th>
                           <th>Produit</th>
                           <th>Quantite</th>
                           <th>Date</th>
                           <th>Actions</th>
                        </tr>
                        </tr>
                      </thead>
                      <tbody>
                       
                      
                      @foreach($consultation_produit_soins as $consultation_produit_soin) 

                      <tr> 
                            <td>{{ $consultation_produit_soin->id }}</td>
                            <td>{{ $consultation_produit_soin->libelle_soin }}</td>
                            <td>{{ $consultation_produit_soin->libelle_produit }}</td>
                            <td>{{ $consultation_produit_soin->quantite_produit }}</td>
                            <td>{{ date('d-m-Y',strtotime($consultation_produit_soin->date_soin)) }}</td>
                            <td>
                              <a class="btn btn-xs btn-primary" href="{{ route('soin_edit',$consultation_produit_soin->id)}}">Editer
                              </a>
                              <a class="btn btn-xs btn-danger" href="{{URL::route('soin_destroy',$consultation_produit_soin->id) }}" onclick="return confirm(' Voulez-vous Vraiment supprimer cet enregistrement?')">Supprimer</a>
                            </td>
                          
            
                      </tr>

                      @endforeach
                      </tbody>
                       <tfoot>
                        <tr>
                           <th>Code</th>
                           <th>Soin</th>
                           <th>Produit</th>
                           <th>Quantite</th>
                           <th>Date</th>
                           <th>Actions</th>
                        </tr>
                      </tfoot>
                    </table>
                  </div><!-- /.box-body -->
            </div><!-- /.box -->  
          </div>


           
</div>

 

@stop