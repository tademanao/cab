@extends("template")
  
  @section('content')

  @include('flash')

  <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Liste des rendez-vous</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                         <th>Patient</th>
                         <th>Medecin</th>
                         <th>consultation</th>
                         <th>Rendez-vous</th>
                         <th>Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                     
                    @foreach($consultations as $consultation) 

                    <tr> 
                         <td>{{ $consultation->patient->nom}}  {{ $consultation->patient->prenom}}</td>
                         <td>{{ $consultation->medecin->user->lastname}}  {{ $consultation->medecin->user->firstname}}</td>
                         <td>{{ date('d-m-Y',strtotime($consultation->date_consultation)) }}</td>
                         <td>{{ date('d-m-Y',strtotime($consultation->date_rdv)) }}</td>
                         <td>@can('isSecretaire')<a class="btn btn-sm btn-warning" href="{{ route('rdv_edit',$consultation)}}">Editer</a>@endcan</td>

                    </tr>
                    @endforeach
                    </tbody>
                     <tfoot>
                      <tr>
                         <th>Patient</th>
                         <th>Medecin</th>
                         <th>consultation</th>
                         <th>Rendez-vous</th>
                         <th>Actions</th>
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
          </div><!-- /.box --> 
          <a class="btn btn-primary" href="{{ route('rendez_vous') }}">Retour</a>
        </div> 

  </div> 



     
@stop

@section('scripts')


<script type="text/javascript">

  
     $("#patient_id").change(function(){

         var patient = $(this).val();
       

         $.ajax({
            url: 'http://localhost/cab/getConstante/' + patient,
            cache: false,
            type: 'GET',
            success: function(response){
              console.log('succes');
              $("#constante").html(response);
            },
            error: function(xhr){
              console.log('error');
            }
         });
          
     }); 


      $("#patient_id").change(function(){

         var patient = $(this).val();
       

         $.ajax({
            url: 'http://localhost/cab/getConsultation/' + patient,
            cache: false,
            type: 'GET',
            success: function(response){
              console.log('succes');
              $("#consultation").html(response);
            },
            error: function(xhr){
              console.log('error');
            }
         });
          
     });



</script>
@stop