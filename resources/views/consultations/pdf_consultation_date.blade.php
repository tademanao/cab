
<!DOCTYPE html>
<html>
  <head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
   <style>
    table {
      border-collapse: collapse;
      border-spacing: 0;
      width: 100%;
      border: 1px solid black;
    }

    th{
      background-color: silver;
    }

    th, td {
      text-align: left;
      padding: 16px;
    }

    tr:nth-child(even) {
      background-color: #f2f2f2
    }
   
   </style>

  </head>
  <body>
   
   <h2><center>LISTE DES CONSULTATIONS</center></h2>

     <table >
    


                        <thead>
                          <tr>
                             <th>Code</th>
                             <th>Patient</th>
                             <th>Diagnostic</th>
                             <th>Date</th>
                          </tr>
                        </thead>
                        <tbody>
                        
                      @foreach($consultations as $consultation) 

                        <tr> 
                             <td>{{$consultation->id}}</td>
                             <td>{{$consultation->patient->nom}} {{$consultation->patient->prenom}}</td>
                             <td>{{$consultation->diagnostic}}</td>
                             <td>{{date('d-m-Y',strtotime($consultation->date_consultation)) }}</td>
                        </tr>
                        @endforeach
                        </tbody>
                         <tfoot>
                          <tr>
                             <th>Code</th>
                             <th>Patient</th>
                             <th>Diagnostic</th>
                             <th>Date</th>
                          </tr>
                        </tfoot>
                      </table>

  </body>
</html>
                   
          

