
<!DOCTYPE html>
<html>
  <head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
   <style>
    table {
      border-collapse: collapse;
      border-spacing: 0;
      width: 100%;
      border: 1px solid black;
    }

    th{
      background-color: silver;
    }

    th, td {
      text-align: left;
      padding: 16px;
    }

    tr:nth-child(even) {
      background-color: #f2f2f2
    }
   
   </style>

  </head>
  <body>
   
   <h2><center>LISTE DES ELEVES PAR PARCOURS</center></h2>

     <table >
    


                        <thead>
                          <tr>
                             <th>Matricule</th>
                             <th>Nom</th>
                             <th>Prenoms</th>
                             <th>Sexe</th>
                             <th>Parcours</th>
                             
                          </tr>
                        </thead>
                        <tbody>
                        
                      @foreach($eleves as $eleve) 

                        <tr> 
                             <td>{{ $eleve->matricule}}</td>
                             <td>{{ $eleve->user->name}}</td>
                             <td>{{ $eleve->user->prenoms}}</td>
                             <td>{{ $eleve->user->sexe}}</td>
                             <td>{{ $eleve->parcour->libelle_parcours }}</td>
                        </tr>
                        @endforeach
                        </tbody>
                         <tfoot>
                          <tr>
                             <th>Matricule</th>
                             <th>Nom</th>
                             <th>Prenoms</th>
                             <th>Sexe</th>
                             <th>Parcours</th>
                          </tr>
                        </tfoot>
                      </table>

  </body>
</html>
                   
          

