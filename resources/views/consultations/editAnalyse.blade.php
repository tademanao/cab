@extends("template")
  
  @section('content')

  @include('flash')

<div class="row">
          <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">analyse</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
               {!!Form::open(['method'=>'put','url'=>route('analyse_update',$analyse->id)]) !!}
                  <div class="box-body"> 

                  <!--  <div class="form-group col-md-6 {!! $errors->has('date_analyse')?'has-error': '' !!}">
                            {!!Form::label('date_analyse','date')!!}
                            {!! Form::input('date','date_analyse', date('Y-m-d'),['class'=>'form-control','id'=>'date_analyse','placeholder'=>'', 'row'=>8]) !!}
                            {!! $errors->first('date_analyse','<small class="help-block"><strong>:message</strong></small>') !!}
                   </div>  --> 

                   <div class="form-group col-md-6">

                          {!!Form::label('analyse_id', 'analyses')!!}
                           {!! Form::select('analyse_id',$analyses,null, ['class'=>'selectpicker form-control', 'data-live-search'=>'true', 'data-live-search-style'=>'startsWith' ]) !!}
                           
                   </div>

                    <div class="form-group col-md-6 {!! $errors->has('quantite_analyse')?'has-error': '' !!}">
                            {!!Form::label('quantite_analyse','quantite_analyse')!!}
                            {!! Form::input('number','quantite_analyse',$analyse->quantite_analyse, ['class'=>'form-control','min'=>1,'id'=>'quantite_analyse','placeholder'=>'']) !!}
                            {!! $errors->first('quantite_analyse','<small class="help-block"><strong>:message</strong></small>') !!}
                    </div>

                    <div class="form-group col-md-6 {!! $errors->has('resultat')?'has-error': '' !!}">
                         {!!Form::label('resultat','resultat')  !!}
                         {!! Form::textarea('resultat',$analyse->resultat, ['class'=>'form-control','id'=>'resultat','placeholder'=>'','rows'=>'6']) !!}
                          {!! $errors->first('resultat','<small class="help-block"><strong>:message</strong></small>') !!}
                    </div>
                  
                    
                 </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Envoyer</button> <a class="btn btn-primary" href="{{ route('analyse',$analyse->consultation_id) }}">Retour</a>
                  </div>

                  {!! Form::close() !!}
               </div><!-- /.box -->
                         
          </div>
           
</div>

 

@stop