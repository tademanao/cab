@extends("template")
  
  @section('content')

  @include('flash')

<div class="row">
          <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">liste des consultations par date</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                {!!Form::open(['method'=>'POST', 'url'=>route('consultation_date_pdf')]) !!}
                  <div class="box-body">

                         <div class="form-group col-md-6 {!! $errors->has('date_consultation')?'has-error': '' !!}">
                            {!! Form::input('date','date_consultation', date('Y-m-d'),['class'=>'form-control','id'=>'date_consultation','placeholder'=>'', 'row'=>8]) !!}
                            {!! $errors->first('date_consultation','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div> 
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Imprimer</button>
                  </div>

                  {!! Form::close() !!}
              </div><!-- /.box -->
                  
          </div>

          <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">liste des consultations par periode</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                {!!Form::open(['method'=>'POST', 'url'=>route('consultation_periode_pdf')]) !!}
                  <div class="box-body">

                         <div class="form-group col-md-6 {!! $errors->has('date_deb')?'has-error': '' !!}">
                            {!! Form::input('date','date_deb', date('Y-m-d'),['class'=>'form-control','id'=>'date_deb','placeholder'=>'', 'row'=>8]) !!}
                            {!! $errors->first('date_deb','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div> 
                         <div class="form-group col-md-6 {!! $errors->has('date_fin')?'has-error': '' !!}">
                            {!! Form::input('date','date fin', date('Y-m-d'),['class'=>'form-control','id'=>'date_fin','placeholder'=>'', 'row'=>8]) !!}
                            {!! $errors->first('date_fin','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div> 
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Imprimer</button>
                  </div>

                  {!! Form::close() !!}
              </div><!-- /.box -->
                  
          </div>

           <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">liste des consultations par patient</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                {!!Form::open(['method'=>'POST', 'url'=>route('consultation_patient_pdf')]) !!}
                  <div class="box-body">

                         <div class="form-group col-md-6">
                            {!! Form::select('patient_id',$patients,null, ['class'=>'selectpicker form-control', 'data-live-search'=>'true', 'data-live-search-style'=>'startsWith' ]) !!}
                         </div>   
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Imprimer</button>
                  </div>

                  {!! Form::close() !!}
              </div><!-- /.box -->
                  
          </div>

            <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">liste des patients par periode</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                {!!Form::open(['method'=>'POST', 'url'=>route('patient_periode_pdf')]) !!}
                  <div class="box-body"> 
                         <div class="form-group col-md-6 {!! $errors->has('date_deb')?'has-error': '' !!}">
                            {!! Form::input('date','date_deb', date('Y-m-d'),['class'=>'form-control','id'=>'date_deb','placeholder'=>'', 'row'=>8]) !!}
                            {!! $errors->first('date_deb','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div> 
                         <div class="form-group col-md-6 {!! $errors->has('date_fin')?'has-error': '' !!}">
                            {!! Form::input('date','date fin', date('Y-m-d'),['class'=>'form-control','id'=>'date_fin','placeholder'=>'', 'row'=>8]) !!}
                            {!! $errors->first('date_fin','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>  
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Imprimer</button>
                  </div>

                  {!! Form::close() !!}
              </div><!-- /.box -->
                  
          </div>

          <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">liste des consultations par patient et par periode</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                {!!Form::open(['method'=>'POST', 'url'=>route('consultation_patient_periode_pdf')]) !!}
                  <div class="box-body">

                         <div class="form-group col-md-6">
                            {!! Form::select('patient_id',$patients,null, ['class'=>'selectpicker form-control', 'data-live-search'=>'true', 'data-live-search-style'=>'startsWith' ]) !!}
                         </div>  
                         <div class="form-group col-md-6 {!! $errors->has('date_deb')?'has-error': '' !!}">
                            {!! Form::input('date','date_deb', date('Y-m-d'),['class'=>'form-control','id'=>'date_deb','placeholder'=>'', 'row'=>8]) !!}
                            {!! $errors->first('date_deb','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div> 
                         <div class="form-group col-md-6 {!! $errors->has('date_fin')?'has-error': '' !!}">
                            {!! Form::input('date','date fin', date('Y-m-d'),['class'=>'form-control','id'=>'date_fin','placeholder'=>'', 'row'=>8]) !!}
                            {!! $errors->first('date_fin','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>  
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Imprimer</button>
                  </div>

                  {!! Form::close() !!}
              </div><!-- /.box -->
                  
           </div>


</div> 

     
@stop