@extends("template")
  
  @section('content')

  @include('flash')

<div class="row">
          <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Editer la consultation</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
               {!!Form::open(['method'=>'put','url'=>route('consultation.update',$consultation)]) !!}
                  <div class="box-body">
                    
                    <div class="form-group col-md-4">
                       {!!Form::label('patient_id', 'patients')!!}
                       {!! Form::select('patient_id',$patients,$consultation->patient_id, ['class'=>'selectpicker form-control', 'data-live-search'=>'true', 'data-live-search-style'=>'startsWith' ]) !!}
                    </div>  

                    <div class="form-group col-md-4">
                           {!! Form::label('type_consultation_id', 'type consultation')!!}
                           {!! Form::select('type_consultation_id',$tab,$num, ['class'=>'selectpicker form-control', 'data-live-search'=>'true', 'data-live-search-style'=>'startsWith' ]) !!}
                     </div> 

                   <div class="form-group col-md-4 {!! $errors->has('interrogatoire')?'has-error': '' !!}">
                    {!!Form::label('interrogatoire','interrogatoire')  !!}
                    {!!Form::textarea('interrogatoire',$consultation->interrogatoire,['class'=>'form-control','rows'=>'3'])  !!}
                    {!! $errors->first('interrogatoire','<small class="help-block"><strong>:message</strong></small>') !!}
                   </div>  
                  
                    <div class="form-group col-md-4 {!! $errors->has('examen_clinique')?'has-error': '' !!}">
                    {!!Form::label('examen clinique','examen clinique')  !!}
                    {!!Form::textarea('examen_clinique',$consultation->examen_clinique,['class'=>'form-control','rows'=>'5'])  !!}
                     {!! $errors->first('examen_clinique','<small class="help-block"><strong>:message</strong></small>') !!}
                    </div>    
                  
                  
                    <div class="form-group col-md-4 {!! $errors->has('examen_paraclinique')?'has-error': '' !!}">
                    {!!Form::label('examen_paraclinique','examen_paraclinique')  !!}
                    {!!Form::textarea('examen_paraclinique',$consultation->examen_paraclinique,['class'=>'form-control','rows'=>'5'])  !!}
                    {!! $errors->first('examen_paraclinique','<small class="help-block"><strong>:message</strong></small>') !!}
                    </div>
                     <div class="form-group col-md-4 {!! $errors->has('diagnostic')?'has-error': '' !!}">  
                    {!!Form::label('diagnostic','diagnostic')  !!}
                    {!!Form::text('diagnostic',$consultation->diagnostic,['class'=>'form-control'])  !!}
                    {!! $errors->first('diagnostic','<small class="help-block"><strong>:message</strong></small>') !!}
                    </div>

                    <div class="form-group col-md-4">
                            {!!Form::label('date_rdv','date rendez-vous')!!}
                            {!! Form::input('date','date_rdv', $consultation->date_rdv,['class'=>'form-control','id'=>'date_rdv','placeholder'=>'', 'row'=>8]) !!}
                            {!! $errors->first('date_rdv','<small class="help-block"><strong>:message</strong></small>') !!}
                    </div> 
                     
                    

                      
                     <!--  <div class="form-group col-md-4">

                       {!!Form::label('medecin_id', 'medecins')!!}
                       {!! Form::select('medecin_id',$medecins,$consultation->medecin_id, ['class'=>'form-control']) !!}
                       
                       </div> -->

                 </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Envoyer</button>  <a class="btn btn-primary" href="{{ route('consultation.create',$consultation) }}">Retour</a>
                  </div>

                  {!! Form::close() !!}
              </div><!-- /.box -->
                   <!--  <a class="btn btn-primary" href="{{ route('consultation.index',$consultation)}}">Retour <i class="fa fa-index"></i></a></td>  -->       
          </div>

@stop