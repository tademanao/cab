@extends("template")
  
  @section('content')

  @include('flash')

<div class="row">
          <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">analyse</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
               {!!Form::open(['method'=>'pot','url'=>('/consultation/analyse')]) !!}
                  <div class="box-body">
                    
                   <div class="form-group col-md-6" style="display: none;">
                    {!!Form::label('consultation','consultation')  !!}
                    {!!Form::text('id',$consultation->id,['class'=>'form-control','rows'=>'8'])  !!}
                   </div> 

                  <!--  <div class="form-group col-md-6 {!! $errors->has('date_analyse')?'has-error': '' !!}">
                            {!!Form::label('date_analyse','date')!!}
                            {!! Form::input('date','date_analyse', date('Y-m-d'),['class'=>'form-control','id'=>'date_analyse','placeholder'=>'', 'row'=>8]) !!}
                            {!! $errors->first('date_analyse','<small class="help-block"><strong>:message</strong></small>') !!}
                   </div>  --> 

                   <div class="form-group col-md-6">
                           {!!Form::label('analyse_id', 'analyses')!!}
                           {!! Form::select('analyse_id',$analyses,null, ['class'=>'selectpicker form-control', 'data-live-search'=>'true', 'data-live-search-style'=>'startsWith' ]) !!}  
                   </div>

                    <div class="form-group col-md-6 {!! $errors->has('quantite_analyse')?'has-error': '' !!}">
                            {!!Form::label('quantite_analyse','quantite analyse')!!}
                            {!! Form::input('number','quantite_analyse',null, ['class'=>'form-control','min'=>1,'id'=>'quantite_analyse','placeholder'=>'','required'=>true]) !!}
                            {!! $errors->first('quantite_analyse','<small class="help-block"><strong>:message</strong></small>') !!}
                    </div>

                     <div class="form-group col-md-6 {!! $errors->has('resultat')?'has-error': '' !!}">
                         {!!Form::label('resultat','resultat')  !!}
                         {!! Form::textarea('resultat',null, ['class'=>'form-control','id'=>'resultat','placeholder'=>'','rows'=>'6']) !!}
                          {!! $errors->first('resultat','<small class="help-block"><strong>:message</strong></small>') !!}
                      </div>
                  
                    
                 </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Envoyer</button> &nbsp;<a class="btn btn-primary" href="{{ route('consultation.show',$consultation)}}">Retour <i class="fa fa-index"></i></a>
                  </div>

                  {!! Form::close() !!}
               </div><!-- /.box -->
                         
          </div>

          <div class="col-md-12">
            <div class="box box-primary">
                  <div class="box-header">
                    <h3 class="box-title"></h3>
                  </div><!-- /.box-header -->
                  <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                      <thead>
                        <tr>
                           <th>Code</th>
                           <th>Libelle</th>
                           <th>Quantite</th>
                           <th>Resultat</th>
                           <th>Date</th>
                           <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                       
                      
                      @foreach($analyse_consultations as $analyse_consultation) 

                      <tr> 
                            <td>{{ $analyse_consultation->id }}</td>
                            <td>{{ $analyse_consultation->libelle_analyse }}</td>
                            <td>{{ $analyse_consultation->quantite_analyse }}</td>
                            <td>{{ $analyse_consultation->resultat }}</td>
                            <td>{{ date('d-m-Y',strtotime($analyse_consultation->date_analyse)) }}</td>
                            <td>
                             <a class="btn btn-xs btn-primary" href="{{ route('analyse_edit',$analyse_consultation->id)}}">Editer</a>
                             <a class="btn btn-xs btn-danger" href="{{URL::route('analyse_destroy',$analyse_consultation->id) }}" onclick="return confirm(' Voulez-vous Vraiment supprimer cet enregistrement?')">Supprimer</a></td>
                            </td>
                      </tr>
                      @endforeach
                      </tbody>
                       <tfoot>
                        <tr>
                           <th>Code</th>
                           <th>Libelle</th>
                           <th>Quantite</th>
                           <th>Resultat</th>
                           <th>Date</th>
                           <th>Actions</th>
                      </tfoot>
                    </table>
                  </div><!-- /.box-body -->
            </div><!-- /.box -->  
          </div>


           
</div>

 

@stop