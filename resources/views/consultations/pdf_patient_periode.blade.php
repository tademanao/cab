
<!DOCTYPE html>
<html>
  <head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
   <style>
    table {
      border-collapse: collapse;
      border-spacing: 0;
      width: 100%;
      border: 1px solid black;
    }

    th{
      background-color: silver;
    }

    th, td {
      text-align: left;
      padding: 16px;
    }

    tr:nth-child(even) {
      background-color: #f2f2f2
    }
   
   </style>

  </head>
  <body>
   
   <h2><center>LISTE DES PATIENTS</center></h2>

     <table >
    


                        <thead>
                          <tr>
                             <th>Code</th>
                             <th>Nom</th>
                             <th>Prénoms</th>
                             <th>Profession</th>
                             <th>Date Inclusion</th>
                          </tr>
                        </thead>
                        <tbody>
                        
                      @foreach($patients as $patient) 

                        <tr> 
                             <td>{{$patient->id}}</td>
                             <td>{{$patient->nom}}</td>
                             <td>{{$patient->prenom}}</td>
                             <td>{{$patient->profession}}</td>
                             <td>{{date('d-m-Y',strtotime($patient->date_inclusion)) }}</td>
                        </tr>
                        @endforeach
                        </tbody>
                         <tfoot>
                          <tr>
                             <th>Code</th>
                             <th>Nom</th>
                             <th>Prénoms</th>
                             <th>Profession</th>
                             <th>Date Inclusion</th>
                          </tr>
                        </tfoot>
                      </table>

  </body>
</html>
                   
          

