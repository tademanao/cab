@extends("template")
  
  @section('content')

  @include('flash')

<div class="row">
          <div class="col-md-8">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">soin</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
               {!!Form::open(['method'=>'put','url'=>route('soin_update',$consultation_produit_soin->id)]) !!}
                  <div class="box-body">

                  <!--  <div class="form-group col-md-6 {!! $errors->has('date_soin')?'has-error': '' !!}">
                            {!!Form::label('date_soin','date')!!}
                            {!! Form::input('date','date_soin', date('Y-m-d'),['class'=>'form-control','id'=>'date_soin','placeholder'=>'', 'row'=>8]) !!}
                            {!! $errors->first('date_soin','<small class="help-block"><strong>:message</strong></small>') !!}
                   </div>  --> 

                   <div class="form-group col-md-6">

                           {!!Form::label('soin_id', 'soins')!!}
                           {!! Form::select('soin_id',$soins,$consultation_produit_soin->soin_id, ['class'=>'selectpicker form-control', 'data-live-search'=>'true', 'data-live-search-style'=>'startsWith' ]) !!} 
                           
                   </div>

                   <div class="form-group col-md-6">

                           {!!Form::label('produit_id', 'produit')!!}
                           {!! Form::select('produit_id',$produits,$consultation_produit_soin->produit_id, ['class'=>'selectpicker form-control', 'data-live-search'=>'true', 'data-live-search-style'=>'startsWith']) !!}
                           
                   </div>

                    <div class="form-group col-md-6 {!! $errors->has('quantite_produit')?'has-error': '' !!}">
                            {!!Form::label('quantite_produit','quantite produit')!!}
                            {!!Form::input('number','quantite_produit',$consultation_produit_soin->quantite_produit, ['class'=>'form-control','id'=>'quantite_produit','placeholder'=>'','min'=>1]) !!}
                            {!! $errors->first('quantite_produit','<small class="help-block"><strong>:message</strong></small>') !!}
                    </div>
                    
                 </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Envoyer</button> <a class="btn btn-primary" href="{{ route('soin',$consultation_produit_soin->consultation_id) }}">Retour</a> 
                  </div>

                  {!! Form::close() !!}
               </div><!-- /.box -->
                         
          </div>
           
</div>

 

@stop