@extends("template")
  
  @section('content')

  @include('flash')

<div class="row">
          <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Editer le rendez-vous</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
               {!!Form::open(['method'=>'put','url'=>route('rdv_update',$consultation)]) !!}
                  <div class="box-body">
                    
                    <div class="form-group col-md-4">
                       {!!Form::label('patient_id', 'patient')!!}
                       {!! Form::select('patient_id',$patients,$consultation->patient_id, ['class'=>'selectpicker form-control', 'data-live-search'=>'true', 'data-live-search-style'=>'startsWith','disabled'=>'disabled']) !!}
                    </div>  

                    <div class="form-group col-md-4">
                       {!!Form::label('medecin_id', 'medecin')!!}
                       {!! Form::select('medecin_id',$medecins,$consultation->medecin_id, ['class'=>'selectpicker form-control', 'data-live-search'=>'true', 'data-live-search-style'=>'startsWith','disabled'=>'disabled']) !!}
                    </div> 

                    <div class="form-group col-md-4">
                            {!!Form::label('date_consultation','consultation')!!}
                            {!! Form::input('date','date_consultation', $consultation->date_consultation,['class'=>'form-control','id'=>'date_consultation','placeholder'=>'', 'row'=>8,'readOnly'=>'readOnly']) !!}
                            {!! $errors->first('date_consultation','<small class="help-block"><strong>:message</strong></small>') !!}
                    </div> 

                    <div class="form-group col-md-4">
                            {!!Form::label('date_rdv','date rendez-vous')!!}
                            {!! Form::input('date','date_rdv', $consultation->date_rdv,['class'=>'form-control','id'=>'date_rdv','placeholder'=>'', 'row'=>8]) !!}
                            {!! $errors->first('date_rdv','<small class="help-block"><strong>:message</strong></small>') !!}
                    </div> 
                     
                    

                      
                     <!--  <div class="form-group col-md-4">

                       {!!Form::label('medecin_id', 'medecins')!!}
                       {!! Form::select('medecin_id',$medecins,$consultation->medecin_id, ['class'=>'form-control']) !!}
                       
                       </div> -->

                 </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Envoyer</button>  <a class="btn btn-primary" href="{{ route('rdv') }}">Retour</a>
                  </div>

                  {!! Form::close() !!}
              </div><!-- /.box -->
                   <!--  <a class="btn btn-primary" href="{{ route('consultation.index',$consultation)}}">Retour <i class="fa fa-index"></i></a></td>  -->       
          </div>

@stop