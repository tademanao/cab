@extends("template")
  
  @section('content')

  @include('flash')

  <div class="row">
          <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Créer une consultation</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                {!!Form::open(['method'=>'POST', 'url'=>route('consultation.store')]) !!}
                  <div class="box-body">

                         <div class="form-group col-md-4">
                            {!!Form::label('patient_id', 'patients')!!}
                            {!! Form::select('patient_id',$patients,null, ['class'=>'selectpicker form-control','id'=>'patient_id','data-live-search'=>'true', 'data-live-search-style'=>'startsWith' ]) !!}
                         </div>  
                         <div class="form-group col-md-4">
                           {!! Form::label('type_consultation_id', 'type consultation')!!}
                           {!! Form::select('type_consultation_id',$tab,null, ['class'=>'selectpicker form-control', 'data-live-search'=>'true', 'data-live-search-style'=>'startsWith' ]) !!}
                         </div> 

                         <div class="form-group col-md-4 {!! $errors->has('interrogatoire')?'has-error': '' !!}">
                         {!!Form::label('interrogatoire','interrogatoire')  !!}
                         {!! Form::textarea('interrogatoire',null, ['class'=>'form-control','id'=>'interrogatoire','placeholder'=>'','rows'=>'3']) !!}
                         {!! $errors->first('interrogatoire','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>
                         <div class="form-group col-md-4 {!! $errors->has('examen_clinique')?'has-error': '' !!}">
                         {!!Form::label('examen_clinique','examen clinique')  !!}
                         {!! Form::textarea('examen_clinique',null, ['class'=>'form-control','id'=>'examen_clinique','placeholder'=>'','rows'=>'5']) !!}
                          {!! $errors->first('examen_clinique','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>
                       
                         <div class="form-group col-md-4 {!! $errors->has('examen_paraclinique')?'has-error': '' !!}">
                         {!!Form::label('examen_paraclinique','examen paraclinique')  !!}
                         {!! Form::textarea('examen_paraclinique',null, ['class'=>'form-control','id'=>'examen_paraclinique','placeholder'=>'','rows'=>'5']) !!}
                          {!! $errors->first('examen_paraclinique','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>

                           <div class="form-group col-md-4 {!! $errors->has('diagnostic')?'has-error': '' !!}">
                         {!!Form::label('diagnostic','diagnostic')  !!}
                         {!! Form::text('diagnostic',null, ['class'=>'form-control','id'=>'diagnostic','placeholder'=>'']) !!}
                          {!! $errors->first('diagnostic','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>
                        
                         
                     <!--    <div class="form-group col-md-4 {!! $errors->has('traitement')?'has-error': '' !!}">
                          {!!Form::label('traitement','traitement')  !!}
                          {!!Form::textarea('traitement',null,['class'=>'form-control','placeholder'=>'traitement','rows'=>'8'])  !!}
                           {!! $errors->first('traitement','<small class="help-block"><strong>:message</strong></small>') !!}
                        </div> -->

                         <div class="form-group col-md-4 {!! $errors->has('date_rdv')?'has-error': '' !!}">
                            {!!Form::label('date_rdv','date rendez-vous')!!}
                            {!! Form::input('date','date_rdv', date('Y-m-d'),['class'=>'form-control','id'=>'date_rdv','placeholder'=>'', 'row'=>8]) !!}
                            {!! $errors->first('date_rdv','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div> 
                     
                      

                     

                        <!--   <div class="form-group col-md-4">

                           {!!Form::label('medecin_id', 'medecins')!!}
                           {!! Form::select('medecin_id',$medecins,null, ['class'=>'form-control']) !!}
                           
                           </div> -->

                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    @can('isMedecin')
                    <button type="submit" class="btn btn-primary">Envoyer</button>
                    @endcan
                  </div>

                  {!! Form::close() !!}
              </div><!-- /.box -->
                  
          </div>

          <div id="constante">
            


          </div>
          <div id="consultation">
            


          </div>


         <div class="col-md-12">
          <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Liste des consultations</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                         <th>Code</th>
                         <th>Patient</th>
                         <th>Diagnostic</th>
                         <th>Rendez-vous</th>
                         <th>Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                     
                    @foreach($consultations as $consultation) 

                    <tr> 

                         <td>{{ $consultation->id }}</td>
                         <td>{{ $consultation->patient->nom}}  {{ $consultation->patient->prenom}}</td>
                         <td>{{ $consultation->diagnostic}}</td>
                         <td>{{ date('d-m-Y',strtotime($consultation->date_rdv)) }}</td>
                        
                      

                         <td>
                          @can('isMedecin')
                          <a class="btn btn-xs btn-primary" href="{{ route('consultation.show',$consultation)}}">Détail</a>
                          @endcan
                          @if( (Auth::user()->role=='Admin') OR ( Auth::user()->role=='Médecin' && Auth::user()->userable_id == $consultation->medecin_id) )
                          <a class="btn btn-xs btn-warning" href="{{ route('consultation.edit',$consultation)}}">Editer</a> 
                          @endif
                          @can('isAdmin')<a class="btn btn-xs btn-danger" href="{{URL::to('/consultation/destroy/'.$consultation->id) }}" onclick="return confirm(' Voulez-vous Vraiment supprimer cet enregistrement?')">Supprimer</a>
                          @endcan
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                     <tfoot>
                      <tr>
                        <th>Code</th>
                        <th>Patient</th>
                        <th>Diagnostic</th>
                        <th>Rendez-vous</th>
                        <th>Actions</th>
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box --> 

  </div> 
</div> 



     
@stop

@section('scripts')


<script type="text/javascript">

  
     $("#patient_id").change(function(){

         var patient = $(this).val();
       

         $.ajax({
            url: 'http://localhost/cab/getConstante/' + patient,
            cache: false,
            type: 'GET',
            success: function(response){
              console.log('succes');
              $("#constante").html(response);
            },
            error: function(xhr){
              console.log('error');
            }
         });
          
     }); 


      $("#patient_id").change(function(){

         var patient = $(this).val();
       

         $.ajax({
            url: 'http://localhost/cab/getConsultation/' + patient,
            cache: false,
            type: 'GET',
            success: function(response){
              console.log('succes');
              $("#consultation").html(response);
            },
            error: function(xhr){
              console.log('error');
            }
         });
          
     });



</script>
@stop