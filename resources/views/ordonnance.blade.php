@extends("template")
  
  @section('content')

  @include('flash')

<div class="row">
          <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Ordonnance</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
               {!!Form::open(['method'=>'pot','url'=>('/consultation/ordonnance')]) !!}
                  <div class="box-body">
                    
                   <div class="form-group col-md-6">
                    {!!Form::label('consultation','consultation')  !!}
                    {!!Form::text('id',$consultation->id,['class'=>'form-control','rows'=>'8'])  !!}
                   </div>  

                  <!--  <div class="form-group col-md-6 {!! $errors->has('date_ordonnance')?'has-error': '' !!}">
                            {!!Form::label('date_ordonnance','date')!!}
                            {!! Form::input('date','date_ordonnance', date('Y-m-d'),['class'=>'form-control','id'=>'date_ordonnance','placeholder'=>'', 'row'=>8]) !!}
                            {!! $errors->first('date_ordonnance','<small class="help-block"><strong>:message</strong></small>') !!}
                   </div>  --> 

                   <div class="form-group col-md-6">

                           {!!Form::label('medicament_id', 'medicaments')!!}
                           {!! Form::select('medicament_id',$medicaments,null, ['class'=>'form-control']) !!}
                           
                   </div>

                    <div class="form-group col-md-6 {!! $errors->has('posologie')?'has-error': '' !!}">
                            {!!Form::label('posologie','posologie')!!}
                            {!! Form::text('posologie',null, ['class'=>'form-control','id'=>'posologie','placeholder'=>'']) !!}
                            {!! $errors->first('posologie','<small class="help-block"><strong>:message</strong></small>') !!}
                    </div>
                  
                    
                 </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Envoyer</button> &nbsp;<a class="btn btn-primary" href="{{ route('consultation.create',$consultation)}}">Retour <i class="fa fa-index"></i></a>
                  </div>

                  {!! Form::close() !!}
               </div><!-- /.box -->
                         
          </div>

          <div class="col-md-12">
            <div class="box box-primary">
                  <div class="box-header">
                    <h3 class="box-title"></h3>
                  </div><!-- /.box-header -->
                  <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                      <thead>
                        <tr>
                           <th>medicament</th>
                           <th>posologie</th>
                           <th>modifier</th>
                           <th>supprimer</th>
                        </tr>
                      </thead>
                      <tbody>
                       
                      
                      @foreach($ordonnances as $ordonnance) 

                      <tr> 
                           <td>{{ $ordonnance->libelle_medicament }}</td>
                           <td>{{ $ordonnance->posologie }}</td>
            
                      </tr>

                      @endforeach
                      </tbody>
                       <tfoot>
                        <tr>
                           <th>medicament</th>
                           <th>posologie</th>
                           <th>modifier</th>
                           <th>supprimer</th>
                        </tr>
                      </tfoot>
                    </table>
                  </div><!-- /.box-body -->
            </div><!-- /.box -->  
            <a class="btn btn-primary" href="#">Imprimer <i class="fa fa-index"></i></a>
          </div>
</div>

@stop