@extends("template")
  
  @section('content')

  @include('flash')

  <div class="row">
          <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Editer</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                {!!Form::open(['method'=>'PUT', 'url'=>route('produit.update',$produit)]) !!}

                  <div class="box-body">

                        <div class="form-group col-md-6 {!! $errors->has('libelle_produit')?'has-error': '' !!}">
                         {!!Form::label('libelle_produit','libelle')  !!}
                         {!! Form::text('libelle_produit',$produit->libelle_produit, ['class'=>'form-control','id'=>'libelle_produit','placeholder'=>'']) !!}
                         {!! $errors->first('libelle_produit','<small class="help-block"><strong>:message</strong></small>') !!}
                        </div>
                         
                        <div class="form-group col-md-6 {!! $errors->has('prix_produit')?'has-error': '' !!}">
                            {!!Form::label('prix_produit','prix')!!}
                            {!! Form::text('prix_produit',$produit->prix_produit, ['class'=>'form-control','id'=>'prix_produit','placeholder'=>'']) !!}
                            {!! $errors->first('prix_produit','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>


                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Envoyer</button> <a class="btn btn-primary" href="{{ route('produit.create',$produit) }}">Retour</a>
                  </div>

                  {!! Form::close() !!}
              </div><!-- /.box -->
                  
          </div>
  
  </div>

  

 

     
@stop