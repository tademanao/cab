@extends("template")
  
  @section('content')

  @include('flash')

  <div class="row">
          <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Créer</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                {!!Form::open(['method'=>'POST', 'url'=>route('produit.store')]) !!}

                  <div class="box-body">

                         <div class="form-group col-md-6 {!! $errors->has('libelle_produit')?'has-error': '' !!}">
                         {!!Form::label('libelle_produit','libelle')  !!}
                         {!! Form::text('libelle_produit',null, ['class'=>'form-control','id'=>'libelle_produit','placeholder'=>'']) !!}
                         {!! $errors->first('libelle_produit','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>
                         
                         <div class="form-group col-md-6 {!! $errors->has('prix_produit')?'has-error': '' !!}">
                            {!!Form::label('prix_produit','prix')!!}
                            {!! Form::text('prix_produit',null, ['class'=>'form-control','id'=>'prix_produit','placeholder'=>'']) !!}
                            {!! $errors->first('prix_produit','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>

                       

                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <br>
                    <button type="submit" class="btn btn-primary">Envoyer</button>
                  </div>

                  {!! Form::close() !!}
              </div><!-- /.box -->
                  
          </div>

          <div class="col-md-12">
            <div class="box box-primary">
                  <div class="box-header">
                    <h3 class="box-title">Liste des produits</h3>
                  </div><!-- /.box-header -->
                  <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                      <thead>
                        <tr>
                           <th>Code</th>
                           <th>libelle</th>
                           <th>Prix</th>
                           <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                       
                      
                    @foreach($produits as $produit) 

                      <tr> 
                           <td>{{ $produit->id }}</td>
                           <td>{{ $produit->libelle_produit }}</td>
                           <td>{{ $produit->prix_produit }}</td>

                          <td>
                           <a class="btn btn-xs btn-primary" href="{{ route('produit.show',$produit) }}">Détail</a> 
                           <a class="btn btn-xs btn-warning" href="{{ route('produit.edit',$produit) }}">Editer</a>
                           @can('isAdmin')
                           <a class="btn btn-xs btn-danger" href="{{URL::to('/produit/destroy/'.$produit->id) }}" onclick="return confirm(' Voulez-vous Vraiment supprimer cet enregistrement?')">Supprimer</a>@endcan
                         </td>
                      </tr>
                      @endforeach
                      </tbody>
                       <tfoot>
                        <tr>
                          <th>Code</th>
                           <th>libelle</th>
                           <th>Prix</th>
                           <th>Actions</th>
                        </tr>
                      </tfoot>
                    </table>
                  </div><!-- /.box-body -->
            </div><!-- /.box -->  
          </div>

         
  
  </div>

  

 

     
@stop