@extends("template")
  
  @section('content')

  @include('flash')

  <div class="row">
          <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Créer un patient</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                {!!Form::open(['method'=>'POST', 'url'=>route('patient.store')]) !!}

                  <div class="box-body">

                         <div class="form-group col-md-4 {!! $errors->has('nom')?'has-error': '' !!}">
                         {!!Form::label('nom','nom')  !!}
                         {!! Form::text('nom',null, ['class'=>'form-control','id'=>'nom','placeholder'=>'']) !!}
                         {!! $errors->first('nom','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>
                         
                         <div class="form-group col-md-4 {!! $errors->has('prenom')?'has-error': '' !!}">
                            {!!Form::label('prenom','prénom')!!}
                            {!! Form::text('prenom',null, ['class'=>'form-control','id'=>'prenom','placeholder'=>'']) !!}
                            {!! $errors->first('prenom','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>

                           <div class="form-group col-md-4 {!! $errors->has('profession')?'has-error': '' !!}">
                            {!!Form::label('profession','profession')!!}
                            {!! Form::text('profession',null, ['class'=>'form-control','id'=>'profession','placeholder'=>'']) !!}
                            {!! $errors->first('profession','<small class="help-block"><strong>:message</strong></small>') !!}
                           </div>

                         <div class="form-group col-md-4 {!! $errors->has('phone')?'has-error': '' !!}">
                            {!!Form::label('phone','téléphone')!!}
                            {!! Form::text('phone',null, ['class'=>'form-control','id'=>'phone','placeholder'=>'Exemple:22-25-91-20 ou 90-32-58-42']) !!}
                            {!! $errors->first('phone','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>
                         <div class="form-group col-md-4 {!! $errors->has('email')?'has-error': '' !!}">
                            {!!Form::label('email','email')!!}
                            {!! Form::email('email',null, ['class'=>'form-control','id'=>'email','placeholder'=>' Exemple:tademanao92.ot@gmail.com']) !!}
                            {!! $errors->first('email','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>

                          <div class="form-group col-md-4 {!! $errors->has('sexe')?'has-error': '' !!}">

                         {!!Form::label('sexe','sexe')!!}
                         {!! Form::select('sexe',['M'=>'masculin','F'=>'féminin'],null, ['class'=>'form-control','id'=>'sexe']) !!}

                         {!! $errors->first('sexe','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>

                          <div class="form-group col-md-4 {!! $errors->has('adresse')?'has-error': '' !!}">
                            {!!Form::label('adresse','adresse')!!}
                            {!! Form::text('adresse',null, ['class'=>'form-control','id'=>'adresse','placeholder'=>'', 'row'=>8]) !!}
                            {!! $errors->first('adresse','<small class="help-block"><strong>:message</strong></small>') !!}
                          </div>

                           <div class="form-group col-md-4 {!! $errors->has('date_naissance')?'has-error': '' !!}">
                            {!!Form::label('date_naissance','date naissance')!!}
                            {!! Form::input('date','date_naissance', date('Y-m-d'),['class'=>'form-control','id'=>'date_naissance','placeholder'=>'', 'row'=>8]) !!}
                            {!! $errors->first('date_naissance','<small class="help-block"><strong>:message</strong></small>') !!}
                          </div>  

                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    @can('isSecretaire')<button type="submit" class="btn btn-primary">Envoyer</button>@endcan
                  </div>

                  {!! Form::close() !!}
              </div><!-- /.box -->
                  
          </div>

          <div class="col-md-12">
            <div class="box box-primary">
                  <div class="box-header">
                    <h3 class="box-title">Liste des patients</h3>
                  </div><!-- /.box-header -->
                  <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                      <thead>
                        <tr>
                           <th>Code</th>
                           <th>Nom</th>
                           <th>Prenoms</th>
                           <th>Telephone</th>
                           <th>Email</th>
                           <th>Profession</th>
                           <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                       
                      
                    @foreach($patients as $patient) 

                      <tr> 

                           <td>{{ $patient->id }}</td>
                           <td>{{ $patient->nom}}</td>
                           <td>{{ $patient->prenom}}</td>
                           <td>{{ $patient->phone}}</td>
                           <td>{{ $patient->email}}</td>
                           <td>{{ $patient->profession}}</td>

                           <td>
                            <a class="btn btn-xs btn-primary" href="{{ route('patient.show',$patient) }}">Détail</a> 
                            @can('isSecretaire')
                            <a class="btn btn-xs btn-warning" href="{{ route('patient.edit',$patient) }}">Editer</a>
                            @endcan
                            @can('isAdmin')
                            <a class="btn btn-xs btn-danger" href="{{URL::to('/patient/destroy/'.$patient->id) }}" onclick="return confirm(' Voulez-vous Vraiment supprimer cet enregistrement?')">Supprimer</a>
                            </td>
                            @endcan
                        
                      </tr>
                      @endforeach
                      </tbody>
                       <tfoot>
                        <tr>
                          <th>Code</th>
                           <th>Nom</th>
                           <th>Prenoms</th>
                           <th>Telephone</th>
                           <th>Email</th>
                           <th>Profession</th>
                           <th>Actions</th>
                        </tr>
                      </tfoot>
                    </table>
                  </div><!-- /.box-body -->
            </div><!-- /.box -->  
          </div>

         
  
  </div>

  

 

     
@stop