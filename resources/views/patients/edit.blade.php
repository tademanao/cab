@extends("template")
  
  @section('content')

  @include('flash')

  <div class="row">
           <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Editer</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                {!!Form::open(['method'=>'PUT', 'url'=>route('patient.update',$patient)]) !!}

                  <div class="box-body">

                         <div class="form-group col-md-4 {!! $errors->has('nom')?'has-error': '' !!}">
                         {!!Form::label('nom','nom')  !!}
                         {!! Form::text('nom',$patient->nom, ['class'=>'form-control','id'=>'nom','placeholder'=>'']) !!}
                         {!! $errors->first('nom','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>
                         
                         <div class="form-group col-md-4 {!! $errors->has('prenom')?'has-error': '' !!}">
                            {!!Form::label('prenom','prénom')!!}
                            {!! Form::text('prenom',$patient->prenom, ['class'=>'form-control','id'=>'prenom','placeholder'=>'']) !!}
                            {!! $errors->first('prenom','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>

                          <div class="form-group col-md-4 {!! $errors->has('profession')?'has-error': '' !!}">
                            {!!Form::label('profession','profession')!!}
                            {!! Form::text('profession',$patient->profession, ['class'=>'form-control','id'=>'profession','placeholder'=>'']) !!}
                            {!! $errors->first('profession','<small class="help-block"><strong>:message</strong></small>') !!}
                          </div>

                         <div class="form-group col-md-4 {!! $errors->has('phone')?'has-error': '' !!}">
                            {!!Form::label('phone','téléphone')!!}
                            {!! Form::text('phone',$patient->phone, ['class'=>'form-control','id'=>'phone','placeholder'=>'']) !!}
                            {!! $errors->first('phone','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>
                         <div class="form-group col-md-4 {!! $errors->has('email')?'has-error': '' !!}">
                            {!!Form::label('email','email')!!}
                            {!! Form::email('email',$patient->email, ['class'=>'form-control','id'=>'email','placeholder'=>' Exemple:tademanao92.ot@gmail.com']) !!}
                            {!! $errors->first('email','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>

                         <div class="form-group col-md-4 {!! $errors->has('sexe')?'has-error': '' !!}">

                             {!!Form::label('sexe','sexe')!!}
                             {!! Form::select('sexe',['M'=>'masculin','F'=>'féminin'],$patient->sexe, ['class'=>'form-control','id'=>'sexe']) !!}

                             {!! $errors->first('sexe','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>

                          <div class="form-group col-md-4 {!! $errors->has('adresse')?'has-error': '' !!}">
                            {!!Form::label('adresse','adresse')!!}
                            {!! Form::text('adresse',$patient->adresse, ['class'=>'form-control','id'=>'adresse','placeholder'=>'', 'row'=>8]) !!}
                            {!! $errors->first('adresse','<small class="help-block"><strong>:message</strong></small>') !!}
                          </div>

                           <div class="form-group col-md-4 {!! $errors->has('date_naissance')?'has-error': '' !!}">
                            {!!Form::label('date_naissance','date naissance')!!}
                            {!! Form::input('date','date_naissance', $patient->date_naissance,['class'=>'form-control','id'=>'date_naissance','placeholder'=>'', 'row'=>8]) !!}
                            {!! $errors->first('date_naissance','<small class="help-block"><strong>:message</strong></small>') !!}
                          </div>  

                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Envoyer</button>  <a class="btn btn-primary" href="{{ route('patient.create',$patient) }}">Retour</a>
                  </div>

                  {!! Form::close() !!}
              </div><!-- /.box -->
                  
          </div>
  
  </div>

  

 

     
@stop