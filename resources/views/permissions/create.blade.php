@extends("template")
  
  @section('content')

  @include('flash')

  <div class="row">
          <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Créer</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                {!!Form::open(['method'=>'POST', 'url'=>route('permission.store')]) !!}

                  <div class="box-body">

                         <div class="form-group col-md-12 {!! $errors->has('name')?'has-error': '' !!}">
                         {!!Form::label('name','name')  !!}
                         {!! Form::text('name',null, ['class'=>'form-control','id'=>'name','placeholder'=>'']) !!}
                         {!! $errors->first('name','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>      

                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Envoyer</button>
                  </div>

                  {!! Form::close() !!}
              </div><!-- /.box -->
                  
          </div>

          <div class="col-md-12">
            <div class="box box-primary">
                  <div class="box-header">
                    <h3 class="box-title">Liste des permissions</h3>
                  </div><!-- /.box-header -->
                  <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                      <thead>
                        <tr>
                           <th>Code</th>
                           <th>name</th>
                           <th>modifier</th>
                           <th>supprimer</th>
                        </tr>
                      </thead>
                      <tbody>
                       
                      
                    @foreach($permissions as $permission) 

                      <tr> 
                           <td>{{ $permission->id }}</td>
                           <td>{{ $permission->name}}</td>
          
                           <td><a class="btn btn-primary" href="{{ route('permission.edit',$permission) }}">Editer</a></td>
                           <td>
                            {{Form::open(['method'=>'delete', 'url'=>route('permission.destroy',$permission)]) }}
    
                            <button class="btn btn-danger" onclick="return confirm(' Voulez-vous Vraiment supprimer cet enregistrement?')" >Supprimer</button>
                              {!! Form::close() !!}
                           </td>
                      </tr>
                      @endforeach
                      </tbody>
                       <tfoot>
                        <tr>
                          <th>Code</th>
                           <th>name</th>
                           <th>modifier</th>
                           <th>supprimer</th>
                        </tr>
                      </tfoot>
                    </table>
                  </div><!-- /.box-body -->
            </div><!-- /.box -->  
          </div>

         
  
  </div>

  

 

     
@stop