@extends("template")
  
  @section('content')

  @include('flash')

  <div class="row">
      
         <div class="col-md-12">
          <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Dernière consultation</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table  class="table table-bordered table-striped">
                    <thead>
                      <tr>
                         <th>Code</th>
                         <th>Diagnostic</th>
                         <th>Date</th>
                         <th>Rendez-vous</th>
                         <th>Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                     
                    @foreach($consultations as $consultation) 

                    <tr> 

                         <td>{{ $consultation->id }}</td>
                         <td>{{ $consultation->diagnostic}}</td>
                         <td>{{ date('d-m-Y',strtotime($consultation->date_consultation)) }}</td>
                         <td>{{ date('d-m-Y',strtotime( $consultation->date_rdv)) }}</td>
                         <td><a class="btn btn-xs btn-primary" href="{{ route('consultation.show',$consultation)}}">Détail</a>
                    @endforeach
                    </tbody>
                     <tfoot>
                      <tr>
                         <th>Code</th>
                         <th>Diagnostic</th>
                         <th>Date</th>
                         <th>Rendez-vous</th>
                         <th>Actions</th>
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box --> 

  </div> 
</div> 

     
@stop