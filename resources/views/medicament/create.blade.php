@extends("template")
  
  @section('content')

  @include('flash')

  <div class="row">
          <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Créer</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                {!!Form::open(['method'=>'POST', 'url'=>route('medicament.store')]) !!}

                  <div class="box-body">

                         <div class="form-group col-md-12 {!! $errors->has('libelle_medicament')?'has-error': '' !!}">
                         {!!Form::label('libelle_medicament','libelle')  !!}
                         {!! Form::text('libelle_medicament',null, ['class'=>'form-control','id'=>'libelle_medicament','placeholder'=>'Exemple: PARAFIZZ[500MG] cp effervescent']) !!}
                         {!! $errors->first('libelle_medicament','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>
                         

                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Envoyer</button>
                  </div>

                  {!! Form::close() !!}
              </div><!-- /.box -->
                  
          </div>

          <div class="col-md-12">
            <div class="box box-primary">
                  <div class="box-header">
                    <h3 class="box-title">Liste des médicaments</h3>
                  </div><!-- /.box-header -->
                  <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                      <thead>
                        <tr>
                           <th>Code</th>
                           <th>libelle</th>
                           <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                       
                      
                    @foreach($medicaments as $medicament) 

                      <tr> 
                           <td>{{ $medicament->id }}</td>
                           <td>{{ $medicament->libelle_medicament }}</td>

                          <td>
                            <a class="btn btn-xs btn-primary" href="{{ route('medicament.show',$medicament) }}">Détail</a> 
                            <a class="btn btn-xs btn-warning" href="{{ route('medicament.edit',$medicament) }}">Editer</a>
                            @can('isAdmin')
                           <a class="btn btn-xs btn-danger" href="{{URL::to('/medicament/destroy/'.$medicament->id) }}" onclick="return confirm(' Voulez-vous Vraiment supprimer cet enregistrement?')">Supprimer</a>@endcan</td>
                      </tr>
                      @endforeach
                      </tbody>
                       <tfoot>
                        <tr>
                          <th>Code</th>
                           <th>libelle</th>
                           <th>Actions</th>
                        </tr>
                      </tfoot>
                    </table>
                  </div><!-- /.box-body -->
            </div><!-- /.box -->  
          </div>

         
  
  </div>

  

 

     
@stop