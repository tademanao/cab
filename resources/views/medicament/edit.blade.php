@extends("template")
  
  @section('content')

  @include('flash')

  <div class="row">
          <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Editer</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                {!!Form::open(['method'=>'PUT', 'url'=>route('medicament.update',$medicament)]) !!}

                  <div class="box-body">

                        <div class="form-group col-md-12 {!! $errors->has('libelle_medicament')?'has-error': '' !!}">
                         {!!Form::label('libelle_medicament','libelle')  !!}
                         {!! Form::text('libelle_medicament',$medicament->libelle_medicament, ['class'=>'form-control','id'=>'libelle_medicament','placeholder'=>'']) !!}
                         {!! $errors->first('libelle_medicament','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>

                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Envoyer</button> <a class="btn btn-primary" href="{{ route('medicament.create',$medicament) }}">Retour</a>
                  </div>

                  {!! Form::close() !!}
              </div><!-- /.box -->
                  
          </div>
  
  </div>

  

 

     
@stop