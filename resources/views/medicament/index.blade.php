@extends("template")
  
  @section('content')

<div class="col-md-12">
          <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Liste des prix des medicaments</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                          <th>Code</th>
                         <th>libellé</th>
                        <th>conditionnement</th>
                         <th>prix unitaire</th>
                        <th>Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                     
                    @foreach($medicaments as $medicament) 

                    <tr> 

                         <td>{{ $medicament->id }}</td>
                         <td>{{ $medicament->libMed}}</td>
                         <td>{{ $medicament->conditionnement}}</td>
                         <td>{{ $medicament->pu}}</td>

                         <td><a class="btn btn-primary" href="{{ route('medicament.edit',$medicament)}}">Editer <i class="fa fa-edit"></i></a> </td>
                    </tr>
                    @endforeach
                    </tbody>
                     <tfoot>
                      <tr>
                          <th>Code</th>
                         <th>libellé</th>
                        <th>conditionnement</th>
                         <th>prix unitaire</th>
                        <th>Actions</th>
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box --> 

               <div><a class="btn btn-primary" href="{{ route('medicament.create',$medicament)}}">Créer un nouvel enregistrement <i class="fa fa-create"></i></a></div>   
     </div>         
</div> 

@stop


