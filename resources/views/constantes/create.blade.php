@extends("template")
  
  @section('content')

  @include('flash')

  <div class="row">
          <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Créer la constante</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                {!!Form::open(['method'=>'POST', 'url'=>route('constante.store')]) !!}
                  <div class="box-body">

                         <div class="form-group col-md-4">
                            {!!Form::label('patient_id', 'patients')!!}
                            {!! Form::select('patient_id',$patients,null, ['class'=>'selectpicker form-control', 'data-live-search'=>'true', 'data-live-search-style'=>'startsWith', ]) !!}
                          </div>  

                          <div class="form-group col-md-4 {!! $errors->has('temperature')?'has-error': '' !!}">

                           {!!Form::label('temperature','temperature')  !!}
                           {!! Form::text('temperature',null, ['class'=>'form-control','id'=>'temperature','placeholder'=>'Exemple:37.5','rows'=>'4']) !!}
                           {!! $errors->first('temperature','<small class="help-block"><strong>:message</strong></small>') !!}
                         
                         </div>

                         <div class="form-group col-md-4 {!! $errors->has('poids')?'has-error': '' !!}">
                            {!!Form::label('poids','poids')!!}
                            {!! Form::text('poids',null, ['class'=>'form-control','id'=>'poids','placeholder'=>'Exemple:90.5']) !!}
                            {!! $errors->first('poids','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>

                          <div class="form-group col-md-4 {!! $errors->has('taille')?'has-error': '' !!}">
                         {!!Form::label('taille','taille')  !!}
                         {!! Form::text('taille',null, ['class'=>'form-control','id'=>'taille','placeholder'=>'Exemple:1.75','rows'=>'4']) !!}
                          {!! $errors->first('taille','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>
                          <div class="form-group col-md-4 {!! $errors->has('tension_bras_gauche')?'has-error': '' !!}">
                         {!!Form::label('tension_bras_gauche','tension bras gauche')  !!}
                         {!! Form::text('tension_bras_gauche',null, ['class'=>'form-control','id'=>'tension_bras_gauche','placeholder'=>'Exemple:120/80','rows'=>'4']) !!}
                          {!! $errors->first('tension_bras_gauche','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>
                          <div class="form-group col-md-4 {!! $errors->has('tension_bras_droit')?'has-error': '' !!}">
                         {!!Form::label('tension_bras_droit','tension bras droit')  !!}
                         {!! Form::text('tension_bras_droit',null, ['class'=>'form-control','id'=>'tension_bras_droit','placeholder'=>'Exemple:100/70','rows'=>'4']) !!}
                          {!! $errors->first('tension_bras_droit','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>

                          <div class="form-group col-md-4 {!! $errors->has('poul')?'has-error': '' !!}">
                            {!!Form::label('poul','poul')!!}
                            {!! Form::text('poul',null, ['class'=>'form-control','id'=>'poul','placeholder'=>'Exemple:80']) !!}
                            {!! $errors->first('poul','<small class="help-block"><strong>:message</strong></small>') !!}
                          </div>

                         

                  </div><!-- /.box-body -->

                  <div class="box-footer">

                   @can('isInfirmiere') <button type="submit" class="btn btn-primary">Envoyer</button>@endcan
                  </div>

                  {!! Form::close() !!}
              </div><!-- /.box -->
                  
          </div>


          <div class="col-md-12">
          <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Liste des constantes</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Code</th>
                        <th>Patient</th>
                        <th>Temperature</th>
                        <th>Poids</th>
                        <th>Tension bras gauche</th>
                        <th>Tension bras droit</th>
                        <th>Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                     
                    @foreach($constantes as $constante) 

                    <tr> 

                         <td>{{ $constante->id }}</td>
                         <td>{{ $constante->patient->nom}}  {{ $constante->patient->prenom}}</td>
                         <td>{{ $constante->temperature}}</td>
                         <td>{{ $constante->poids }}</td>
                         <td>{{ $constante->tension_bras_gauche}}</td>
                         <td>{{ $constante->tension_bras_droit}}</td>
                         
                      

                        <td>
                          <a class="btn btn-xs btn-primary" href="{{ route('constante.show',$constante) }}">Détail</a> 
                          @can('isInfirmiere')
                          <a class="btn btn-xs btn-warning" href="{{ route('constante.edit',$constante) }}">Editer</a>
                          @endcan
                          @can('isAdmin')
                          <a class="btn btn-xs btn-danger" href="{{URL::to('/constante/destroy/'.$constante->id) }}" onclick="return confirm(' Voulez-vous Vraiment supprimer cet enregistrement?')">Supprimer</a>
                          @endcan
                          </td>
                    </tr>
                    @endforeach
                    </tbody>
                     <tfoot>
                      <tr>
                        <th>Code</th>
                        <th>Patient</th>
                        <th>Temperature</th>
                        <th>Poids</th>
                        <th>Tension bras gauche</th>
                        <th>Tension bras droit</th>
                        <th>Actions</th>
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box --> 
     </div>         
</div> 

     
@stop