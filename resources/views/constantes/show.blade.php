@extends("template")
  
  @section('content')

  @include('flash')

  <div class="row">
          <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Détail</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                {!!Form::open(['method'=>'put', 'url'=>route('constante.update',$constante)]) !!}
                  <div class="box-body">

                         <div class="form-group col-md-4">
                           {!!Form::label('patient_id', 'patients')!!}
                           {!! Form::select('patient_id',$patients,$constante->patient_id, ['class'=>'selectpicker form-control', 'data-live-search'=>'true', 'data-live-search-style'=>'startsWith','disabled'=>'disabled']) !!}
                         </div>

                         <div class="form-group col-md-4 {!! $errors->has('temperature')?'has-error': '' !!}">
                         {!!Form::label('temperature','temperature')  !!}
                         {!! Form::text('temperature',$constante->temperature, ['class'=>'form-control','id'=>'temperature','placeholder'=>'','rows'=>'4','readOnly'=>'readOnly']) !!}
                         {!! $errors->first('temperature','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>
                         <div class="form-group col-md-4 {!! $errors->has('poids')?'has-error': '' !!}">
                         {!!Form::label('poids','poids')  !!}
                         {!! Form::text('poids',$constante->poids, ['class'=>'form-control','id'=>'poids','placeholder'=>'','rows'=>'4','readOnly'=>'readOnly']) !!}
                         {!! $errors->first('poids','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>
                         <div class="form-group col-md-4 {!! $errors->has('taille')?'has-error': '' !!}">
                         {!!Form::label('taille','taille')  !!}
                         {!! Form::text('taille',$constante->taille, ['class'=>'form-control','id'=>'taille','placeholder'=>'description','rows'=>'4','readOnly'=>'readOnly']) !!}
                         {!! $errors->first('taille','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>
                         <div class="form-group col-md-4 {!! $errors->has('tension_bras_gauche')?'has-error': '' !!}">
                         {!!Form::label('tension_bras_gauche','tension bras gauche')  !!}
                         {!! Form::text('tension_bras_gauche',$constante->tension_bras_gauche, ['class'=>'form-control','id'=>'tension_bras_gauche','placeholder'=>'','rows'=>'4','readOnly'=>'readOnly']) !!}
                         {!! $errors->first('tension_bras_gauche','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>

                         <div class="form-group col-md-4 {!! $errors->has('tension_bras_droit')?'has-error': '' !!}">
                         {!!Form::label('tension_bras_droit','tension bras droit')  !!}
                         {!! Form::text('tension_bras_droit',$constante->tension_bras_droit, ['class'=>'form-control','id'=>'tension_bras_droit','placeholder'=>'','rows'=>'4','readOnly'=>'readOnly']) !!}
                          {!! $errors->first('tension_bras_droit','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>
                          <div class="form-group col-md-4 {!! $errors->has('poul')?'has-error': '' !!}">
                            {!!Form::label('poul','poul')!!}
                            {!! Form::text('poul',$constante->poul, ['class'=>'form-control','id'=>'poul','placeholder'=>'','readOnly'=>'readOnly']) !!}
                            {!! $errors->first('poul','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>
                         
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                  <a class="btn btn-primary" href="{{ route('constante.create',$constante) }}">Retour</a>
                  </div>

                  {!! Form::close() !!}
              </div><!-- /.box -->
            
                   <!--  <a class="btn btn-primary" href="{{ route('constante.create',$constante)}}">Retour <i class="fa fa-index"></i></a></td> -->
                  
          </div>

@stop

