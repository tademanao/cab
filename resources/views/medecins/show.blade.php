@extends("template")
  
  @section('content')
  
  @include('flash')

  <div class="row">
          <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Détail</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                {!!Form::open(['method'=>'put', 'url'=>route('medecin.update',$medecin)]) !!}

                  <div class="box-body">
                        
                         <div class="form-group col-md-4 {!! $errors->has('lastname')?'has-error': '' !!}">
                         {!!Form::label('lastname','nom')  !!}
                         {!! Form::text('lastname', $medecin->user->lastname, ['class'=>'form-control','id'=>'lastname','placeholder'=>'lastname','readOnly'=>'readOnly']) !!}
                         {!! $errors->first('lastname','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>
                         
                         <div class="form-group col-md-4 {!! $errors->has('firstname')?'has-error': '' !!}">
                            {!!Form::label('firstname','prénom')!!}
                            {!! Form::text('firstname',$medecin->user->firstname, ['class'=>'form-control','id'=>'firstname','placeholder'=>'firstnames','readOnly'=>'readOnly']) !!}
                            {!! $errors->first('firstname','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>

                           <div class="form-group col-md-4 {!! $errors->has('sexe')?'has-error': '' !!}">

                         {!!Form::label('sexe','sexe')!!}
                         {!! Form::select('sexe',['M'=>'masculin','F'=>'feminin'],$medecin->user->sexe, ['class'=>'form-control','id'=>'sexe','readOnly'=>'readOnly']) !!}

                         {!! $errors->first('sexe','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>

                         <div class="form-group col-md-4 {!! $errors->has('phone')?'has-error': '' !!}">
                            {!!Form::label('phone','téléphone')!!}
                            {!! Form::text('phone',$medecin->user->phone, ['class'=>'form-control','id'=>'phone','placeholder'=>'Téléphone','readOnly'=>'readOnly']) !!}
                            {!! $errors->first('phone','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>
                         <div class="form-group col-md-4 {!! $errors->has('email')?'has-error': '' !!}">
                            {!!Form::label('email','email')!!}
                            {!! Form::email('email',$medecin->user->email, ['class'=>'form-control','id'=>'email','placeholder'=>'Email','readOnly'=>'readOnly']) !!}
                            {!! $errors->first('email','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>

                         <div class="form-group col-md-4 {!! $errors->has('username')?'has-error': '' !!}">
                            {!!Form::label('username','nom utilisateur')!!}
                            {!! Form::text('username',$medecin->user->username, ['class'=>'form-control','id'=>'username','placeholder'=>'username','readOnly'=>'readOnly']) !!}
                            {!! $errors->first('username','<small class="help-block"><strong>:message</strong></small>') !!}
                          </div>

                          <div class="form-group col-md-4 {!! $errors->has('address')?'has-error': '' !!}">
                            {!!Form::label('address','adresse')!!}
                            {!! Form::text('address',$medecin->user->address, ['class'=>'form-control','id'=>'address','placeholder'=>'address', 'row'=>8,'readOnly'=>'readOnly']) !!}
                            {!! $errors->first('address','<small class="help-block"><strong>:message</strong></small>') !!}
                          </div>

                          <div class="form-group col-md-4 {!! $errors->has('sexe')?'has-error': '' !!}">
                             {!!Form::label('role','role')!!}
                             {!! Form::select('role',['Admin'=>'Admin','Médecin'=>'Médecin','Secrétaire'=>'Secrétaire','Infirmière'=>'Infirmière'],null, ['class'=>'form-control','id'=>'sexe','readOnly'=>'readOnly']) !!}

                             {!! $errors->first('role','<small class="help-block"><strong>:message</strong></small>') !!}
                          </div>

                  </div><!-- /.box-body -->

                  <div class="box-footer">
                   <a class="btn btn-primary" href="{{ route('medecin.create',$medecin) }}">Retour</a>
                  </div>

                  {!! Form::close() !!}
              </div><!-- /.box -->
                  
          </div>
  
  </div>

@stop

