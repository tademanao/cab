@extends("template")
  
  @section('content')

  @include('flash')

  <div class="row">
          <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Créer</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                {!!Form::open(['method'=>'POST', 'url'=>route('antecedent.store')]) !!}
                  <div class="box-body">

                       <div class="form-group col-md-6">
                            {!!Form::label('type antecedent','type antecedent')!!}
                            {!! Form::select('type_antecedent_id',$typeAntecedents,null, ['class'=>'form-control','id'=>'patient_id']) !!}
                       </div>
                       <div class="form-group col-md-6">
                             {!!Form::label('patient','patient')!!}
                            {!! Form::select('patient_id',$patients,null, ['class'=>'form-control','id'=>'patient_id']) !!}
                       </div>

                        <div class="form-group col-md-6 {!! $errors->has('description')?'has-error': '' !!}">
                         {!!Form::label('description','description')  !!}
                         {!! Form::textarea('description',null, ['class'=>'form-control','id'=>'description','placeholder'=>'','rows'=>'6']) !!}
                         {!! $errors->first('description','<small class="help-block"><strong>:message</strong></small>') !!}
                        </div>
                         
                         

                  </div><!-- /.box-body -->

                  <div class="box-footer">

                    <button type="submit" class="btn btn-primary">Envoyer</button>
                  </div>

                  {!! Form::close() !!}
              </div><!-- /.box -->
                  
          </div>


            <div class="col-md-12">
          <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Liste des Antécédents</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Code</th>
                        <th>Libelle</th>
                        <th>description</th>
                        <th>patient</th>
                        <th>modifier</th>
                        <th>supprimer</th>
                      </tr>
                    </thead>
                    <tbody>
                     
                    @foreach($antecedents as $antecedent) 

                    <tr> 

                         <td>{{ $antecedent->id }}</td>
                         <td>{{ $antecedent->typeAntecedent->libelle }}</td>
                         <td>{{ $antecedent->description}}</td>
                         <td>{{ $antecedent->patient->nom}}  {{ $antecedent->patient->prenom}}</td>
                      

                         <td><a class="btn btn-primary" href="{{ route('antecedent.edit',$antecedent)}}">Editer <i class="fa fa-edit"></i></a> </td>
                         <td>
                            {{Form::open(['method'=>'delete', 'url'=>route('antecedent.destroy',$antecedent)]) }}
    
                            <button class="btn btn-danger" onclick="return confirm(' Voulez-vous Vraiment supprimer cet enregistrement?')" >Supprimer</button>
                              {!! Form::close() !!}
                         </td>
                    </tr>
                    @endforeach
                    </tbody>
                     <tfoot>
                      <tr>
                        <th>Code</th>
                        <th>Libelle</th>
                        <th>description</th>
                        <th>patient</th>
                        <th>modifier</th>
                        <th>supprimer</th>
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box --> 
 
     </div>        
</div> 

     
@stop