@extends("template")
  
  @section('content')

  	<div class="col-md-12">
          <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Liste des Antécédents</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                          <th>Code</th>
                         <th>description</th>
                        <th>patient</th>
                        <th>modifier</th>
                        <th>supprimer</th>
                      </tr>
                    </thead>
                    <tbody>
                     
                    @foreach($antecedents as $antecedent) 

                    <tr> 

                         <td>{{ $antecedent->id }}</td>
                         <td>{{ $antecedent->description}}</td>
                         <td>{{ $antecedent->patient_id}}</td>
                      

                         <td><a class="btn btn-primary" href="{{ route('antecedent.edit',$antecedent)}}">Editer <i class="fa fa-edit"></i></a> </td>
                    </tr>
                    @endforeach
                    </tbody>
                     <tfoot>
                      <tr>
                        <th>Code</th>
                        <th>description</th>
                        <th>patient</th>
                        <th>modifier</th>
                        <th>supprimer</th>
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box --> 

               <div><a class="btn btn-primary" href="{{ route('antecedent.create',$antecedent)}}">Créer un nouvel enregistrement <i class="fa fa-create"></i></a></div>   
     </div>         
</div> 

@stop


