@extends("template")
  
  @section('content')

  @include('flash')

  <div class="row">
          <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Editer</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                {!!Form::open(['method'=>'put', 'url'=>route('antecedent.update',$antecedent)]) !!}
                  <div class="box-body">
                         <div class="form-group col-md-6">
                            {!!Form::label('type antecedent','type antecedent')!!}
                            {!! Form::select('type_antecedent_id',$typeAntecedents,$antecedent->type_antecedent_id, ['class'=>'form-control','id'=>'patient_id']) !!}
                         </div>

                         <div class="form-group col-md-6 {!! $errors->has('description')?'has-error': '' !!}">
                         {!!Form::label('description','description')  !!}
                         {!! Form::textarea('description',$antecedent->description, ['class'=>'form-control','id'=>'description','placeholder'=>'','rows'=>'6']) !!}
                         {!! $errors->first('description','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>
                         
                         <div class="form-group col-md-6">
                            {!!Form::label('patient','patient')!!}
                            {!! Form::select('patient_id',$patients,$antecedent->patient_id, ['class'=>'form-control','id'=>'patient_id']) !!}
                         </div>

                  </div><!-- /.box-body -->

                  <div class="box-footer">

                    <button type="submit" class="btn btn-primary">Envoyer</button>
                  </div>

                  {!! Form::close() !!}
              </div><!-- /.box -->
            
                   <!--  <a class="btn btn-primary" href="{{ route('antecedent.create',$antecedent)}}">Retour <i class="fa fa-index"></i></a></td> -->
                  
          </div>

@stop

