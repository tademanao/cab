<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Cabinet</title>
    <style type="text/css">
      
      .help-block{

        position: absolute;
      }
      .form-group{
        margin: 10px 0 0 0;
      }

    </style>

    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link href="{!!asset('resources/bootstrap/css/bootstrap.min.css')!!}" rel="stylesheet" type="text/css" />    
    <link href="{!!asset('resources/bootstrap/css/bootstrap-select.min.css')!!}" rel="stylesheet" type="text/css" />  
    <!-- FontAwesome 4.3.0 -->
    <link href="{!!asset('resources/assets/font-awesome/css/font-awesome.min.css') !!}" rel="stylesheet" type="text/css" />
    <!-- Ionicons 2.0.0 -->
    <link href="{!!asset('resources/assets/ionicons.min.css') !!}" rel="stylesheet" type="text/css" />    
    <!-- Theme style -->
    <link href="{!!asset('resources/dist/css/AdminLTE.min.css') !!}" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins 
         folder instead of downloading all of them to reduce the load. -->
    <link href="{!!asset('resources/dist/css/skins/_all-skins.min.css') !!}" rel="stylesheet" type="text/css" />
    <!-- iCheck -->
    <link href="{!!asset('resources/plugins/iCheck/flat/blue.css') !!}" rel="stylesheet" type="text/css" />
    <!-- Morris chart -->
    <link href="{!!asset('resources/plugins/morris/morris.css') !!}" rel="stylesheet" type="text/css" />
    <!-- jvectormap -->
    <link href="{!!asset('resources/plugins/jvectormap/jquery-jvectormap-1.2.2.css') !!}" rel="stylesheet" type="text/css" />
    <!-- Date Picker -->
    <link href="{!!asset('resources/plugins/datepicker/datepicker3.css') !!}" rel="stylesheet" type="text/css" />
    <!-- Daterange picker -->
    <link href="{!!asset('resources/plugins/daterangepicker/daterangepicker-bs3.css') !!}" rel="stylesheet" type="text/css" />
    <!-- bootstrap wysihtml5 - text editor -->
    <link href="{!!asset('resources/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') !!}" rel="stylesheet" type="text/css" />
      <!-- DATA TABLES -->
    <link href="{!!asset('resources/plugins/datatables/dataTables.bootstrap.css') !!}" rel="stylesheet" type="text/css" />
        <!-- jQuery 2.1.4 -->
    <script src="{!!asset('resources/plugins/jQuery/jQuery-2.1.4.min.js') !!}"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="skin-blue">
    <!-- Site wrapper -->
    <div class="wrapper">
      
      <header class="main-header">
        <a href="#" class="logo"><b>Ges</b>Cabinet</a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->
            
              <!-- Notifications: style can be found in dropdown.less -->

             <!--  <li class="dropdown notifications-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-bell-o"></i>
                  <span class="label label-warning">10</span>
                </a>
                <ul class="dropdown-menu">
                  <li class="header">You have 10 notifications</li>
                  <li>
                    inner menu: contains the actual data
                    <ul class="menu">
                      <li>
                        <a href="#">
                          <i class="fa fa-users text-aqua"></i> 5 new members joined today
                        </a>
                      </li>
                    </ul>
                  </li>
                  <li class="footer"><a href="#">View all</a></li>
                </ul>
              </li> -->
              <!-- Tasks: style can be found in dropdown.less -->

              <!-- User Account: style can be found in dropdown.less -->
              
              @auth
               <li class="">
                <a href="{{route('profil')}}">
                  <i class="fa fa-user"></i><span class="hidden-xs"> {{Auth::user()->username}}</span>
                </a>
              </li>

              <li class="">
                 <a href="{{route('logout')}}" 
                        onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Déconnexion') }}

                      </a>
                      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                               {{ csrf_field() }}
                      </form>
              </li>
              @endauth
            </ul>
          </div>
        </nav>
      </header>

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
         <!--  <div class="user-panel">
            <div class="pull-left image">
              <img src="../../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
              <p>Alexander Pierce</p>

              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div> -->
          <!-- search form -->
        <!--   <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </form> -->
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
             <li class="header">MENU PRINCIPAL</li>
             @if(Auth::user()->role=='Médecin' || Auth::user()->role=='Secrétaire' || Auth::user()->role=='Admin')
             <li><a href="{{ route('patient.create') }}"><i class="fa fa-user"></i>Patient</a></li>
             @endif
             @if(Auth::user()->role=='Médecin' || Auth::user()->role=='Infirmière' || Auth::user()->role=='Admin')
             <li><a href="{{ route('constante.create') }}"><i class="fa fa-heartbeat"></i>Constante</a></li>
             @endif
             @can('isMedecin')
             <li><a href="{{ route('consultation.create') }}"><i class="fa fa-stethoscope"></i>Consultation</a></li>
             @endcan
             @if(Auth::user()->role=='Médecin' || Auth::user()->role=='Secrétaire' || Auth::user()->role=='Admin')
             <li><a href="{{ route('rendez_vous') }}"><i class="fa fa-dashboard"></i>Rendez-vous</a></li>
             @endif
             
     <!--         <li><a href="#"><i class="fa fa-circle-o text-info"></i>Rendez-vous</a></li> -->

              <li class="treeview">
                <a href="#">
                  <i class="fa fa-gear fill"></i> <span>Paramètres</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                  <li>
                    <a href="{{ route('soin.create') }}"><i class="fa fa-circle-o"></i> Soin </a>
                    <a href="{{ route('analyse.create') }}"><i class="fa fa-circle-o"></i> Analyse </a>
                    <a href="{{ route('medicament.create') }}"><i class="fa fa-circle-o"></i> Médicament </a>
                    <a href="{{ route('produit.create') }}"><i class="fa fa-circle-o"></i> Produit & Consommable </a>
                    <a href="{{ route('typeConsultation.create') }}"><i class="fa fa-circle-o"></i> Type consultation </a>
                  </li>  
                </ul>
              </li>
              @can('isAdmin')
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-users"></i>
                  <span>Utilisateurs</span>
                  <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li>
                    <a href="{{ route('medecin.create') }}"><i class="fa fa-circle-o"></i> Médecin </a>
                    <a href="{{ route('secretaire.create') }}"><i class="fa fa-circle-o"></i> Secrétaire </a>
                    <a href="{{ route('infirmiere.create') }}"><i class="fa fa-circle-o"></i> Infirmière </a>
                  </li> 
                </ul>
              </li>
              @endcan
             <!--  <li class="treeview">
                <a href="#">
                  <i class="fa fa-user-o"></i>
                  <span>Role & Permissions</span>
                  <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li>
                    <a href="{{ route('role.create') }}"><i class="fa fa-circle-o"></i> Role </a>
                    </li> 
                </ul>
              </li> -->

               <li>
                @if(Auth::user()->role=='Secrétaire' || Auth::user()->role=='Admin')
                <a href="{{ route('etat') }}"><i class="fa fa-file"></i> Etat</a>
                @endif
              </li>
           
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>

       <style type="text/css">
        .content-wrapper {
          min-height: calc(100vh - 100px) !Important;
        }
      </style>

      <!-- =============================================== -->

      <!-- Right side column. Contains the navbar and content of the page -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <!-- <section class="content-header">
          <h1>
            Blank page
            <small>it all starts here</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Examples</a></li>
            <li class="active">Blank page</li>
          </ol>
        </section> -->

        <!-- Main content -->
        <section class="content">

           @yield('content')

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 1.0.0
        </div>
        <strong>Copyright &copy; 2019-2020 <a href="http://almsaeedstudio.com">GesCabinet</a>.</strong> All rights reserved.
      </footer>
    </div><!-- ./wrapper -->

      <!-- jQuery 2.1.4 -->
    <script src="{!!asset('resources/plugins/jQuery/jQuery-2.1.4.min.js') !!}" type="text/javascript"></script> 
    <!-- jQuery UI 1.11.2 -->
    <script src="{!!asset('resources/assets/jquery-ui.min.js') !!}" type="text/javascript"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <!-- page script -->
    <script type="text/javascript">
      $(function () {
        $("#example1").dataTable({

          "bSort": false,

        });
        $('#example2').dataTable({
          "bPaginate": true,
          "bLengthChange": false,
          "bFilter": false,
          "bSort": false,
          "bInfo": true,
          "bAutoWidth": false
        });
      });
    </script>
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="{!!asset('resources/bootstrap/js/bootstrap.min.js') !!}" type="text/javascript"></script>
    <script src="{!!asset('resources/bootstrap/js/bootstrap-select.min.js') !!}" type="text/javascript"></script>
     <!-- DATA TABES SCRIPT -->
    <script src="{!!asset('resources/plugins/datatables/jquery.dataTables.min.js') !!}" type="text/javascript"></script>
    <script src="{!!asset('resources/plugins/datatables/dataTables.bootstrap.min.js') !!}" type="text/javascript"></script>   
    <!-- Morris.js charts raphael 2.1.0 -->
    <script src="{!!asset('resources/assets/raphael-min.js') !!}"></script>
    <script src="{!!asset('resources/plugins/morris/morris.min.js') !!}" type="text/javascript"></script>
    <!-- Sparkline -->
    <script src="{!!asset('resources/plugins/sparkline/jquery.sparkline.min.js') !!}" type="text/javascript"></script>
    <!-- jvectormap -->
    <script src="{!!asset('resources/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') !!}" type="text/javascript"></script>
    <script src="{!!asset('resources/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') !!}" type="text/javascript"></script>
    <!-- jQuery Knob Chart -->
    <script src="{!!asset('resources/plugins/knob/jquery.knob.js') !!}" type="text/javascript"></script>
    <!-- daterangepicker moment 2.10.2 -->
    <script src="{!!asset('resources/assets/moment.min.js') !!}" type="text/javascript"></script>
    <script src="{!!asset('resources/plugins/daterangepicker/daterangepicker.js') !!}" type="text/javascript"></script>
    <!-- datepicker -->
    <script src="{!!asset('resources/plugins/datepicker/bootstrap-datepicker.js') !!}" type="text/javascript"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="{!!asset('resources/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') !!}" type="text/javascript"></script>
    <!-- Slimscroll -->
    <script src="{!!asset('resources/plugins/slimScroll/jquery.slimscroll.min.js') !!}" type="text/javascript"></script>
    <!-- FastClick -->
    <script src='plugins/fastclick/fastclick.min.js'></script>
    <!-- AdminLTE App -->
    <script src="{!!asset('resources/dist/js/app.min.js') !!}" type="text/javascript"></script>    
    
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="{!!asset('resources/dist/js/pages/dashboard.js') !!}" type="text/javascript"></script>    
    
    <!-- AdminLTE for demo purposes -->
    <script src="{!!asset('resources/dist/js/demo.js') !!}" type="text/javascript"></script>

    <script src="{!!asset('resources/assets/js/custom.js') !!}" type="text/javascript"></script>

    @yield('scripts')


  </body>
</html>