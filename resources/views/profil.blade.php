@extends("template")
  
  @section('content')
  
  @include('flash')

  @include('error')

  <div class="row">
          <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Modifier le profil</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                {!!Form::open(['method'=>'put', 'url'=>route('profil.update',$profil)]) !!}

                  <div class="box-body">
                        
                         <div class="form-group col-md-4 {!! $errors->has('lastname')?'has-error': '' !!}">
                         {!!Form::label('lastname','nom')  !!}
                         {!! Form::text('lastname', $profil->lastname, ['class'=>'form-control','id'=>'lastname','placeholder'=>'lastname']) !!}
                         {!! $errors->first('lastname','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>
                         
                         <div class="form-group col-md-4 {!! $errors->has('firstname')?'has-error': '' !!}">
                            {!!Form::label('firstname','prénom')!!}
                            {!! Form::text('firstname',$profil->firstname, ['class'=>'form-control','id'=>'firstname','placeholder'=>'firstnames']) !!}
                            {!! $errors->first('firstname','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>

                         <div class="form-group col-md-4 {!! $errors->has('phone')?'has-error': '' !!}">
                            {!!Form::label('phone','téléphone')!!}
                            {!! Form::text('phone',$profil->phone, ['class'=>'form-control','id'=>'phone','placeholder'=>'Téléphone']) !!}
                            {!! $errors->first('phone','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>

                         <div class="form-group col-md-4 {!! $errors->has('email')?'has-error': '' !!}">
                            {!!Form::label('email','email')!!}
                            {!! Form::email('email',$profil->email, ['class'=>'form-control','id'=>'email','placeholder'=>'Email']) !!}
                            {!! $errors->first('email','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>

                         <div class="form-group col-md-4 {!! $errors->has('username')?'has-error': '' !!}">
                            {!!Form::label('username','nom utilisateur')!!}
                            {!! Form::text('username',$profil->username, ['class'=>'form-control','id'=>'username','placeholder'=>'username']) !!}
                            {!! $errors->first('username','<small class="help-block"><strong>:message</strong></small>') !!}
                          </div>

                          <div class="form-group col-md-4 {!! $errors->has('address')?'has-error': '' !!}">
                            {!!Form::label('address','adresse')!!}
                            {!! Form::text('address',$profil->address, ['class'=>'form-control','id'=>'address','placeholder'=>'address']) !!}
                            {!! $errors->first('address','<small class="help-block"><strong>:message</strong></small>') !!}
                          </div>

                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Envoyer</button> <a class="btn btn-primary" href="javascript:history.back()">
                      <!-- <span class="glyphicon glyphicon-circle-arrow-left"></span> --> Retour
                      </a>
                  </div>

                  {!! Form::close() !!}
              </div><!-- /.box -->
                  
          </div>
  
  </div>

    <div class="row">
          <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Changement de mot de passe</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                {!!Form::open(['method'=>'post', 'url'=>route('password')]) !!}

                  <div class="box-body">

                         <div class="form-group col-md-4 {!! $errors->has('old_password')?'has-error': '' !!}">
                            {!!Form::label('old_password',' ancien mot de passe')!!}
                            {!! Form::password('old_password', ['class'=>'form-control','id'=>'old_password','placeholder'=>'', 'row'=>8]) !!}
                            {!! $errors->first('old_password','<small class="help-block"><strong>:message</strong></small>') !!}
                          </div>

                          <div class="form-group col-md-4 {!! $errors->has('password')?'has-error': '' !!}">
                            {!!Form::label('password','nouveau mot de passe')!!}
                            {!! Form::password('password', ['class'=>'form-control','id'=>'password','placeholder'=>'', 'row'=>8]) !!}
                            {!! $errors->first('password','<small class="help-block"><strong>:message</strong></small>') !!}
                          </div>

                          <div class="form-group col-md-4">
                            {!!Form::label('password-confirm','confirmation de mot de passe')!!}
                            {!! Form::password('password_confirmation', ['class'=>'form-control','id'=>'password-confirm', 'row'=>8]) !!}
                          </div>

                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Envoyer</button>  <a class="btn btn-primary" href="javascript:history.back()">
                      <!-- <span class="glyphicon glyphicon-circle-arrow-left"></span> --> Retour
                      </a>
                  </div>

                  {!! Form::close() !!}
              </div><!-- /.box -->
                  
          </div>
  
  </div>

@stop

