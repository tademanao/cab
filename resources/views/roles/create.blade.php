@extends("template")
  
  @section('content')

  @include('flash')

  <div class="row">
          <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Créer</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                {!!Form::open(['method'=>'POST', 'url'=>route('role.store')]) !!}

                  <div class="box-body">

                         <div class="form-group col-md-6 {!! $errors->has('name')?'has-error': '' !!}">
                         {!!Form::label('name','name')  !!}
                         {!! Form::text('name',null, ['class'=>'form-control','id'=>'name','placeholder'=>'']) !!}
                         {!! $errors->first('name','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>
                        

                       
                          <div class="col-xs-12 col-sm-12 col-md-12">

                           <!--  <div class="form-group">

                                <strong>Permissions:</strong>

                                <br/>

                                @foreach($permissions as $value)

                                    <label>{{ Form::checkbox('permissions[]', $value->id, false, array('class' => 'name')) }}

                                    {{ $value->name }}</label>

                                <br/>

                                @endforeach

                            </div> -->

                          </div>

                       <table id="example1" class="table table-bordered table-striped">
                          <thead>
                            <tr>
                               <th>Libelle</th>
                               <th></th>
                             
                            </tr>
                          </thead>
                          <tbody> 
                            @foreach($permissions as $value) 

                              <tr> 
                                   <td>{{ $value->name }}</td>
                                   <td>{{ Form::checkbox('permissions[]', $value->id, false, array('class' => 'name')) }}</td>
                              </tr>
                              @endforeach
                          </tbody>
                          <tfoot>
                            <tr>
                              <th>Libelle</th>
                              <th></th>
                            </tr>
                          </tfoot>
                       </table>

                          

                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Envoyer</button>
                  </div>

                  {!! Form::close() !!}
              </div><!-- /.box -->
                  
          </div>

          <div class="col-md-12">
            <div class="box box-primary">
                  <div class="box-header">
                    <h3 class="box-title">Liste des roles</h3>
                  </div><!-- /.box-header -->
                  <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                      <thead>
                        <tr>
                           <th>Code</th>
                           <th>name</th>
                           <th>modifier</th>
                           <th>supprimer</th>
                        </tr>
                      </thead>
                      <tbody>
                       
                      
                    @foreach($roles as $role) 

                      <tr> 
                           <td>{{ $role->id }}</td>
                           <td>{{ $role->name }}</td>
                  

                           <td><a class="btn btn-primary" href="{{ route('role.edit',$role) }}">Editer</a></td>
                           <td>
                            {{Form::open(['method'=>'delete', 'url'=>route('role.destroy',$role)]) }}
    
                            <button class="btn btn-danger" onclick="return confirm(' Voulez-vous Vraiment supprimer cet enregistrement?')" >Supprimer</button>
                              {!! Form::close() !!}
                           </td>
                      </tr>
                      @endforeach
                      </tbody>
                       <tfoot>
                        <tr>
                          <th>Code</th>
                           <th>name</th>
                           <th>modifier</th>
                           <th>supprimer</th>
                        </tr>
                      </tfoot>
                  </table>
                  </div><!-- /.box-body -->
            </div><!-- /.box -->  
          </div>

         
  
  </div>

  

 

     
@stop