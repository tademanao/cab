
<?php

if(!isset($soin->id)){
	$options = ['method'=>'post','url'=>action('SoinController@store')];
	
} else{

	$options = ['method'=>'put','url'=>action('SoinController@update',$soin)];
}

?>

@include('flash')

<div class="row">
          <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Créer</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                {!!Form::open($options)!!}

                  <div class="box-body">
                  			
                         <div class="form-group col-md-6 {!! $errors->has('libelle')?'has-error': '' !!}">
                         {!!Form::label('libelle','libelle')  !!}
                         {!! Form::text('libelle', $soin->libelle, ['class'=>'form-control','id'=>'libelle','placeholder'=>'libelle']) !!}
                         {!! $errors->first('libelle','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>
                         
                         <div class="form-group col-md-6 {!! $errors->has('prix_unitaire')?'has-error': '' !!}">
                            {!!Form::label('prix_unitaire','prix_unitaire')!!}
                            {!! Form::text('prix_unitaire',$soin->prix_unitaire, ['class'=>'form-control','id'=>'prix_unitaire','placeholder'=>'prix_unitaires']) !!}
                            {!! $errors->first('prix_unitaire','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>

                          
                       

                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Envoyer</button>
                  </div>

                  {!! Form::close() !!}
              </div><!-- /.box -->
                  
          </div>
  
  </div>
