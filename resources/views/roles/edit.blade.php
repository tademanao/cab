@extends("template")
  
  @section('content')

  @include('flash')

  <div class="row">
          <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Créer</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                {!!Form::open(['method'=>'PUT', 'url'=>route('role.update',$role)]) !!}

                  <div class="box-body">

                         <div class="form-group col-md-6 {!! $errors->has('name')?'has-error': '' !!}">
                         {!!Form::label('name','name')  !!}
                         {!! Form::text('name',$role->name, ['class'=>'form-control','id'=>'name','placeholder'=>'']) !!}
                         {!! $errors->first('name','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>
                        

                       
                       <div class="col-xs-12 col-sm-12 col-md-12">

                          <!-- <div class="form-group">

                              <strong>Permission:</strong>

                              <br/>

                              @foreach($permissions as $value)

                                  <label>{{ Form::checkbox('permissions[]', $value->id, in_array($value->id, $rolePermissions) ? true : false, array('class' => 'name')) }}

                                  {{ $value->name }}</label>

                              <br/>

                              @endforeach

                          </div> -->

                       </div>

                       <table id="example1" class="table table-bordered table-striped">
                          <thead>
                            <tr>
                               <th>Libelle</th>
                               <th></th>
                             
                            </tr>
                          </thead>
                          <tbody> 
                            @foreach($permissions as $value) 

                              <tr> 
                                   <td>{{ $value->name }}</td>
                                   <td>{{ Form::checkbox('permissions[]', $value->id, in_array($value->id, $rolePermissions) ? true : false, array('class' => 'name')) }}</td>
                              </tr>
                              @endforeach
                          </tbody>
                          <tfoot>
                            <tr>
                              <th>Libelle</th>
                              <th></th>
                            </tr>
                          </tfoot>
                       </table>

                          

                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Envoyer</button>
                  </div>

                  {!! Form::close() !!}
              </div><!-- /.box -->
                  
          </div>
         
  </div>

  

 

     
@stop