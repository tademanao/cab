@extends("template")
  
  @section('content')

  @include('flash')

  <div class="row">
          <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Créer</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                {!!Form::open(['method'=>'POST', 'url'=>route('assurance.store')]) !!}

                  <div class="box-body">

                         <div class="form-group col-md-6 {!! $errors->has('libelle')?'has-error': '' !!}">
                         {!!Form::label('libelle','libelle')  !!}
                         {!! Form::text('libelle',null, ['class'=>'form-control','id'=>'libelle','placeholder'=>'']) !!}
                         {!! $errors->first('libelle','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>
                         
                         <div class="form-group col-md-6 {!! $errors->has('part_assure')?'has-error': '' !!}">
                            {!!Form::label('part_assure','part assuré')!!}
                            {!! Form::text('part_assure',null, ['class'=>'form-control','id'=>'part_assure','placeholder'=>'']) !!}
                            {!! $errors->first('part_assure','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>
                         <div class="form-group col-md-6 {!! $errors->has('part_assureur')?'has-error': '' !!}">
                         {!!Form::label('part_assureur','part assureur')  !!}
                         {!! Form::text('part_assureur',null, ['class'=>'form-control','id'=>'part_assureur','placeholder'=>'']) !!}
                         {!! $errors->first('part_assureur','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>
                         <div class="form-group col-md-6 {!! $errors->has('type')?'has-error': '' !!}">

                         {!!Form::label('type','type assurance')!!}
                         {!! Form::select('type',['tiers'=>'tiers','comptant'=>'comptant'],null, ['class'=>'form-control','id'=>'type']) !!}

                         {!! $errors->first('type','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>

                          

                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Envoyer</button>
                  </div>

                  {!! Form::close() !!}
              </div><!-- /.box -->
                  
          </div>

          <div class="col-md-12">
            <div class="box box-primary">
                  <div class="box-header">
                    <h3 class="box-title">Liste des assurances</h3>
                  </div><!-- /.box-header -->
                  <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                      <thead>
                        <tr>
                           <th>Code</th>
                           <th>Libelle</th>
                           <th>Part assuré</th>
                           <th>Part assureur</th>
                           <th>Type</th>
                           <th>modifier</th>
                           <th>supprimer</th>
                        </tr>
                      </thead>
                      <tbody>
                       
                      
                    @foreach($assurances as $assurance) 

                      <tr> 
                           <td>{{ $assurance->id }}</td>
                           <td>{{ $assurance->libelle}}</td>
                           <td>{{ $assurance->part_assure}}</td>
                           <td>{{ $assurance->part_assureur}}</td>
                           <td>{{ $assurance->type}}</td>
                           <td><a class="btn btn-primary" href="{{ route('assurance.edit',$assurance) }}">Editer</a></td>
                           <td>
                            {{Form::open(['method'=>'delete', 'url'=>route('assurance.destroy',$assurance)]) }}
    
                            <button class="btn btn-danger" onclick="return confirm(' Voulez-vous Vraiment supprimer cet enregistrement?')" >Supprimer</button>
                              {!! Form::close() !!}
                           </td>
                      </tr>
                      @endforeach
                      </tbody>
                       <tfoot>
                        <tr>
                          <th>Code</th>
                           <th>Libelle</th>
                           <th>Part assuré</th>
                           <th>Part assureur</th>
                           <th>Type</th>
                           <th>modifier</th>
                           <th>supprimer</th>
                        </tr>
                      </tfoot>
                    </table>
                  </div><!-- /.box-body -->
            </div><!-- /.box -->  
          </div>

         
  
  </div>

  

 

     
@stop