@extends("template")
  
  @section('content')

  @include('flash')

  <div class="row">
          <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Créer</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                {!!Form::open(['method'=>'PUT', 'url'=>route('assurance.update',$assurance)]) !!}

                  <div class="box-body">

                         <div class="form-group col-md-6 {!! $errors->has('libelle')?'has-error': '' !!}">
                         {!!Form::label('libelle','libelle')  !!}
                         {!! Form::text('libelle',$assurance->libelle, ['class'=>'form-control','id'=>'libelle','placeholder'=>'']) !!}
                         {!! $errors->first('libelle','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>
                         
                         <div class="form-group col-md-6 {!! $errors->has('part_assure')?'has-error': '' !!}">
                            {!!Form::label('part_assure','prix assuré')!!}
                            {!! Form::text('part_assure',$assurance->part_assure, ['class'=>'form-control','id'=>'part_assure','placeholder'=>'']) !!}
                            {!! $errors->first('part_assure','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>
                          <div class="form-group col-md-6 {!! $errors->has('part_assureur')?'has-error': '' !!}">
                         {!!Form::label('part_assureur','part assureur')  !!}
                         {!! Form::text('part_assureur',$assurance->part_assureur, ['class'=>'form-control','id'=>'part_assureur','placeholder'=>'']) !!}
                         {!! $errors->first('part_assureur','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>
                         <div class="form-group col-md-6 {!! $errors->has('type')?'has-error': '' !!}">

                         {!!Form::label('type','type assurance')!!}
                         {!! Form::select('type',['tiers'=>'tiers','comptant'=>'comptant'],$assurance->type, ['class'=>'form-control','id'=>'type']) !!}

                         {!! $errors->first('type','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>

                          

                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Envoyer</button>
                  </div>

                  {!! Form::close() !!}
              </div><!-- /.box -->
                  
          </div>
  
  </div>

  

 

     
@stop