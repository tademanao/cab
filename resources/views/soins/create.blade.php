@extends("template")
  
  @section('content')

  @include('flash')

  <div class="row">
          <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Créer</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                {!!Form::open(['method'=>'POST', 'url'=>route('soin.store')]) !!}

                  <div class="box-body">

                         <div class="form-group col-md-6 {!! $errors->has('libelle_soin')?'has-error': '' !!}">
                         {!!Form::label('libelle_soin','libelle soin')  !!}
                         {!! Form::text('libelle_soin',null, ['class'=>'form-control','id'=>'libelle_soin','placeholder'=>'']) !!}
                         {!! $errors->first('libelle_soin','<small class="help-block"><strong>:message</strong><br></small>') !!}
                         </div>
                         
                         <div class="form-group col-md-6 {!! $errors->has('prix_soin')?'has-error': '' !!}">
                            {!!Form::label('prix_soin','prix unitaire')!!}
                            {!! Form::text('prix_soin',null, ['class'=>'form-control','id'=>'prix_soin','placeholder'=>'']) !!}
                            {!! $errors->first('prix_soin','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>

                         <div class="form-group col-md-6 {!! $errors->has('description')?'has-error': '' !!}">
                          <br>
                           {!!Form::label('description','description')  !!}
                           {!! Form::textarea('description',null, ['class'=>'form-control col-md-6','id'=>'description','placeholder'=>'','rows'=>'4']) !!}
                           {!! $errors->first('description','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>
                         
                       

                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Envoyer</button>
                  </div>

                  {!! Form::close() !!}
              </div><!-- /.box -->
                  
          </div>

          <div class="col-md-12">
            <div class="box box-primary">
                  <div class="box-header">
                    <h3 class="box-title">Liste des soins</h3>
                  </div><!-- /.box-header -->
                  <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                      <thead>
                        <tr>
                           <th>Code</th>
                           <th>libelle</th>
                           <th>Prix</th>
                          <!--  <th>description</th> -->
                           <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                       
                      
                    @foreach($soins as $soin) 

                      <tr> 
                           <td>{{ $soin->id }}</td>
                           <td>{{ $soin->libelle_soin }}</td>
                           <td>{{ $soin->prix_soin }}</td>
                           <!-- <td>{{ $soin->description }}</td> -->
                           <td><a class="btn btn-primary btn-xs" href="{{ route('soin.show',$soin) }}">Détail</a> <a class="btn btn-xs btn-warning" href="{{ route('soin.edit',$soin) }}">Editer</a>
                            @can('isAdmin')
                            <a class="btn btn-xs btn-danger" href="{{URL::to('/soin/destroy/'.$soin->id) }}" onclick="return confirm(' Voulez-vous Vraiment supprimer cet enregistrement?')">Supprimer</a> @endcan
                           </td>
                      </tr>
                      @endforeach
                      </tbody>
                       <tfoot>
                        <tr>
                          <th>Code</th>
                           <th>libelle</th>
                           <th>Prix</th>
                          <!--  <th>description</th> -->
                           <th>Actions</th>
                        </tr>
                      </tfoot>
                    </table>
                  </div><!-- /.box-body -->
            </div><!-- /.box -->  
          </div>

         
  
  </div>

  

 

     
@stop