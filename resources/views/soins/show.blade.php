@extends("template")
  
  @section('content')

  @include('flash')

  <div class="row">
          <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Détail</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                {!!Form::open(['method'=>'PUT', 'url'=>route('soin.update',$soin)]) !!}

                  <div class="box-body">

                        <div class="form-group col-md-6 {!! $errors->has('libelle_soin')?'has-error': '' !!}">
                         {!!Form::label('libelle_soin','libelle_soin')  !!}
                         {!! Form::text('libelle_soin',$soin->libelle_soin, ['class'=>'form-control','id'=>'libelle_soin','placeholder'=>'','readOnly'=>'readOnly']) !!}
                         {!! $errors->first('libelle_soin','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>
                         
                         <div class="form-group col-md-6 {!! $errors->has('prix_soin')?'has-error': '' !!}">
                            {!!Form::label('prix_soin','prix unitaire')!!}
                            {!! Form::text('prix_soin',$soin->prix_soin, ['class'=>'form-control','id'=>'prix_soin','placeholder'=>'','readOnly'=>'readOnly']) !!}
                            {!! $errors->first('prix_soin','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>

                         <div class="form-group col-md-6 {!! $errors->has('description')?'has-error': '' !!}">
                         {!!Form::label('description','description')  !!}
                         {!! Form::textarea('description',$soin->description, ['class'=>'form-control','id'=>'description','placeholder'=>'','rows'=>'4','readOnly'=>'readOnly']) !!}
                         {!! $errors->first('description','<small class="help-block"><strong>:message</strong></small>') !!}
                        </div>


                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <a class="btn btn-primary" href="{{ route('soin.create',$soin) }}">Retour</a>
                  </div>

                  {!! Form::close() !!}
              </div><!-- /.box -->
                  
          </div>
  
  </div>

  

 

     
@stop