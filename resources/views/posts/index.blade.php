@extends("adminTemplate")
  
  @section('content')
  @include('flash')
  
   <div class="col-md-12">
          <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Liste des membres</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                          <th>Code</th>
                         <th>nom</th>
                         <th>prenom</th>
                         <th>telephone</th>
                         <th>sexe</th>
                         <th>Modifier</th>
                         <th>Supprimer</th>
                      </tr>
                      </tr>
                    </thead>
                    <tbody>
                     
                   @foreach($membres as $membre) 

                    <tr> 

                         <td>{{ $membre->id }}</td>
                         <td>{{ $membre->nom}}</td>
                         <td>{{ $membre->prenom}}</td>
                         <td>{{ $membre->sexe}}</td>
                         <td>{{ $membre->tel}}</td>
                         
                      

                        <td><a class="btn btn-primary" href="{{ route('membre.edit',$membre) }}">Editer <i class="fa fa-edit"></i></a> </td>
                         
                          {{Form::open(['method'=>'delete', 'url'=>route('membre.destroy',$membre)]) }}
                          <td>
                          <button class="btn btn-danger" onclick="return confirm(' Voulez-vous Vraiment supprimer cet enregistrement?')" >Supprimer<i class="fa fa-trash"></i></button>
                          </td>
                            {!! Form::close() !!}
                    </tr>
                    @endforeach
                    </tbody>
                     <tfoot>
                      <tr>
                         <th>Code</th>
                         <th>nom</th>
                         <th>prenom</th>
                         <th>telephone</th>
                         <th>sexe</th>
                         <th>Modifier</th>
                         <th>Supprimer</th>
                      </tr>
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->  
     </div>
</div>       
     
@stop