@extends("template")
  
  @section('content')

  @include('flash')

  <div class="row">
          <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Créer</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                {!!Form::open(['method'=>'POST', 'url'=>route('post.store')]) !!}

                  <div class="box-body">

                         <div class="form-group col-md-12 {!! $errors->has('name')?'has-error': '' !!}">
                         {!!Form::label('name','name')  !!}
                         {!! Form::text('name',null, ['class'=>'form-control','id'=>'name','placeholder'=>'']) !!}
                         {!! $errors->first('name','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>      

                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Envoyer</button>
                  </div>

                  {!! Form::close() !!}
              </div><!-- /.box -->
                  
          </div>

          <div class="col-md-12">
            <div class="box box-primary">
                  <div class="box-header">
                    <h3 class="box-title">Liste des posts</h3>
                  </div><!-- /.box-header -->
                  <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                      <thead>
                        <tr>
                           <th>Code</th>
                           <th>name</th>
                           <th>modifier</th>
                           <th>supprimer</th>
                        </tr>
                      </thead>
                      <tbody>
                       
                      
                    @foreach($posts as $post) 

                      <tr> 
                           <td>{{ $post->id }}</td>
                           <td>{{ $post->name}}</td>
          
                           <td>@if($user->Userpermissions('post-edit'))<a class="btn btn-primary" href="{{ route('post.edit',$post) }}">Editer</a>@endif</td>
                           <td>
                            @if($user->Userpermissions('post-delete'))<a href="javascript:;" data-toggle="modal" 
                            onclick="deleteData({{$post->id}})" 
                             data-target="#DeleteModal" class="btn btn-danger">Supprimer</a>@endif
                           <!--  {{Form::open(['method'=>'delete', 'url'=>route('post.destroy',$post)]) }}
    
                            <button class="btn btn-danger" onclick="return confirm(' Voulez-vous Vraiment supprimer cet enregistrement?')" >Supprimer</button>
                              {!! Form::close() !!} -->
                           </td>
                      </tr>
                      @endforeach
                      </tbody>
                       <tfoot>
                        <tr>
                          <th>Code</th>
                           <th>name</th>
                           <th>modifier</th>
                           <th>supprimer</th>
                        </tr>
                      </tfoot>
                    </table>
                  </div><!-- /.box-body -->
            </div><!-- /.box -->  
          </div>

         
  
  </div>

  
<div id="DeleteModal" class="modal fade text-danger" role="dialog">
   <div class="modal-dialog ">
     <!-- Modal content-->
     <form action="" id="deleteForm" method="post">
         <div class="modal-content">
             <div class="modal-header bg-danger">
                 <button type="button" class="close" data-dismiss="modal">&times;</button>
                 <h4 class="modal-title text-center">CONFIRMATION</h4>
             </div>
             <div class="modal-body">
                 {{ csrf_field() }}
                 {{ method_field('DELETE') }}
                 <p class="text-center">Etes-vous sure de vouloir supprimer cet enregistrement ?</p>
             </div>
             <div class="modal-footer">
                 <center>
                     <button type="button" class="btn btn-success" data-dismiss="modal">Annuler</button>
                     <button type="submit" name="" class="btn btn-danger" data-dismiss="modal" onclick="formSubmit()">Supprimer</button>
                 </center>
             </div>
         </div>
     </form>
   </div>
  </div>


  <script type="text/javascript">
     function deleteData(id)
     {
         var id = id;
         var url = '{{ route("post.destroy", ":id") }}';
         url = url.replace(':id', id);
         $("#deleteForm").attr('action', url);
     }

     function formSubmit()
     {
         $("#deleteForm").submit();
     }
  </script>
 

     
@stop