@extends("template")
  
  @section('content')

  @include('flash')

  <div class="row">
          <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Créer</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                {!!Form::open(['method'=>'PUT', 'url'=>route('post.update',$post)]) !!}

                  <div class="box-body">

                         <div class="form-group col-md-12 {!! $errors->has('name')?'has-error': '' !!}">
                         {!!Form::label('name','name')  !!}
                         {!! Form::text('name',$post->name, ['class'=>'form-control','id'=>'name','placeholder'=>'']) !!}
                         {!! $errors->first('name','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>

                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Envoyer</button>
                  </div>

                  {!! Form::close() !!}
              </div><!-- /.box -->
                  
          </div>
  
  </div>

  

 

     
@stop