<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>cabinet</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.4 -->
    <link href="{!!asset('resources/bootstrap/css/bootstrap.min.css')!!}" rel="stylesheet" type="text/css" /><!-- FontAwesome 4.3.0 -->
    <link href="{!!asset('resources/assets/font-awesome/css/font-awesome.min.css') !!}" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="{!!asset('resources/dist/css/AdminLTE.min.css') !!}" rel="stylesheet" type="text/css" />
     <!-- iCheck -->
    <link href="{!!asset('resources/plugins/iCheck/flat/blue.css') !!}" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="login-page">
    <div class="login-box">
      <div class="login-logo">
        <a href="../../index2.html"><b>GES</b>CABINET</a>
      </div><!-- /.login-logo -->
      <div class="login-box-body">
        <h3 class="login-box-msg">Connexion</h3>
        <form method="POST" action="{{ route('login') }}">

          <div class="form-group has-feedback {{ $errors->has('username') || $errors->has('email') ? ' has-error' : '' }}">
            <input id="login" type="text" class="form-control" name="login" value="{{ old('username') ?: old('email') }}" placeholder="Email ou Nom d'utilisateur" required autofocus/>
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>

             @if ($errors->has('username') || $errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('username') ?: $errors->first('email') }}</strong>
                                    </span>
                                @endif
          </div>

          <div class="form-group has-feedback {{ $errors->has('password') ? ' has-error' : '' }}">
            <input type="password" class="form-control" name="password" placeholder="Mot de passe" required />
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>

             @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
          </div>

          <div class="row">
            <div class="col-xs-8">    
              <button type="submit" class="btn btn-primary btn-block btn-flat">Se connecter</button>              </div><!-- /.col -->
            <div class="col-xs-4">
              
            </div><!-- /.col -->
          </div>
        </form>

       <!-- /.social-auth-links -->

      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

     <!-- jQuery 2.1.4 -->
    <script src="{!!asset('resources/plugins/jQuery/jQuery-2.1.4.min.js') !!}" type="text/javascript"></script> 
    <!-- Bootstrap 3.3.2 JS -->
    <script src="{!!asset('resources/bootstrap/js/bootstrap.min.js') !!}" type="text/javascript"></script>
    <!-- iCheck -->
    <script src="{!!asset('resources/plugins/iCheck/icheck.min.js') !!}" type="text/javascript"></script>
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>
  </body>
</html>