@extends("template")
  
  @section('content')

  @include('flash')

  <div class="row">
          <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Créer</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                {!!Form::open(['method'=>'POST', 'url'=>route('analyse.store')]) !!}

                  <div class="box-body">

                         <div class="form-group col-md-6 {!! $errors->has('libelle_analyse')?'has-error': '' !!}">
                         {!!Form::label('libelle_analyse','libelle')  !!}
                         {!! Form::text('libelle_analyse',null, ['class'=>'form-control','id'=>'libelle_analyse','placeholder'=>'']) !!}
                         {!! $errors->first('libelle_analyse','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>
                         
                         <div class="form-group col-md-6 {!! $errors->has('prix_analyse')?'has-error': '' !!}">
                            {!!Form::label('prix_analyse','prix')!!}
                            {!! Form::text('prix_analyse',null, ['class'=>'form-control','id'=>'prix_analyse','placeholder'=>'']) !!}
                            {!! $errors->first('prix_analyse','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>

                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <br>
                    <button type="submit" class="btn btn-primary">Envoyer</button>
                  </div>

                  {!! Form::close() !!}
              </div><!-- /.box -->
                  
          </div>

          <div class="col-md-12">
            <div class="box box-primary">
                  <div class="box-header">
                    <h3 class="box-title">Liste des analyses</h3>
                  </div><!-- /.box-header -->
                  <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                      <thead>
                        <tr>
                           <th>Code</th>
                           <th>libelle</th>
                           <th>Prix</th>
                           <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                       
                      
                    @foreach($analyses as $analyse) 

                      <tr> 
                           <td>{{ $analyse->id }}</td>
                           <td>{{ $analyse->libelle_analyse }}</td>
                           <td>{{ $analyse->prix_analyse }}</td>

                          <td><a class="btn btn-xs btn-primary" href="{{ route('analyse.show',$analyse) }}">Détail</a> 
                            <a class="btn btn-xs btn-warning" href="{{ route('analyse.edit',$analyse) }}">Editer</a>
                            @can('isAdmin')
                            <a class="btn btn-xs btn-danger" href="{{URL::to('/analyse/destroy/'.$analyse->id) }}" onclick="return confirm(' Voulez-vous Vraiment supprimer cet enregistrement?')">Supprimer</a>@endcan
                          </td>
                      </tr>
                      @endforeach
                      </tbody>
                       <tfoot>
                        <tr>
                          <th>Code</th>
                           <th>libelle</th>
                           <th>Prix</th>
                           <th>Actions</th>
                        </tr>
                      </tfoot>
                    </table>
                  </div><!-- /.box-body -->
            </div><!-- /.box -->  
          </div>

         
  
  </div>

  

 

     
@stop