@extends("template")
  
  @section('content')

  @include('flash')

  <div class="row">
          <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Détail</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                {!!Form::open(['method'=>'PUT', 'url'=>route('analyse.update',$analyse)]) !!}

                  <div class="box-body">

                          <div class="form-group col-md-6 {!! $errors->has('libelle_analyse')?'has-error': '' !!}">
                         {!!Form::label('libelle_analyse','libelle')  !!}
                         {!! Form::text('libelle_analyse',$analyse->libelle_analyse, ['class'=>'form-control','id'=>'libelle_analyse','placeholder'=>'','readOnly'=>'readOnly']) !!}
                         {!! $errors->first('libelle_analyse','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>
                         
                         <div class="form-group col-md-6 {!! $errors->has('prix_analyse')?'has-error': '' !!}">
                            {!!Form::label('prix_analyse','prix')!!}
                            {!! Form::text('prix_analyse',$analyse->prix_analyse, ['class'=>'form-control','id'=>'prix_analyse','placeholder'=>'','readOnly'=>'readOnly']) !!}
                            {!! $errors->first('prix_analyse','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>


                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <a class="btn btn-primary" href="{{ route('analyse.create',$analyse) }}">Retour</a>
                  </div>

                  {!! Form::close() !!}
              </div><!-- /.box -->
                  
          </div>
  
  </div>

  

 

     
@stop