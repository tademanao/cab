@extends("template")
  
  @section('content')

  @include('flash')

  <div class="row">
          <div class="col-md-12">
             <br/>

             <h3 align="center">Dynamicaly Add/Remove dynamic fields in laravel 5.8 using Ajax Jquery</h3>

             <div class="table-responsive">

                <form method="post" id="dynamic_form">
                  {{ csrf_field() }}
                  
                  <span id="result"></span>

                   <table class="table table-bordered table-striped" id="user_table">

                      <thead>
                        <tr>
                           <th width="35%">First Name</th>
                           <th width="35%">Last Name</th>
                           <th width="30%">Action</th>
                        </tr>
                      </thead>

                      <tbody>
                       

                     
                      </tbody>

                       <tfoot>
                        <tr>
                          <td colspan="2" align="right">&nbsp;</td>
                          <td>
                            <input type="submit" class="btn btn-primary"  name="save" value="Save" />
                          </td>
                        </tr>
                      </tfoot>

                    </table>

                </form>
               
             </div>
                  
          </div>


         
         
  
  </div>

  
<script>
  
    $(function(){

        var count = 1;

        dynamic_field(count);

       function dynamic_field(number)
       {
          var html = '<tr>';

           html += '<td><input type="text" name= "first_name[]" class="form-control"></td>';
           html += '<td><input type="text" name= "last_name[]" class="form-control"></td>';

          if(number>1) 
          {
             html += '<td><button type="button name="remove" id="remove" class="btn btn-danger">Remove</button></td></tr>';
             $('tbody').append(html);
          }
          else
          {
             html += '<td><button type="button name="add" id="add" class="btn btn-success">Add</button></td></tr>';

             $('tbody').html(html);
          }

       }

       $('#add').click(function(){ 
          count++;
          dynamic_field(count);

       });

       $(document).on('click','#remove', function()
       {
         $(this).parent().parent().remove();
       });

        $('#dynamic_form').on('submit', function(event)
       {
            event.preventDefault();

            $.ajax({

                  url:'{{route("dynamic.store")}}',
                  method:'post',
                  data:$(this).serialize(),
                  dataType:'json',
                  beforeSend:function(){
                    $('#save').attr('disabled','disabled');
                  },
                  success:function(data)
                  {
                    if(data.error){

                       var error_html='';

                       for(var count=0; count<data.error.length; count++)
                       {
                        error_html += '<p>'+data.error[count]+'</p>'
                       }

                       $('#result').html('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+error_html+'</div>');
                    }else{
                      dynamic_field(1);
                      $('#result').html('<div class="alert alert-success ">'+data.success+'</div>');
                    }

                     $('#save').attr('disabled','false');
                  }

            })
       });

    });

</script>
 

     
@stop