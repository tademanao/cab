<div class="col-md-6">
          <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Dernière consultation</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table  class="table table-bordered table-striped">
                    <thead>
                      <tr>
                         <th>Code</th>
                         <th>Diagnostic</th>
                         <th>Date Consultation</th>
                         <th>Rendez-vous</th>
                      </tr>
                    </thead>
                    <tbody>
                     
                        <?php

                      foreach($consultations as $consultation) {

                        echo'<tr> 

                               <td>'.$consultation->id.'</td>
                               <td>'.$consultation->diagnostic.'</td>
                               <td>'.date('d-m-Y',strtotime($consultation->date_consultation)).'</td>
                               <td>'.date('d-m-Y',strtotime( $consultation->date_rdv)).'</td>
                          </tr>';

                        } ?>


                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box --> 

</div> 

    