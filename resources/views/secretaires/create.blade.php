@extends("template")
  
  @section('content')

  @include('flash')

  <div class="row">
          <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Créer</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                {!!Form::open(['method'=>'POST', 'url'=>route('secretaire.store')]) !!}

                  <div class="box-body">

                         <div class="form-group col-md-4 {!! $errors->has('lastname')?'has-error': '' !!}">
                         {!!Form::label('lastname','nom')  !!}
                         {!! Form::text('lastname',null, ['class'=>'form-control','id'=>'lastname','placeholder'=>'']) !!}
                         {!! $errors->first('lastname','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>
                         
                         <div class="form-group col-md-4 {!! $errors->has('firstname')?'has-error': '' !!}">
                            {!!Form::label('firstname','prénom')!!}
                            {!! Form::text('firstname',null, ['class'=>'form-control','id'=>'firstname','placeholder'=>'']) !!}
                            {!! $errors->first('firstname','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>

                           <div class="form-group col-md-4 {!! $errors->has('sexe')?'has-error': '' !!}">

                         {!!Form::label('sexe','sexe')!!}
                         {!! Form::select('sexe',['M'=>'masculin','F'=>'feminin'],null, ['class'=>'form-control','id'=>'sexe']) !!}

                         {!! $errors->first('sexe','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>

                         <div class="form-group col-md-4 {!! $errors->has('phone')?'has-error': '' !!}">
                            {!!Form::label('phone','téléphone')!!}
                            {!! Form::text('phone',null, ['class'=>'form-control','id'=>'phone','placeholder'=>'22-25-91-20 ou 90-32-58-62']) !!}
                            {!! $errors->first('phone','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>
                         <div class="form-group col-md-4 {!! $errors->has('email')?'has-error': '' !!}">
                            {!!Form::label('email','email')!!}
                            {!! Form::email('email',null, ['class'=>'form-control','id'=>'email','placeholder'=>' Exemple:tademanao92.ot@gmail.com']) !!}
                            {!! $errors->first('email','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>

                         <div class="form-group col-md-4 {!! $errors->has('username')?'has-error': '' !!}">
                            {!!Form::label('username','nom utilisateur')!!}
                            {!! Form::text('username',null, ['class'=>'form-control','id'=>'username','placeholder'=>'']) !!}
                            {!! $errors->first('username','<small class="help-block"><strong>:message</strong></small>') !!}
                          </div>

                          <div class="form-group col-md-4 {!! $errors->has('address')?'has-error': '' !!}">
                            {!!Form::label('address','adresse')!!}
                            {!! Form::text('address',null, ['class'=>'form-control','id'=>'address','placeholder'=>'', 'row'=>8]) !!}
                            {!! $errors->first('address','<small class="help-block"><strong>:message</strong></small>') !!}
                          </div>

                           <div class="form-group col-md-4 {!! $errors->has('sexe')?'has-error': '' !!}">
                           {!!Form::label('role','role')!!}
                           {!! Form::select('role',['Admin'=>'Admin','Médecin'=>'Médecin','Secrétaire'=>'Secrétaire','Infirmière'=>'Infirmière'],null, ['class'=>'form-control','id'=>'sexe']) !!}

                           {!! $errors->first('role','<small class="help-block"><strong>:message</strong></small>') !!}
                          </div>

                           <div class="form-group col-md-4 {!! $errors->has('password')?'has-error': '' !!}">
                            {!!Form::label('password','mot de passe')!!}
                            {!! Form::password('password', ['class'=>'form-control','id'=>'password','placeholder'=>'', 'row'=>8]) !!}
                            {!! $errors->first('password','<small class="help-block"><strong>:message</strong></small>') !!}
                          </div>

                          <div class="form-group col-md-4">
                            {!!Form::label('password-confirm','confirmation de mot de passe')!!}
                            {!! Form::password('password_confirmation', ['class'=>'form-control','id'=>'password-confirm', 'row'=>8]) !!}
                          </div>     

                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Envoyer</button>
                  </div>

                  {!! Form::close() !!}
              </div><!-- /.box -->
                  
          </div>

          <div class="col-md-12">
            <div class="box box-primary">
                  <div class="box-header">
                    <h3 class="box-title">Liste des secrétaires</h3>
                  </div><!-- /.box-header -->
                  <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                      <thead>
                        <tr>
                           <th>Code</th>
                           <th>Nom</th>
                           <th>Prénoms</th>
                           <th>Téléphone</th>
                           <th>Email</th>
                           <th>Nom Utilisateur</th>
                           <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                      
                    @foreach($secretaires as $secretaire) 

                      <tr> 

                           <td>{{ $secretaire->id }}</td>
                           <td>{{ $secretaire->user->lastname}}</td>
                           <td>{{ $secretaire->user->firstname}}</td>
                           <td>{{ $secretaire->user->phone}}</td>
                           <td>{{ $secretaire->user->email}}</td>
                           <td>{{ $secretaire->user->username}}</td>

                           <td>
                            <a class="btn btn-xs btn-primary" href="{{ route('secretaire.show',$secretaire) }}">Détail</a>
                            <a class="btn btn-xs btn-warning" href="{{ route('secretaire.edit',$secretaire) }}">Editer</a>
                            <a class="btn btn-xs btn-xs btn-danger" href="{{URL::to('/secretaire/destroy/'.$secretaire->id) }}" onclick="return confirm(' Voulez-vous Vraiment supprimer cet enregistrement?')">Supprimer</a>
                           </td>
                      </tr>
                      @endforeach
                      </tbody>
                       <tfoot>
                        <tr>
                           <th>Code</th>
                           <th>Nom</th>
                           <th>Prénoms</th>
                           <th>Téléphone</th>
                           <th>Email</th>
                           <th>Nom Utilisateur</th>
                           <th>Actions</th>
                        </tr>
                      </tfoot>
                    </table>
                  </div><!-- /.box-body -->
            </div><!-- /.box -->  
          </div>

         
  
  </div>

  

 

     
@stop