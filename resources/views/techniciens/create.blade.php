@extends("template")
  
  @section('content')

  @include('flash')

  <div class="row">
          <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Créer</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                {!!Form::open(['method'=>'POST', 'url'=>route('technicien.store')]) !!}

                  <div class="box-body">

                         <div class="form-group col-md-6 {!! $errors->has('lastname')?'has-error': '' !!}">
                         {!!Form::label('lastname','nom')  !!}
                         {!! Form::text('lastname',null, ['class'=>'form-control','id'=>'lastname','placeholder'=>'']) !!}
                         {!! $errors->first('lastname','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>
                         
                         <div class="form-group col-md-6 {!! $errors->has('firstname')?'has-error': '' !!}">
                            {!!Form::label('firstname','prénom')!!}
                            {!! Form::text('firstname',null, ['class'=>'form-control','id'=>'firstname','placeholder'=>'']) !!}
                            {!! $errors->first('firstname','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>

                           <div class="form-group col-md-6 {!! $errors->has('sexe')?'has-error': '' !!}">

                         {!!Form::label('sexe','sexe')!!}
                         {!! Form::select('sexe',['M'=>'masculin','F'=>'feminin'],null, ['class'=>'form-control','id'=>'sexe']) !!}

                         {!! $errors->first('sexe','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>

                         <div class="form-group col-md-6 {!! $errors->has('phone')?'has-error': '' !!}">
                            {!!Form::label('phone','téléphone')!!}
                            {!! Form::text('phone',null, ['class'=>'form-control','id'=>'phone','placeholder'=>'']) !!}
                            {!! $errors->first('phone','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>
                         <div class="form-group col-md-6 {!! $errors->has('email')?'has-error': '' !!}">
                            {!!Form::label('email','email')!!}
                            {!! Form::email('email',null, ['class'=>'form-control','id'=>'email','placeholder'=>' Exemple:tademanao92.ot@gmail.com']) !!}
                            {!! $errors->first('email','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>

                         <div class="form-group col-md-6 {!! $errors->has('username')?'has-error': '' !!}">
                            {!!Form::label('username','nom utilisateur')!!}
                            {!! Form::text('username',null, ['class'=>'form-control','id'=>'username','placeholder'=>'']) !!}
                            {!! $errors->first('username','<small class="help-block"><strong>:message</strong></small>') !!}
                          </div>

                          <div class="form-group col-md-6 {!! $errors->has('address')?'has-error': '' !!}">
                            {!!Form::label('address','adresse')!!}
                            {!! Form::textarea('address',null, ['class'=>'form-control','id'=>'address','placeholder'=>'', 'row'=>8]) !!}
                            {!! $errors->first('address','<small class="help-block"><strong>:message</strong></small>') !!}
                          </div>

                           <div class="form-group col-md-6 {!! $errors->has('role_id')?'has-error': '' !!}">
                             {!!Form::label('roles','roles')!!}
                             {!! Form::select('role_id',$roles,null, ['class'=>'form-control','id'=>'role_id']) !!}
                             {!! $errors->first('role_id','<small class="help-block"><strong>:message</strong></small>') !!}
                           </div>

                           <div class="form-group col-md-6 {!! $errors->has('password')?'has-error': '' !!}">
                            {!!Form::label('password','mot de passe')!!}
                            {!! Form::password('password', ['class'=>'form-control','id'=>'password','placeholder'=>'', 'row'=>8]) !!}
                            {!! $errors->first('password','<small class="help-block"><strong>:message</strong></small>') !!}
                          </div>

                          <div class="form-group col-md-6">
                            {!!Form::label('password-confirm','confirmation de mot de passe')!!}
                            {!! Form::password('password_confirmation', ['class'=>'form-control','id'=>'password-confirm', 'row'=>8]) !!}
                          </div>     

                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Envoyer</button>
                  </div>

                  {!! Form::close() !!}
              </div><!-- /.box -->
                  
          </div>

          <div class="col-md-12">
            <div class="box box-primary">
                  <div class="box-header">
                    <h3 class="box-title">Liste des plans</h3>
                  </div><!-- /.box-header -->
                  <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                      <thead>
                        <tr>
                           <th>Code</th>
                           <th>Nom</th>
                           <th>Prenoms</th>
                           <th>Telephone</th>
                           <th>Email</th>
                           <th>Nom Utilisateur</th>
                           <th>modifier</th>
                           <th>supprimer</th>
                        </tr>
                      </thead>
                      <tbody>
                       
                      
                    @foreach($techniciens as $technicien) 

                      <tr> 

                           <td>{{ $technicien->id }}</td>
                           <td>{{ $technicien->user->lastname}}</td>
                           <td>{{ $technicien->user->firstname}}</td>
                           <td>{{ $technicien->user->phone}}</td>
                           <td>{{ $technicien->user->email}}</td>
                           <td>{{ $technicien->user->username}}</td>

                           <td><a class="btn btn-primary" href="{{ route('technicien.edit',$technicien) }}">Editer</a></td>
                           <td>
                            {{Form::open(['method'=>'delete', 'url'=>route('technicien.destroy',$technicien)]) }}
    
                            <button class="btn btn-danger" onclick="return confirm(' Voulez-vous Vraiment supprimer cet enregistrement?')" >Supprimer</button>
                              {!! Form::close() !!}
                           </td>
                      </tr>
                      @endforeach
                      </tbody>
                       <tfoot>
                        <tr>
                          <th>Code</th>
                           <th>Nom</th>
                           <th>Prenoms</th>
                           <th>Telephone</th>
                           <th>Email</th>
                           <th>Nom Utilisateur</th>
                           <th>modifier</th>
                           <th>supprimer</th>
                        </tr>
                      </tfoot>
                    </table>
                  </div><!-- /.box-body -->
            </div><!-- /.box -->  
          </div>

         
  
  </div>

  

 

     
@stop