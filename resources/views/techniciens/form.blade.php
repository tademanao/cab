
<?php

if(!isset($medecin->id)){
	$options = ['method'=>'post','url'=>action('MedecinController@store')];
	
} else{

	$options = ['method'=>'put','url'=>action('MedecinController@update',$medecin)];
}

?>

@include('flash')

<div class="row">
          <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Créer</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                {!!Form::open($options)!!}

                  <div class="box-body">
                  			
                         <div class="form-group col-md-6 {!! $errors->has('lastname')?'has-error': '' !!}">
                         {!!Form::label('lastname','lastname')  !!}
                         {!! Form::text('lastname', $medecin->user->lastname, ['class'=>'form-control','id'=>'lastname','placeholder'=>'lastname']) !!}
                         {!! $errors->first('lastname','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>
                         
                         <div class="form-group col-md-6 {!! $errors->has('firstname')?'has-error': '' !!}">
                            {!!Form::label('firstname','firstname')!!}
                            {!! Form::text('firstname',$medecin->user->firstname, ['class'=>'form-control','id'=>'firstname','placeholder'=>'firstnames']) !!}
                            {!! $errors->first('firstname','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>

                           <div class="form-group col-md-6 {!! $errors->has('sexe')?'has-error': '' !!}">

                         {!!Form::label('sexe','Sexe')!!}
                         {!! Form::select('sexe',['M'=>'masculin','F'=>'feminin'],$medecin->user->sexe, ['class'=>'form-control','id'=>'sexe']) !!}

                         {!! $errors->first('sexe','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>

                         <div class="form-group col-md-6 {!! $errors->has('phone')?'has-error': '' !!}">
                            {!!Form::label('phone','Téléphone')!!}
                            {!! Form::text('phone',$medecin->user->phone, ['class'=>'form-control','id'=>'phone','placeholder'=>'Téléphone']) !!}
                            {!! $errors->first('phone','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>
                         <div class="form-group col-md-6 {!! $errors->has('email')?'has-error': '' !!}">
                            {!!Form::label('email','Email')!!}
                            {!! Form::email('email',$medecin->user->email, ['class'=>'form-control','id'=>'email','placeholder'=>'Email']) !!}
                            {!! $errors->first('email','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>

                         <div class="form-group col-md-6 {!! $errors->has('name')?'has-error': '' !!}">
                            {!!Form::label('name','username')!!}
                            {!! Form::text('name',$medecin->user->name, ['class'=>'form-control','id'=>'name','placeholder'=>'name']) !!}
                            {!! $errors->first('name','<small class="help-block"><strong>:message</strong></small>') !!}
                          </div>

                          <div class="form-group col-md-6 {!! $errors->has('address')?'has-error': '' !!}">
                            {!!Form::label('address','address')!!}
                            {!! Form::textarea('address',$medecin->user->address, ['class'=>'form-control','id'=>'address','placeholder'=>'address', 'row'=>8]) !!}
                            {!! $errors->first('address','<small class="help-block"><strong>:message</strong></small>') !!}
                          </div>

                          <div class="col-md-6">
                              <label for="password">{{ __('Password') }}</label>
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? 'has-error' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                          </div>
                        
                            <div class="col-md-6">
                              <label for="password-confirm">{{ __('Confirm Password') }}</label>
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                       

                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Envoyer</button>
                  </div>

                  {!! Form::close() !!}
              </div><!-- /.box -->
                  
          </div>
  
  </div>
