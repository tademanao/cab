@extends("template")
  
  @section('content')
  
  @include('flash')

  <div class="row">
          <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Créer</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                {!!Form::open(['method'=>'put', 'url'=>route('technicien.update',$technicien)]) !!}

                  <div class="box-body">
                        
                         <div class="form-group col-md-6 {!! $errors->has('lastname')?'has-error': '' !!}">
                         {!!Form::label('lastname','nom')  !!}
                         {!! Form::text('lastname', $technicien->user->lastname, ['class'=>'form-control','id'=>'lastname','placeholder'=>'lastname']) !!}
                         {!! $errors->first('lastname','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>
                         
                         <div class="form-group col-md-6 {!! $errors->has('firstname')?'has-error': '' !!}">
                            {!!Form::label('firstname','prénom')!!}
                            {!! Form::text('firstname',$technicien->user->firstname, ['class'=>'form-control','id'=>'firstname','placeholder'=>'firstnames']) !!}
                            {!! $errors->first('firstname','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>

                           <div class="form-group col-md-6 {!! $errors->has('sexe')?'has-error': '' !!}">

                         {!!Form::label('sexe','sexe')!!}
                         {!! Form::select('sexe',['M'=>'masculin','F'=>'feminin'],$technicien->user->sexe, ['class'=>'form-control','id'=>'sexe']) !!}

                         {!! $errors->first('sexe','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>

                         <div class="form-group col-md-6 {!! $errors->has('phone')?'has-error': '' !!}">
                            {!!Form::label('phone','téléphone')!!}
                            {!! Form::text('phone',$technicien->user->phone, ['class'=>'form-control','id'=>'phone','placeholder'=>'Téléphone']) !!}
                            {!! $errors->first('phone','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>
                         <div class="form-group col-md-6 {!! $errors->has('email')?'has-error': '' !!}">
                            {!!Form::label('email','email')!!}
                            {!! Form::email('email',$technicien->user->email, ['class'=>'form-control','id'=>'email','placeholder'=>'Email']) !!}
                            {!! $errors->first('email','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>

                         <div class="form-group col-md-6 {!! $errors->has('username')?'has-error': '' !!}">
                            {!!Form::label('username','nom utilisateur')!!}
                            {!! Form::text('username',$technicien->user->username, ['class'=>'form-control','id'=>'username','placeholder'=>'username']) !!}
                            {!! $errors->first('username','<small class="help-block"><strong>:message</strong></small>') !!}
                          </div>

                          <div class="form-group col-md-6 {!! $errors->has('address')?'has-error': '' !!}">
                            {!!Form::label('address','adresse')!!}
                            {!! Form::textarea('address',$technicien->user->address, ['class'=>'form-control','id'=>'address','placeholder'=>'address', 'row'=>8]) !!}
                            {!! $errors->first('address','<small class="help-block"><strong>:message</strong></small>') !!}
                          </div>
                          <div class="form-group col-md-6 {!! $errors->has('role_id')?'has-error': '' !!}">
                             {!!Form::label('role','role')!!}
                             {!! Form::select('role_id',$roles,$technicien->user->role_id, ['class'=>'form-control','id'=>'role_id']) !!}
                             {!! $errors->first('role_id','<small class="help-block"><strong>:message</strong></small>') !!}
                          </div>

                          <div class="form-group col-md-6 {!! $errors->has('password')?'has-error': '' !!}">
                            {!!Form::label('password','mot de passe')!!}
                            {!! Form::password('password', ['class'=>'form-control','id'=>'password','placeholder'=>'', 'row'=>8]) !!}
                            {!! $errors->first('password','<small class="help-block"><strong>:message</strong></small>') !!}
                          </div>

                          <div class="form-group col-md-6">
                            {!!Form::label('password-confirm','confirmation de mot de passe')!!}
                            {!! Form::password('password_confirmation', ['class'=>'form-control','id'=>'password-confirm', 'row'=>8]) !!}
                          </div>

                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Envoyer</button>
                  </div>

                  {!! Form::close() !!}
              </div><!-- /.box -->
                  
          </div>
  
  </div>

@stop

