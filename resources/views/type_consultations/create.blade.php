@extends("template")
  
  @section('content')

  @include('flash')

  <div class="row">
          <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Créer</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                {!!Form::open(['method'=>'POST', 'url'=>route('typeConsultation.store')]) !!}

                  <div class="box-body">

                         <div class="form-group col-md-6 {!! $errors->has('libelle')?'has-error': '' !!}">
                         {!!Form::label('libelle','libelle')  !!}
                         {!! Form::text('libelle',null, ['class'=>'form-control','id'=>'libelle','placeholder'=>'']) !!}
                         {!! $errors->first('libelle','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>
                         
                         <div class="form-group col-md-6 {!! $errors->has('prix_consultation')?'has-error': '' !!}">
                            {!!Form::label('prix_consultation','prix consultation')!!}
                            {!! Form::text('prix_consultation',null, ['class'=>'form-control','id'=>'prix_consultation','placeholder'=>'']) !!}
                            {!! $errors->first('prix_consultation','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>

                          <div class="form-group col-md-6 {!! $errors->has('description')?'has-error': '' !!}">
                         {!!Form::label('description','description')  !!}
                         {!! Form::textarea('description',null, ['class'=>'form-control','id'=>'description','placeholder'=>'','rows'=>'4']) !!}
                         {!! $errors->first('description','<small class="help-block"><strong>:message</strong></small>') !!}
                          
                         </div>

                       

                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Envoyer</button>
                  </div>

                  {!! Form::close() !!}
              </div><!-- /.box -->
                  
          </div>

          <div class="col-md-12">
            <div class="box box-primary">
                  <div class="box-header">
                    <h3 class="box-title">Liste des types de consultation</h3>
                  </div><!-- /.box-header -->
                  <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                      <thead>
                        <tr>
                           <th>Code</th>
                           <th>Libelle</th>
                           <th>Description</th>
                           <th>Prix consultation</th>
                           <th>Date de création</th>
                           <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                       
                      
                    @foreach($typeConsultations as $typeConsultation) 

                      <tr> 
                           <td>{{ $typeConsultation->id }}</td>
                           <td>{{ $typeConsultation->libelle }}</td>
                           <td>{{ $typeConsultation->description }}</td>
                           <td>{{ $typeConsultation->prix_consultation }}</td>
                           <td>{{ date('d-m-Y',strtotime($typeConsultation->date)) }}</td>

                          <td><a class="btn btn-xs btn-primary" href="{{ route('typeConsultation.show',$typeConsultation) }}">Détail</a> 
                            <a class="btn btn-xs btn-warning" href="{{ route('typeConsultation.edit',$typeConsultation) }}">Editer</a>
                            @can('isAdmin')
                            <a class="btn btn-xs btn-danger" href="{{URL::to('/typeConsultation/destroy/'.$typeConsultation->id) }}" onclick="return confirm(' Voulez-vous Vraiment supprimer cet enregistrement?')">Supprimer</a>@endcan
                          </td>
                      </tr>
                      @endforeach
                      </tbody>
                       <tfoot>
                        <tr>
                          <th>Code</th>
                           <th>Libelle</th>
                           <th>Description</th>
                           <th>Prix consultation</th>
                           <th>Date de création</th>
                           <th>Actions</th>
                        </tr>
                      </tfoot>
                    </table>
                  </div><!-- /.box-body -->
            </div><!-- /.box -->  
          </div>

         
  
  </div>

  

 

     
@stop