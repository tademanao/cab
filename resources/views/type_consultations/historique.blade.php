@extends("template")
  
  @section('content')
  @include('flash')
  
   <div class="col-md-12">
          <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">historique</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                         <th>Code</th>
                         <th>libelle</th>
                         <th>prix consultation</th>
                         <th>statut</th>
                         <th>date</th>
                      </tr>
                      </tr>
                    </thead>
                    <tbody>
                     
                   @foreach($prixHistoriques as $prixHistorique) 

                    <tr> 

                         <td>{{ $prixHistorique->id }}</td>
                         <td>{{ $prixHistorique->libelle}}</td>
                         <td>{{ $prixHistorique->prix_consultation}}</td>
                         <td>{{ $prixHistorique->status}}</td>
                         <td>{{ date('d-m-Y',strtotime($prixHistorique->date)) }}</td>
                    </tr>
                    @endforeach
                    </tbody>
                     <tfoot>
                      <tr>
                         <th>Code</th>
                         <th>libelle</th>
                         <th>prix consultation</th>
                         <th>statut</th>
                         <th>date</th>
                      </tr>
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->  
     </div>
</div>       
     
@stop