@extends("template")
  
  @section('content')

  @include('flash')

  <div class="row">
          <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Détail</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                {!!Form::open(['method'=>'PUT', 'url'=>route('typeConsultation.update',$typeConsultation)]) !!}

                  <div class="box-body">

                         <div class="form-group col-md-6 {!! $errors->has('libelle')?'has-error': '' !!}">
                         {!!Form::label('libelle','libelle')  !!}
                         {!! Form::text('libelle',$typeConsultation->libelle, ['class'=>'form-control','id'=>'libelle','placeholder'=>'','readOnly'=>'readOnly']) !!}
                         {!! $errors->first('libelle','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>
                         
                         <div class="form-group col-md-6 {!! $errors->has('prix_consultation')?'has-error': '' !!}">
                            {!!Form::label('prix consultation','prix consultation')!!}
                            {!! Form::text('prix consultation',$typeConsultation->prix_consultation, ['class'=>'form-control','id'=>'prix_consultation','placeholder'=>'','readOnly'=>'readOnly']) !!}
                            {!! $errors->first('prix_consultation','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>

                           <div class="form-group col-md-6 {!! $errors->has('description')?'has-error': '' !!}">
                         {!!Form::label('description','description')  !!}
                         {!! Form::textarea('description',$typeConsultation->description, ['class'=>'form-control','id'=>'description','placeholder'=>'','rows'=>'4','readOnly'=>'readOnly']) !!}
                          {!! $errors->first('description','<small class="help-block"><strong>:message</strong></small>') !!}
                         </div>


                  </div><!-- /.box-body -->

                  <div class="box-footer">
                   <a class="btn btn-primary" href="{{ route('typeConsultation.create',$typeConsultation) }}">Retour</a>
                  </div>

                  {!! Form::close() !!}
              </div><!-- /.box -->
                  
          </div>
  
  </div>

  

 

     
@stop