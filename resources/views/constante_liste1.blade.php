@extends("template")
  
  @section('content')

  @include('flash')

<div class="row">
        
          <div class="col-md-12">
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Liste des constantes</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Code</th>
                        <th>Temperature</th>
                        <th>Poids</th>
                        <th>Taille</th>
                        <th>Poul</th>
                        <th>Tension bras gauche</th>
                        <th>Tension bras droit</th>
                      </tr>
                    </thead>
                    <tbody>

                      @foreach($constantes as $constante) 

                    <tr> 

                         <td>{{ $constante->id }}</td>
                         <td>{{ $constante->temperature }}</td>
                         <td>{{ $constante->poids }}</td>
                         <td>{{ $constante->taille }}</td>
                         <td>{{ $constante->poul }}</td>
                         <td>{{ $constante->tension_bras_gauche }}</td>
                         <td>{{ $constante->tension_bras_droit }}</td>
                         
                    </tr>
                    @endforeach
      
                    </tbody>
                     <tfoot>
                      <tr>
                       <th>Code</th>
                        <th>Temperature</th>
                        <th>Poids</th>
                        <th>Taille</th>
                        <th>Poul</th>
                        <th>Tension bras gauche</th>
                        <th>Tension bras droit</th>
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box --> 
          </div>         
</div> 

     
@stop