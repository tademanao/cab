@extends("template")
  
  @section('content')

  @include('flash')

<div class="row">
          <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Ordonnance</h3>
                </div><!-- /.box-header -->
                <!-- form start -->

                 <div class="row" id="medoc_temp" style="display: none;">

                       <div class="form-group col-md-6" style="display: none;">
                        {!!Form::label('consultation','consultation')  !!}
                        {!!Form::hidden('id',$consultation->id,['class'=>'form-control','rows'=>'8'])  !!}
                       </div>  

                        <div class="form-group col-md-6">

                           {!!Form::label('medicament_id', 'medicaments')!!}
                           {!! Form::select('medicament_id',$medicaments,null, ['class'=>'form-control']) !!}
                           
                        </div>

                        <div class="form-group col-md-6 {!! $errors->has('posologie')?'has-error': '' !!}">
                            {!!Form::label('posologie','posologie')!!}
                            {!! Form::text('posologie',null, ['class'=>'form-control','id'=>'posologie','placeholder'=>'']) !!}
                            {!! $errors->first('posologie','<small class="help-block"><strong>:message</strong></small>') !!}
                        </div>
                  </div>   
               {!!Form::open(['method'=>'pot','url'=>('/consultation/ordonnance')]) !!}
                  <div class="box-body">
                    
                   <div class="form-group col-md-6" style="display: none;">
                    {!!Form::label('consultation','consultation')  !!}
                    {!!Form::hidden('id',$consultation->id,['class'=>'form-control','rows'=>'8'])  !!}
                   </div>  

                   <div class="row" id="container">
                        <div class="form-group col-md-6">

                           {!!Form::label('medicament_id', 'medicaments')!!}
                           {!! Form::select('medicament_id',$medicaments,null, ['class'=>'selectpicker form-control', 'data-live-search'=>'true', 'data-live-search-style'=>'startsWith' ]) !!}

                        <!--     <select name="medicament_id" id="medicament_id" class="selectpicker form-control" data-live-search="true" data-live-search-style="startsWith" required>
                                    @foreach ($medocs as $medoc)
                                        <option value="{{ $medoc->id}}">{{ $medoc->libelle_medicament}}</option>
                                    @endforeach
                                    
                            </select> -->
                           
                        </div>

                         <div class="form-group col-md-6">

                      <!--      {!!Form::label('medicament_id', 'medicaments')!!}
                           {!! Form::select('medicament_id',$medicaments,null, ['class'=>'form-control select-picker data-live-search = true']) !!}
 -->
                            <select data-live-search="true" data-live-search-style="startsWith" class="selectpicker form-control"  required>
                                   <option >4444</option>
                                   <option>Fedex</option>
                                   <option>Elite</option>
                                   <option>oui</option>
                                   <option>Interp</option>
                                   <option>Test</option>
                                    
                            </select>
                           
                        </div>

                        <div class="form-group col-md-6 {!! $errors->has('posologie')?'has-error': '' !!}">
                            {!!Form::label('posologie','posologie')!!}
                            {!! Form::text('posologie',null, ['class'=>'form-control','id'=>'posologie','placeholder'=>'']) !!}
                            {!! $errors->first('posologie','<small class="help-block"><strong>:message</strong></small>') !!}
                        </div>
                   </div>

                  <!--  <div class="form-group col-md-6 {!! $errors->has('date_ordonnance')?'has-error': '' !!}">
                            {!!Form::label('date_ordonnance','date')!!}
                            {!! Form::input('date','date_ordonnance', date('Y-m-d'),['class'=>'form-control','id'=>'date_ordonnance','placeholder'=>'', 'row'=>8]) !!}
                            {!! $errors->first('date_ordonnance','<small class="help-block"><strong>:message</strong></small>') !!}
                   </div>  --> 

                 
                  <div class="row col-12 text-right" style="margin-right: 15px;">
                    <button type="button" id="add" class="btn btn-success" style="border-radius: 50%;">
                      <i class="fa fa-plus"></i> 
                    </button>
                    
                  </div>
                    
                 </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Envoyer</button> &nbsp;<a class="btn btn-primary" href="{{ route('consultation.create',$consultation)}}">Retour <i class="fa fa-index"></i></a>
                  </div>

                  {!! Form::close() !!}
               </div><!-- /.box -->
                         
          </div>

          <div class="col-md-12">
            <div class="box box-primary">
                  <div class="box-header">
                    <h3 class="box-title"></h3>
                  </div><!-- /.box-header -->
                  <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                      <thead>
                        <tr>
                           <th>code</th>
                           <th>medicament</th>
                           <th>posologie</th>
                           <th>modifier</th>
                           <th>supprimer</th>
                        </tr>
                      </thead>
                      <tbody>
                       
                      
                      @foreach($ordonnances as $ordonnance) 

                      <tr> 
                            <td>{{ $ordonnance->id }}</td>
                            <td>{{ $ordonnance->libelle_medicament }}</td>
                            <td>{{ $ordonnance->posologie}}</td>
                            <td><a class="btn btn-primary" href="{{ route('ordonnance_edit',$ordonnance->id)}}">Editer</a></td>
                             <td>
                              {{Form::open(['method'=>'delete', 'url'=>route('ordonnance_destroy',$ordonnance->id)]) }}
      
                              <button class="btn btn-danger" onclick="return confirm(' Voulez-vous Vraiment supprimer cet enregistrement?')" >Supprimer</button>
                                {!! Form::close() !!}
                         </td> 
            
                      </tr>

                      @endforeach
                      </tbody>
                       <tfoot>
                        <tr>
                           <th>code</th>
                           <th>medicament</th>
                           <th>posologie</th>
                           <th>modifier</th>
                           <th>supprimer</th>
                        </tr>
                      </tfoot>
                    </table>
                  </div><!-- /.box-body -->
            </div><!-- /.box -->  
          </div>


           
</div>

 

@stop