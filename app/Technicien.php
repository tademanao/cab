<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Technicien extends User
{
     public function user()
	{
		return $this->morphOne(User::class,'userable');
	}
}
