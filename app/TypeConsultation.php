<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeConsultation extends Model
{
    protected $fillable = ['libelle','description','prix_consultation','date'];

     public function Consultations()
    {
    	return $this->HasMany(Consultation::class);
    }
}
