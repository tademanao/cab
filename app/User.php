<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

     protected $fillable = [
        'username', 'email', 'firstname', 'lastname', 'status', 'sexe', 'phone', 'address', 'password','role','userable_id','userable_type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    //  public function role()
    // {
    //     return $this->BelongsTo(Role::class);
    // }

     public function userable()
    {
        return $this->morphTo();
    }

    public function Userpermissions($perm)
    {
          $role = $this->role_id;

        $rolePermissions = Permission::join("permission_role","permission_role.permission_id","=","permissions.id")

            ->where("permission_role.role_id",$role)

            ->get()->toArray();

            foreach($rolePermissions as $nb => $permission){

                foreach ($permission as $c => $value){

                    if($c=== 'name'){

                       $aff[]= $value;
                    }  
                }
            }

        foreach ($aff as $value) {

            if($perm===$value){

                return true;
            }
            
        }
    }
}
