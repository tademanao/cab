<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produit extends Model
{
     protected $fillable = ['libelle_produit','prix_produit'];


}
