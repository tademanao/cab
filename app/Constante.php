<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Constante extends Model
{
    protected $fillable = ['temperature','poids','taille','tension_bras_gauche','tension_bras_droit','poul','date_constante','patient_id'];

     public function patient()
    {
    	return $this->BelongsTo(Patient::class);
    }
}
