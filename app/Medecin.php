<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Medecin extends User
{
	public function user()
	{
		return $this->morphOne(User::class,'userable');
	}
        
	 public function consultations()
    {
    	return $this->hasMany(Consultation::class);
    }
    
     public function patients()
    {
        return $this->belongsToMany(Patient::class)->withPivot('date','heure','objet')->withTimestamps();

    }
}
