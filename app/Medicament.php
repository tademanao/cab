<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Medicament extends Model
{
    protected $fillable=['libelle_medicament'];


     public function ordonnances()
    {
        return $this->belongsToMany(Ordonnance::class)->withPivot('posologie','date_ordonnance')->withTimestamps();
    }

    // public function consultations()
    // {
    //     return $this->belongsToMany('App\Consultation');
    // }  
}
