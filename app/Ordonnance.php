<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ordonnance extends Model
{
	 protected $fillable = ['consultation_id'];

     public function consultation()
    {
    	return $this->BelongsTo(Consultation::class);
    }

     public function medicaments()
    {
        return $this->belongsToMany(Medicament::class)->withPivot('posologie','date_ordonnance')->withTimestamps();
    }

}
