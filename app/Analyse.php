<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Analyse extends Model
{
     protected $fillable=['libelle_analyse','prix_analyse'];

       public function consultations()
    {
        return $this->belongsToMany(Consultation::class)->withPivot('date_analyse','quantite_analyse','resultat','montant')->withTimestamps();

    }
}
