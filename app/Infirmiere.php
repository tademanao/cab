<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Infirmiere extends User 
{
    
    public function user()
	{
		return $this->morphOne(User::class,'userable');
	}

}
