<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [

            'lastname'=>'required|regex:/^[(a-zA-Z\-\)]+$/u',
            'firstname'=>'required|regex:/^[(a-zA-Z\-\s\.)(éèà)]+$/u',
            'address'=>'required|string|max:255',
            'phone'=>'required|regex:/^\d{2}((\-)\d{2}){3}$/',
            'username' =>'required|string|max:255',
            'email' => 'required|email|max:255'
        ];
    }

      /**
         * {@inheritdoc}
         */
    protected function formatErrors(Validator $validator)
    {
        return $validator->errors()->all();
    }

    public function messages()
    {
        return [

            'lastname.required' => 'Le champ nom est obligatoire',
            'firstname.required' => 'Le champ prénom est obligatoire',
           
        ];
    }
}
