<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {   
        
        return [

            'lastname'=>'required|regex:/^[(a-zA-Z\-\)]+$/u',
            'firstname'=>'required|regex:/^[(a-zA-Z\-\s\.)(éèà)]+$/u',
            'address'=>'required|string|max:255',
            'phone'=>'required|regex:/^\d{2}((\-)\d{2}){3}$/|unique:users,phone',
            'username' =>'required|string|max:255|unique:users,username',
            'password' => 'required|string|min:6|confirmed',
            'email' => 'required|email|max:255|unique:users,email'


        ];
    }

      /**
         * {@inheritdoc}
         */
    protected function formatErrors(Validator $validator)
    {
        return $validator->errors()->all();
    }

    public function messages()
    {
        return [

          'lastname.required' => 'Le champ nom est obligatoire',
          'firstname.required' => 'Le champ prénom est obligatoire',
           
        ];
    }
}
