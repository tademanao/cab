<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;

class ProduitRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = ($this->produit)? $this->produit : '';

        return [
            'libelle_produit' =>'required|string|max:255|unique:produits,libelle_produit,'.$id,
            'prix_produit' =>['required','regex:/^\d+(\.\d+)?$/'],

        ];
    }

      /**
         * {@inheritdoc}
         */
    protected function formatErrors(Validator $validator)
    {
        return $validator->errors()->all();
    }

    public function messages()
    {
        return [
            
        ];
    }
}
