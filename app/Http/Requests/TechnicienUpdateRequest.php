<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;

class TechnicienUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->technicien;

        return [

            'firstname'=>'required|string|max:255',
            'lastname'=>'required|string|max:255',
            'phone'=>'required|string|max:255',
            'address'=>'required|string|max:255',
            'username' =>'required|string|max:255|unique:users,userable_id,'.$id,
            'email' => 'required|string|email|max:255|unique:users,userable_id,'.$id,

        ];
    }

      /**
         * {@inheritdoc}
         */
    protected function formatErrors(Validator $validator)
    {
        return $validator->errors()->all();
    }

    public function messages()
    {
        return [

            'lastname.required' => 'Le champ nom est obligatoire',
            'firstname.required' => 'Le champ prénom est obligatoire',
           
        ];
    }
}
