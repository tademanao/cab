<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;

class PatientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = ($this->patient)? $this->patient : '';

        return [

            'nom'=>'required|regex:/^[(a-zA-Z\-\)]+$/u',
            'prenom'=>'required|regex:/^[(a-zA-Z\-\s\.)(éèà)]+$/u',
            'adresse'=>'required|string|max:255',
            'phone'=>'required|regex:/^\d{2}((\-)\d{2}){3}$/|unique:patients,phone,'.$id,
            'profession' =>'required|regex:/^[(a-zA-Z\-\s\.)(éèà)]+$/u',
            'email' => 'required|email|max:255|unique:patients,email,'.$id,
        ];
    }

      /**
         * {@inheritdoc}
         */
    protected function formatErrors(Validator $validator)
    {
        return $validator->errors()->all();
    }

    public function messages()
    {
        return [
            
        ];
    }
}
