<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use \Auth;
use App\User;

class UserProfilRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname'=>'required|string|max:255',
            'lastname'=>'required|string|max:255',
            'phone'=>'required|string|max:255',
            'address'=>'required|string|max:255',
            'username' =>'required|string|max:255',
            'email' => 'required|string|email|max:255'
        ];
    }
}
