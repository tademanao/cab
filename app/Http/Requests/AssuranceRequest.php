<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;

class AssuranceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
       

        $id = ($this->assurance)? $this->assurance : '';

        return [
            'libelle' =>'required|string|max:255|unique:assurances,libelle,'.$id,
            'part_assure' =>['required','regex:/^0(\.\d{1,4})?$/'],
            'part_assureur' => ['required','regex:/^0(\.\d{1,4})?$/']

        ];
    }

      /**
         * {@inheritdoc}
         */
    protected function formatErrors(Validator $validator)
    {
        return $validator->errors()->all();
    }

    public function messages()
    {
        return [
            'part_assure.required' => 'Le champ part assuré est obligatoire',
        ];
    }
}
