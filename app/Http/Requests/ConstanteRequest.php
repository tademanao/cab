<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;

class ConstanteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'temperature'=>'required|regex:/^\d{2}(\.\d+)?$/',
            'poids'=>'required|regex:/^\d{2,3}(\.\d+)?$/',
            'taille'=>'required|regex:/^\d{1}(\.\d+)?$/',
            'tension_bras_gauche'=>'required|regex:/^\d{2,3}\/\d{2,3}$/',
            'tension_bras_droit'=>'required|regex:/^\d{2,3}\/\d{2,3}$/',
            'poul'=>'required|regex:/^\d{2}(\.\d+)?$/'
        ];
    }

      /**
         * {@inheritdoc}
         */
    protected function formatErrors(Validator $validator)
    {
        return $validator->errors()->all();
    }

    public function messages()
    {
        return [
            
        ];
    }
}
