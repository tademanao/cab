<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AnalyseRequest;
use App\Analyse;
use NumberToWords\NumberToWords;
// use App\Mode
use DB;
use Auth;

class AnalyseController extends Controller
{
      public function __construct()
    {
        $this->middleware('auth');

        // $this->middleware('permission:analyse-list|analyse-create|analyse-edit|analyse-delete', ['only' => ['index','store']]);
        // $this->middleware('permission:analyse-create', ['only' => ['create','store']]);

        // $this->middleware('permission:analyse-edit', ['only' => ['edit','update']]);
 
        // $this->middleware('permission:analyse-delete', ['only' => ['destroy']]);

    }

    public function index()
    {
        //  $analyses = analyse::get();
        // return view('analyses/index',compact('analyses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
         $analyses = Analyse::latest()->get();

        return view('analyses/create', compact('analyses'));

        
    // $numberToWords = new NumberToWords();

    // $numberTransformer = $numberToWords->getNumberTransformer('fr');
    
    //     $num = 5120.5
    //     $numero = $numberTransformer->toWords(5120.5); 

    //      dd($numero);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AnalyseRequest $request)
    {
        $analyse = Analyse::create($request->all());

         return redirect(route('analyse.create', $analyse))->withInfo('Enregistrement réussi');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $analyse= Analyse::findOrFail($id);

        return view('analyses/show',compact('analyse'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $analyse= Analyse::findOrFail($id);

        return view('analyses/edit',compact('analyse'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AnalyseRequest $request, $id)
    {
        $analyse = Analyse::findOrfail($id);

        $analyse->update($request->all());

        return redirect(route('analyse.create', $id))->withSuccess('modification réussi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $analyse = Analyse::findOrfail($id);

         $analyses = DB::table('analyse_consultation')->where('analyse_id',$id)->get();

         $nb = count($analyses);

         if($nb>0)
         {
             return redirect(route('analyse.create', $analyse))->withDanger(' Vous ne pouvez pas supprimez cet enregistrement car il est utilisé au niveau d\'une consultation');

         }

        $analyse->delete();

        return redirect(route('analyse.create', $id))->withDanger('Suppression réussi');
    }
}
