<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\SoinRepository;
use App\Http\Requests\SoinRequest;
use App\Soin;
// use App\Models\Specialite;
use DB;
use Auth;

class SoinController extends Controller
{
	 protected $soinRepository;
    
  public function __construct()
    {
        $this->middleware('auth');

        // $this->middleware('permission:soin-list|soin-create|soin-edit|soin-delete', ['only' => ['index','store']]);
        // $this->middleware('permission:soin-create', ['only' => ['create','store']]);

        // $this->middleware('permission:soin-edit', ['only' => ['edit','update']]);
 
        // $this->middleware('permission:soin-delete', ['only' => ['destroy']]);

    }

     public function index()
    {
        //  $soins = Soin::get();
        // return view('soins/index',compact('soins'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
         $soins = Soin::latest()->get();

         // $specialites= Specialite::pluck('libelle','id');

        return view('soins/create', compact('soins'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SoinRequest $request)
    {
        $soin = Soin::create($request->all());

         return redirect(route('soin.create', $soin))->withInfo('Enregistrement réussi');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $soin= Soin::findOrFail($id);

        return view('soins/show',compact('soin'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $soin= Soin::findOrFail($id);

        return view('soins/edit',compact('soin'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SoinRequest $request, $id)
    {
        $soin = Soin::findOrfail($id);

        $soin->update($request->all());

        return redirect(route('soin.create', $id))->withSuccess('modification réussi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $soin = Soin::findOrfail($id);

        $soins = DB::table('consultation_produit_soin')->where('soin_id',$id)->get();

         $nb = count($soins);

         if($nb>0)
         {
             return redirect(route('soin.create', $soin))->withDanger(' Vous ne pouvez pas supprimez cet enregistrement car il est utilisé au niveau d\'une consultation');

         }

         // dd(count($soins));

        $soin->delete();

        return redirect(route('soin.create', $id))->withDanger('Suppression réussi');
    }
}
