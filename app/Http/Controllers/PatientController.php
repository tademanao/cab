<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PatientRequest;
use App\Patient;
use App\Constante;
use App\Consultation;
use DB;
use Auth;
use PDF;

class PatientController extends Controller
{
      public function __construct()
    {
        $this->middleware('auth');

        // $this->middleware('permission:patient-list|patient-create|patient-edit|patient-delete', ['only' => ['index','store']]);
        // $this->middleware('permission:patient-create', ['only' => ['create','store']]);

        // $this->middleware('permission:patient-edit', ['only' => ['edit','update']]);
 
        // $this->middleware('permission:patient-delete', ['only' => ['destroy']]);

    }
    

     public function index()
    {
        //  $patients = patient::get();
        // return view('patients/index',compact('patients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
         $patients = Patient::latest()->get();

         // $specialites= Specialite::pluck('libelle','id');

        return view('patients/create', compact('patients'));

        // return date('d-m-Y');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PatientRequest $request)
    {
        $patient = $request->all();

        $patient['date_inclusion']= date('Y-m-d');

        $date_inclusion = $patient['date_inclusion'];

        $date_naissance = $request->date_naissance;
          

        if(strtotime($date_naissance) >= strtotime($date_inclusion)){

            return redirect(route('patient.create', $patient))->withDanger('La date de naissance doit etre inferieure a celle du jour ');
        }

        $patient = Patient::create($patient);


         return redirect(route('patient.create', $patient))->withInfo('Enregistrement réussi');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $patient= Patient::findOrFail($id);

        return view('patients/show',compact('patient'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $patient= Patient::findOrFail($id);

         // $specialites= Specialite::pluck('libelle','id');

        return view('patients/edit',compact('patient'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PatientRequest $request, $id)
    {
        $patient = Patient::findOrfail($id);

        $date_inclusion = $patient->date_inclusion;

        $date_naissance = $patient->date_naissance;

         if(strtotime($date_naissance) >= strtotime($date_inclusion)){

            return redirect(route('patient.edit', $id))->withDanger('La date de naissance doit etre inferieure a celle du jour ');
        }


        $patient->update($request->all());

        return redirect(route('patient.create', $id))->withSuccess('modification réussi');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $patient = Patient::findOrfail($id);

        $patients = DB::table('consultations')->where('patient_id',$id)->get();

         $nb = count($patients);

         if($nb>0)
         {
             return redirect(route('patient.create', $patient))->withDanger(' Vous ne pouvez pas supprimez cet enregistrement car il est utilisé au niveau d\'une consultation');

         }


        $patient->delete();

        return redirect(route('patient.create', $id))->withDanger('Suppression réussi');
    }

      public function getConstante($id)
    {
       
        $constantes = Constante::where('patient_id',$id)->where('patient_id',$id)->latest()->take(1)->get();

        return view('constante_liste',compact('constantes'));

    }  

    public function getConsultation($id)
    {
        $consultations = Consultation::where('patient_id',$id)->where('patient_id',$id)->latest()->take(1)->get();

        return view('consultation_liste',compact('consultations'));

    }   

       /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
      public function pdfPatientParPeriode(Request $request)
      {

            if($request->date_deb && $request->date_fin)
            {
                $date_deb = $request->date_deb;
                $date_fin = $request->date_fin;

              $patients = Patient::whereBetween('date_inclusion',[$date_deb,$date_fin])->get();

              // $rolePermissions = Permission::join("permission_role","permission_role.permission_id","=","permissions.id")

              //   ->where("permission_role.role_id",$id)

              //   ->get();

              $pdf = PDF::loadView('consultations/pdf_patient_periode', compact('patients'));
          
              // return $pdf->download('consultations_par_date.pdf');

              return $pdf->stream();

             }
       }
}
