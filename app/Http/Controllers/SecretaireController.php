<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Secretaire;
use App\Role;
use App\User;
use App\Http\Requests\UserRequest;
use App\Http\Requests\UserUpdateRequest;
use DB;
use Auth;

use Hash;

class SecretaireController extends Controller
{
      public function __construct()
    {
        $this->middleware('auth');

        // $this->middleware('permission:secretaire-list|secretaire-create|secretaire-edit|secretaire-delete', ['only' => ['index','store']]);
        // $this->middleware('permission:secretaire-create', ['only' => ['create','store']]);

        // $this->middleware('permission:secretaire-edit', ['only' => ['edit','update']]);
 
        // $this->middleware('permission:secretaire-delete', ['only' => ['destroy']]);

    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
 

    public function index()
    {
         $secretaires = Secretaire::get();

        return view('secretaires/index',compact('secretaires'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $secretaires = Secretaire::get();
         $roles= Role::pluck('name','id');

        return view('secretaires/create',compact('secretaires','roles'));
    }
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        // $inputs = array_merge($request->all(),['password'=>Hash::make($request->password)]);
         // $secretaire = new secretaire();
         // $secretaire->save();

        $secretaire = Secretaire::create();

         $user = $request->all();
         $user['password']= bcrypt($request->password);
         $secretaire->user()->create($user);

         return redirect(route('secretaire.create', $secretaire))->withInfo('Enregistrement réussi');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $secretaire= Secretaire::findOrFail($id);
         $roles= Role::pluck('name','id');

        return view('secretaires/show',compact('secretaire','secretaires','roles'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $secretaire= Secretaire::findOrFail($id);
         $roles= Role::pluck('name','id');

        return view('secretaires/edit',compact('secretaire','secretaires','roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdateRequest $request, $id)
    {
         $secretaire = Secretaire::findOrfail($id);

         // $user = $request->except('_method','_token','password_confirmation');

          if(empty($request->password))
         {
            $secretaire->user->password = $secretaire->user->password;
         }

         $secretaire->user->username = $request->username;
         $secretaire->user->firstname = $request->firstname;
         $secretaire->user->lastname = $request->lastname;
         $secretaire->user->status = 'active';
         $secretaire->user->sexe = $request->sexe;
         $secretaire->user->phone = $request->phone;
         $secretaire->user->address = $request->address;
         $secretaire->user->role = $request->role;
         $secretaire->user->password = bcrypt($request->password);
         $secretaire->user->userable_id = $id;
         $secretaire->user->userable_type ='App\secretaire';
         $secretaire->user->save();

         // if(empty($request->password))
         // {
         //    $user['password'] = $secretaire->user->password;
         // }

         // $user['password'] = bcrypt($request->password);

         // $secretaire->user()->update($user);

        return redirect(route('secretaire.create', $id))->withSuccess('modification réussi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $secretaire = Secretaire::findOrfail($id);

        $secretaire->user()->delete();

        $secretaire->delete();

        return redirect(route('secretaire.create', $id))->withDanger('Suppression réussi');
    }


}
