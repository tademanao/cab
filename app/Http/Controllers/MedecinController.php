<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Medecin;
use App\User;
use App\Role;
use App\Http\Requests\UserRequest;
use App\Http\Requests\UserUpdateRequest;
use Hash;
use DB;
use Auth;

class MedecinController extends Controller
{
      public function __construct()
    {
        $this->middleware('auth');

        // $this->middleware('permission:medecin-list|medecin-create|medecin-edit|medecin-delete', ['only' => ['index','store']]);
        // $this->middleware('permission:medecin-create', ['only' => ['create','store']]);

        // $this->middleware('permission:medecin-edit', ['only' => ['edit','update']]);
 
        // $this->middleware('permission:medecin-delete', ['only' => ['destroy']]);

    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
 

    public function index()
    {
         $medecins = Medecin::get();

        return view('medecins/index',compact('medecins'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $medecins = Medecin::get();
         $roles= Role::pluck('name','id');

        return view('medecins/create',compact('roles','medecins'));
    }
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        // $inputs = array_merge($request->all(),['password'=>Hash::make($request->password)]);
         // $medecin = new Medecin();
         // $medecin->save();

        $medecin = Medecin::create();

         $user = $request->all();
         $user['password']= bcrypt($request->password);
         $medecin->user()->create($user);

         return redirect(route('medecin.create', $medecin))->withInfo('Enregistrement réussi');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $medecin= Medecin::findOrFail($id);

         $roles= Role::pluck('name','id');

        return view('medecins/show',compact('medecin','roles'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $medecin= Medecin::findOrFail($id);
         $roles= Role::pluck('name','id');

        return view('medecins/edit',compact('medecin','roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdateRequest $request, $id)
    {
         $medecin = Medecin::findOrfail($id);

         $user = User::where('userable_id',$id)->get();

         // $user = $request->except('_method','_token');

         if(empty($request->password))
         {
            $medecin->user->password = $medecin->user->password;
         }

         $medecin->user->username = $request->username;
         $medecin->user->firstname = $request->firstname;
         $medecin->user->lastname = $request->lastname;
         $medecin->user->status = 'active';
         $medecin->user->sexe = $request->sexe;
         $medecin->user->phone = $request->phone;
         $medecin->user->address = $request->address;
         $medecin->user->role = $request->role;
         $medecin->user->password = bcrypt($request->password);
         $medecin->user->userable_id = $id;
         $medecin->user->userable_type ='App\Medecin';
         $medecin->user->save();

         // $medecin->user()->update($user);

        return redirect(route('medecin.create', $id))->withSuccess('modification réussi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $medecin = Medecin::findOrfail($id);

        $medecins = DB::table('consultations')->where('userable_id',$id)->get();

         $nb = count($medecins);

         if($nb>0)
         {
             return redirect(route('typeConsultation.create', $medecin))->withDanger(' Vous ne pouvez pas supprimez cet enregistrement car il est utilisé au niveau d\'une consultation');

         }


        $medecin->user()->delete();

        $medecin->delete();

        return redirect(route('medecin.create', $id))->withDanger('Suppression réussi');
    }


}
