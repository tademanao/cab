<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Permission;
use App\Role;
use \DB;

class RoleController extends Controller
{

     public function __construct()
    {
        $this->middleware('auth');

        // $this->middleware('permission:role-list|role-create|role-edit|role-delete', ['only' => ['index','store']]);
        // $this->middleware('permission:role-create', ['only' => ['create','store']]);

        // $this->middleware('permission:role-edit', ['only' => ['edit','update']]);
 
        // $this->middleware('permission:role-delete', ['only' => ['destroy']]);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

      /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
         $roles = Role::get();

         $permissions = Permission::get();

        return view('roles/create', compact('roles','permissions'));


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $role = Role::create($request->all());

          // Creation du role
            $role = new Role();

            $role->name = $request->name;
            $role->save();

            // liaison role des permissions
            $permissions = $request->permissions;

            $role->permissions()->attach($permissions);

        return redirect(route('role.create', $role))->withInfo('Enregistrement réussi');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $role = Role::find($id);

         $rolePermissions = Permission::join("permission_role","permission_role.permission_id","=","permissions.id")

            ->where("permission_role.role_id",$id)

            ->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //  $role= Role::findOrFail($id);

        //  $permissions= Permission::pluck('libelle','id');

        // return view('roles/edit',compact('role','permissions'));

        $role= Role::findOrFail($id);

        $permissions = Permission::get();

         $rolePermissions = DB::table("permission_role")->where("permission_role.role_id",$id)

            ->pluck('permission_role.permission_id','permission_role.permission_id')

            ->all();

        // $rolePermissions = $role->permissions;



        return view('roles/edit',compact('role','permissions','rolePermissions'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $role = Role::findOrfail($id);

        // $role->update($request->all());


        $role = Role::findOrfail($id);

        $role->name = $request->input('name');

        $role->save();

        $role->permissions()->sync($request->input('permissions'));

      return redirect(route('role.create', $id))->withSuccess('modification réussi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Role::findOrfail($id);

        $permissions = $role->permissions;

        $role->permissions()->detach($permissions);

        $role->delete();

        return redirect(route('role.create', $id))->withDanger('Suppression réussi');
    }
}
