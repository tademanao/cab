<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\TypeAntecedentRequest;
use App\TypeAntecedent;
use DB;
use Auth;

class TypeAntecedentController extends Controller
{
      public function __construct()
    {
        // $this->middleware('auth');

        // $this->middleware('permission:role-list|role-create|role-edit|role-delete', ['only' => ['index','store']]);
        // $this->middleware('permission:role-create', ['only' => ['create','store']]);

        // $this->middleware('permission:role-edit', ['only' => ['edit','update']]);
 
        // $this->middleware('permission:role-delete', ['only' => ['destroy']]);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
       public function index()
    {
        //  $typeAntecedents = typeAntecedent::get();
        // return view('typeAntecedents/index',compact('typeAntecedents'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
         $typeAntecedents = TypeAntecedent::get();

         // $specialites= Specialite::pluck('libelle','id');

        return view('type_antecedents/create', compact('typeAntecedents'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TypeAntecedentRequest $request)
    {
        $typeAntecedent = TypeAntecedent::create($request->all());

         return redirect(route('typeAntecedent.create', $typeAntecedent))->withInfo('Enregistrement réussi');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $typeAntecedent = TypeAntecedent::findOrFail($id);

        return view('type_antecedents/edit',compact('typeAntecedent'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TypeAntecedentRequest $request, $id)
    {
        $typeAntecedent = TypeAntecedent::findOrfail($id);

        $typeAntecedent->update($request->all());

        return redirect(route('typeAntecedent.create', $id))->withSuccess('modification réussi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $typeAntecedent = TypeAntecedent::findOrfail($id);

        $typeAntecedent->delete();

        return redirect(route('typeAntecedent.create', $id))->withDanger('Suppression réussi');
    }
}
