<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ProduitRequest;
use App\Produit;
// use App\Mode
use DB;
use Auth;

class produitController extends Controller
{
      public function __construct()
    {
        $this->middleware('auth');

        // $this->middleware('permission:produit-list|produit-create|produit-edit|produit-delete', ['only' => ['index','store']]);
        // $this->middleware('permission:produit-create', ['only' => ['create','store']]);

        // $this->middleware('permission:produit-edit', ['only' => ['edit','update']]);
 
        // $this->middleware('permission:produit-delete', ['only' => ['destroy']]);

    }

    public function index()
    {
        //  $produits = produit::get();
        // return view('produits/index',compact('produits'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
         $produits = Produit::get();

         // $specialites= Specialite::pluck('libelle','id');

        return view('produits/create', compact('produits'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProduitRequest $request)
    {
        $produit = Produit::create($request->all());

         return redirect(route('produit.create', $produit))->withInfo('Enregistrement réussi');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $produit= Produit::findOrFail($id);

        return view('produits/show',compact('produit'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $produit= Produit::findOrFail($id);

        return view('produits/edit',compact('produit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProduitRequest $request, $id)
    {
        $produit = Produit::findOrfail($id);

        $produit->update($request->all());

        return redirect(route('produit.create', $id))->withSuccess('modification réussi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $produit = Produit::findOrfail($id);

         $produits = DB::table('consultation_produit_soin')->where('produit_id',$id)->get();

         $nb = count($produits);

         if($nb>0)
         {
             return redirect(route('produit.create', $produit))->withDanger(' Vous ne pouvez pas supprimez cet enregistrement car il est utilisé au niveau d\'une consultation');

         }


        $produit->delete();

        return redirect(route('produit.create', $id))->withDanger('Suppression réussi');
    }
}
