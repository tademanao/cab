<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ConsultationRequest;
use App\Http\Requests\ConsRequest;
use App\Consultation;
use App\Patient;
use App\Medecin;
use App\User;
use App\TypeConsultation;
use App\Ordonnance;
use App\Produit;
use App\Soin;
use App\Analyse;
use App\Medicament;
use App\Auth;
use \DB;
use PDF;


class ConsultationController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');

        // $this->middleware('permission:consultation-list|consultation-create|consultation-edit|consultation-delete', ['only' => ['index','store']]);
        // $this->middleware('permission:consultation-create', ['only' => ['create','store']]);

        // $this->middleware('permission:consultation-edit', ['only' => ['edit','update']]);

        // $this->middleware('permission:consultation-delete', ['only' => ['destroy']]);

}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index()
    {
       

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       $consultations = Consultation::latest()->get();

         // $patients= Patient::pluck('nom','id');

       // $typeConsultations= TypeConsultation::pluck('libelle','id');

       $typeConsultations = TypeConsultation::distinct()->get(['libelle']);

       // SELECT id FROM `type_consultations` WHERE libelle="chirurgien" ORDER BY date DESC LIMIT 1

       $count = count($typeConsultations);

       for ($i=0; $i < $count ; $i++) { 
           
           $tab[] = $typeConsultations[$i]->libelle; 
       }


       // dd($tab);

       // $pats= Patient::get();

       $patients = Patient::select("id", DB::raw("CONCAT(patients.nom,' ',patients.prenom) as Nomp"))
       ->pluck('Nomp', 'id');

       $medecins= User::select("id", DB::raw("CONCAT(users.lastname,' ',users.firstname) as Nom"))->where('userable_type','App\Medecin')->pluck('Nom', 'id');

       return view('consultations/create',compact('patients','consultations','medecins','tab'));
   }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ConsultationRequest $request)
    {

      $typeConsultations = TypeConsultation::distinct()->get(['libelle']);

         $count = count($typeConsultations);

       for ($i=0; $i < $count ; $i++) { 
           
           $tab[] = $typeConsultations[$i]->libelle; 
       }
        


      $consultation = $request->except('_token');

      $nb = $request->type_consultation_id;

      $libelle= $tab[$nb];

      // dd($tab[$nb]);

      $typeConsultation_id = TypeConsultation::where('libelle',$libelle)->latest()->take(1)->value('id');

      // dd($typeConsultation_id);

      $consultation['type_consultation_id'] = $typeConsultation_id;

      $consultation['date_consultation']= date('Y-m-d');

      $date_consultation = $consultation['date_consultation'];

        $date_rdv = $request->date_rdv;
          

        if(strtotime($date_rdv) < strtotime($date_consultation)){

            return redirect(route('consultation.create', $consultation))->withDanger('La date du rendez-vous est inférieure a celle de la consultation ');
        }

      $consultation['medecin_id']= $request->user()->userable_id;


      $consultation = Consultation::firstOrCreate($consultation);

      return redirect(route('consultation.create', $consultation))->withInfo('Enregistrement réussi');
  }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
         $typeConsultations = TypeConsultation::distinct()->get(['libelle']);

         $consultation = Consultation::findOrFail($id);

          $lib= TypeConsultation::findOrfail($consultation->type_consultation_id)->libelle;

           $count = count($typeConsultations);

           for ($i=0; $i < $count ; $i++) { 
               
               $tab[] = $typeConsultations[$i]->libelle; 

               if($tab[$i] == $lib )
               {
                    $num = $i;
               }
           }

       $typeConsultations= TypeConsultation::pluck('libelle','id');

       $patients = Patient::select("id", DB::raw("CONCAT(patients.nom,' ',patients.prenom) as Nomp"))
       ->pluck('Nomp', 'id');

       $medecins= User::select("id", DB::raw("CONCAT(users.lastname,' ',users.firstname) as Nom"))->where('userable_type','App\Medecin')->pluck('Nom', 'id');

       return view('consultations/show',compact('consultation','patients','medecins','tab','num') );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

       $typeConsultations = TypeConsultation::distinct()->get(['libelle']);

       $consultation = Consultation::findOrFail($id);

       $lib= TypeConsultation::findOrfail($consultation->type_consultation_id)->libelle;

       $count = count($typeConsultations);

       for ($i=0; $i < $count ; $i++) { 
           
           $tab[] = $typeConsultations[$i]->libelle; 

           if($tab[$i] == $lib )
           {
                $num = $i;
           }
       }


       $patients = Patient::select("id", DB::raw("CONCAT(patients.nom,' ',patients.prenom) as Nomp"))
       ->pluck('Nomp', 'id');

       $medecins= User::select("id", DB::raw("CONCAT(users.lastname,' ',users.firstname) as Nom"))->where('userable_type','App\Medecin')->pluck('Nom', 'id');

       return view('consultations/edit',compact('consultation','patients','medecins','tab','num') );
   }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ConsultationRequest $request, $id)
    {
       $typeConsultations = TypeConsultation::distinct()->get(['libelle']);

       $count = count($typeConsultations);

       for ($i=0; $i < $count ; $i++) { 
           
           $tab[] = $typeConsultations[$i]->libelle; 
       }

        $nb = $request->type_consultation_id;

        $libelle= $tab[$nb];

      // dd($tab[$nb]);

      $typeConsultation_id = TypeConsultation::where('libelle',$libelle)->latest()->take(1)->value('id');

      // dd($typeConsultation_id);

       $request->type_consultation_id = $typeConsultation_id;
     
       $consultation = Consultation::findOrFail($id);

       $consultations = $request->all();

      
       $consultations['type_consultation_id'] = $typeConsultation_id;

        // dd($consultations);

       // dd($consultation->type_consultation_id);

       $consultation->medecin_id = $request->user()->userable_id;

        $date_rdv = $request->date_rdv;
          

        if(strtotime($date_rdv) < strtotime($consultation->date_consultation)){

            return redirect(route('consultation.create', $consultation))->withDanger('La date du rendez-vous est inférieure a celle du jour ');
        }

        // dd($request->all());

       $consultation->update($consultations);

       return redirect(route('consultation.create', $id))->withSuccess('modification réussi');
   }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $consultation = consultation::findOrfail($id);

         $cons1 = DB::table('ordonnances')->where('consultation_id',$id)->get();
         $cons2 = DB::table('consultation_produit_soin')->where('consultation_id',$id)->get();
         $cons3 = DB::table('analyse_consultation')->where('consultation_id',$id)->get();

         $nb1 = count($cons1);
         $nb2 = count($cons2);
         $nb3 = count($cons3);

         if($nb1>0 || $nb2>0 || $nb3>0)
         {
             return redirect(route('consultation.create', $consultation))->withDanger(' Vous ne pouvez pas supprimez cet enregistrement car il est utilisé au niveau d\'une ordonnance ou analyse ou soin');

         }


        $consultation->delete();

        return redirect(route('consultation.create', $id))->withDanger('Suppression réussi');
    }

      public function rendezVous()
    {
        $date = date('Y-m-d');

        $consultations = Consultation::whereColumn('date_consultation','!=','date_rdv')->where('date_rdv',$date)->get();

        return view('consultations/index', compact('consultations') );

    }

     public function rdv()
    {

        $date = date('Y-m-d');

        $consultations = Consultation::latest()->get();

        return view('consultations/rdv', compact('consultations') );

    }

      /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editRdv($id)
    {

       $consultation = Consultation::findOrFail($id);
       $medecins= User::select("id", DB::raw("CONCAT(users.lastname,' ',users.firstname) as Nom"))->where('userable_type','App\Medecin')->pluck('Nom', 'id');
       $patients = Patient::select("id", DB::raw("CONCAT(patients.nom,' ',patients.prenom) as Nomp"))
       ->pluck('Nomp', 'id');

       return view('consultations/editRdv',compact('consultation','medecins','patients') );
   }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateRdv(Request $request, $id)
    {
       
          $consultation = Consultation::findOrFail($id);
          $date_rdv = $request->date_rdv;

        if( (strtotime($date_rdv) < strtotime($consultation->date_consultation)) || (strtotime($date_rdv) <= strtotime(date('d-m-Y'))) )
        {

            return redirect(route('rdv_edit', $consultation))->withDanger('La date du rendez-vous doit etre superieure a celle du jour ');
        }

      
  

       $consultation->update($request->all());


       return redirect(route('rdv'))->withSuccess('modification réussi');
   }


    // a partir d'ici je traite l'ordonnance, les soins et analyses ayant trait a une consultation

     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function ordonnance($id)
     {
        $consultation = Consultation::findOrFail($id);

        $ordonnanceId = DB::table('ordonnances')->where('consultation_id', $consultation->id)->value('id');

        $ordonnances = DB::table('medicament_ordonnance')->where('ordonnance_id',$ordonnanceId)
        ->join('medicaments', 'medicament_ordonnance.medicament_id', '=', 'medicaments.id')->latest()
        ->select('medicament_ordonnance.*','medicaments.libelle_medicament', 'medicament_ordonnance.posologie')
        ->get();   

        // dd($ordonnances);

        $medicaments = Medicament::pluck('libelle_medicament','id');     

        return view('consultations/ordonnance', compact('consultation','medicaments','ordonnances') );
    }

     /**
     * 
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function postOrdonnance(Request $request)
     {   

        $date = date('Y-m-d');

        $ordonnance = Ordonnance::firstOrCreate(['consultation_id'=>$request->id]);

        $id = $request->id;

        $medicament = $request->medicament_id;

        $ordonnance->medicaments()->attach($medicament,['posologie'=>$request->posologie,'date_ordonnance'=>$date]);


           // $consultation = Consultation::findOrFail($id);


           // $consultation->update($request->all());

        return redirect(route('ordonnance', $id ))->withInfo('Enregistrement réussi');
    }

      /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
      public function editOrdonnance($id)
      {
        // $consultation = Consultation::findOrFail($id);

        // $id = intval($id);

        // $ordonnance = DB::table('medicament_ordonnance')
        //     ->join('ordonnances', 'medicament_ordonnance.ordonnance_id', '=', 'ordonnances.id')
        //     ->join('medicaments', 'medicament_ordonnance.medicament_id', '=', 'medicaments.id')
        //     ->where('medicament_ordonnance.id',$id)
        //     ->select('medicament_ordonnance.*','medicaments.libelle_medicament', 'medicament_ordonnance.posologie')
        //     ->get(); 

       $ordonnance = DB::table('medicament_ordonnance')->find($id);    

       $ord = Ordonnance::findOrfail($ordonnance->ordonnance_id);

        $consultation = $ord->consultation_id;

       $medicaments = Medicament::pluck('libelle_medicament','id');     

       return view('consultations/editOrdonnance', compact('medicaments','ordonnance','consultation') );
   }


     /**
     * 
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function updateOrdonnance(Request $request, $id)
     {   

        // return'ok';

        // $date = date('Y-m-d');

        $medicamentOrdonnance = DB::table('medicament_ordonnance')->find($id); 
        $ordonnanceId = $medicamentOrdonnance->ordonnance_id;

        $ordonnance = Ordonnance::findOrfail($ordonnanceId);

        $consultation = $ordonnance->consultation_id;

        // dd($consultation);

        DB::table('medicament_ordonnance')
        ->where('id', $id)
        ->update(['posologie' => $request->posologie, 'medicament_id'=>$request->medicament_id]);

        return redirect(route('ordonnance',$consultation))->withSuccess('Modification réussi');
    }


     /**
     * 
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroyOrdonnance($id)
     {   

        // return'ok';

        // $date = date('Y-m-d');

        $medicamentOrdonnance = DB::table('medicament_ordonnance')->find($id); 
        $ordonnanceId = $medicamentOrdonnance->ordonnance_id;

        $ordonnance = Ordonnance::findOrfail($ordonnanceId);

        $consultation = $ordonnance->consultation_id;

        // dd($consultation);

        DB::delete('delete from medicament_ordonnance where id = ?', [$id]);

        return redirect(route('ordonnance',$consultation))->withDanger('Suppresion réussi');
    }

    //analyses d'une consultation

     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function analyse($id)
     {
        $consultation = Consultation::findOrFail($id);

        // DB::raw('SUM(price) as total_sales')

        $montant_analyse = DB::table('analyse_consultation')->where('consultation_id',$consultation->id)->sum('montant');

        $analyse_consultations = DB::table('analyse_consultation')->where('consultation_id', $consultation->id)
        ->join('analyses', 'analyse_consultation.analyse_id', '=', 'analyses.id')
        ->join('consultations', 'analyse_consultation.consultation_id', '=', 'consultations.id')->latest()
        ->select('analyse_consultation.*','analyses.libelle_analyse', 'analyse_consultation.quantite_analyse','analyse_consultation.date_analyse','analyse_consultation.resultat')
        ->get();   

        $analyses = Analyse::pluck('libelle_analyse','id');     

        return view('consultations/analyse', compact('consultation','analyse_consultations','analyses','montant_analyse') );
    }


     /**
     * 
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function postAnalyse(Request $request)
     {   

        $date = date('Y-m-d');

        $id = $request->id;

        $consultation = Consultation::findOrfail($id);

        $analyseId = $request->analyse_id;

        $analyse = Analyse::findOrfail($analyseId);

        $prix = $analyse->prix_analyse;

        $montant = $prix * $request->quantite_analyse;


        $consultation->analyses()->attach($analyseId,['quantite_analyse'=>$request->quantite_analyse,'date_analyse'=>$date,'resultat'=>$request->resultat,'montant'=>$montant]);

        return redirect(route('analyse', $id ))->withInfo('Enregistrement réussi');
    }

      /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
      public function editAnalyse($id)
      {
        // $consultation = Consultation::findOrFail($id);

        // $id = intval($id);

        // $ordonnance = DB::table('medicament_ordonnance')
        //     ->join('ordonnances', 'medicament_ordonnance.ordonnance_id', '=', 'ordonnances.id')
        //     ->join('medicaments', 'medicament_ordonnance.medicament_id', '=', 'medicaments.id')
        //     ->where('medicament_ordonnance.id',$id)
        //     ->select('medicament_ordonnance.*','medicaments.libelle_medicament', 'medicament_ordonnance.posologie')
        //     ->get(); 

       $analyse = DB::table('analyse_consultation')->find($id);    

       $analyses = Analyse::pluck('libelle_analyse','id');     

       return view('consultations/editAnalyse', compact('analyses','analyse') );
   }

     /**
     * 
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function updateAnalyse(Request $request, $id)
     {   

        // return'ok';

        // $date = date('Y-m-d');

        $analyse = DB::table('analyse_consultation')->find($id); 

        $consultation = $analyse->consultation_id;

        $analyseId = $request->analyse_id;

        $analyse = Analyse::findOrfail($analyseId);

        $prix = $analyse->prix_analyse;

        $montant = $prix * $request->quantite_analyse;

        // dd($consultation);

        DB::table('analyse_consultation')
        ->where('id', $id)
        ->update(['quantite_analyse' => $request->quantite_analyse, 'analyse_id'=>$request->analyse_id,'resultat'=>$request->resultat,'montant'=>$montant]);

        return redirect(route('analyse',$consultation))->withSuccess('Modification réussi');
    }


     /**
     * 
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroyAnalyse($id)
     {   

        // return'ok';

        // $date = date('Y-m-d');


        $analyse = DB::table('analyse_consultation')->find($id); 

        $consultation = $analyse->consultation_id;


        // dd($consultation);

        DB::delete('delete from analyse_consultation where id = ?', [$id]);

        return redirect(route('analyse',$consultation))->withDanger('Suppresion réussi');
    }


    // soins d'une consultation

 /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
 public function soin($id)
 {
    $consultation = Consultation::findOrFail($id);

    //     Avoir le montant des soins de la consultation
    // $consultation_produit_soin = DB::table('consultation_produit_soin')->where('consultation_id',$consultation->id)->distinct()->get(['montant_soin']);

    // $count = count($consultation_produit_soin);

    // $montant = 0;

    // for ($i=0; $i < $count ; $i++) { 
        
    //     $montant = $montant + $consultation_produit_soin[$i]->montant_soin;
    // }

    // $montant_total_soin;

     
      //   tableau de la liste des produits de la consultation
      // $consultation_produit_soins = DB::table('consultation_produit_soin')->where('consultation_id',$consultation->id)->join('soins', 'consultation_produit_soin.soin_id', '=', 'soins.id')->select('soins.*');
     

    $consultation_produit_soins = DB::table('consultation_produit_soin')->where('consultation_id',$consultation->id)
    ->join('soins', 'consultation_produit_soin.soin_id', '=', 'soins.id')
    ->join('consultations', 'consultation_produit_soin.consultation_id', '=', 'consultations.id')
    ->join('produits', 'consultation_produit_soin.produit_id', '=', 'produits.id')->latest()
    ->select('consultation_produit_soin.*','soins.libelle_soin', 'consultation_produit_soin.quantite_produit','consultation_produit_soin.date_soin','produits.libelle_produit','produits.prix_produit')
    ->get();   

     $montant_produit = DB::table('consultation_produit_soin')->where('consultation_id',$consultation->id)->sum('montant_produit');

    $soins = Soin::pluck('libelle_soin','id');     
    $produits = Produit::pluck('libelle_produit','id'); 

    return view('consultations/soin', compact('consultation','consultation_produit_soins','soins','produits','montant_produit') );
}


     /**
     * 
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function postSoin(Request $request)
     {   

        $date = date('Y-m-d');

        $id = $request->id;

        $consultation = Consultation::findOrfail($id);

        $soinId = $request->soin_id;

        $soin = Soin::findOrfail($soinId);

        $montant_soin = $soin->prix_soin;

        $produitId = $request->produit_id;

        $produit = Produit::findOrfail($produitId);

        $prix = $produit->prix_produit;

        $montant_produit = $prix * $request->quantite_produit;

        $consultation->soins()->attach($soinId,['produit_id'=>$request->produit_id,'quantite_produit'=>$request->quantite_produit,'date_soin'=>$date,'montant_produit'=>$montant_produit,'montant_soin'=>$montant_soin]);

        return redirect(route('soin', $id ))->withInfo('Enregistrement réussi');
    }


      /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
      public function editSoin($id)
      {
        // $consultation = Consultation::findOrFail($id);

        // $id = intval($id);

        // $ordonnance = DB::table('medicament_ordonnance')
        //     ->join('ordonnances', 'medicament_ordonnance.ordonnance_id', '=', 'ordonnances.id')
        //     ->join('medicaments', 'medicament_ordonnance.medicament_id', '=', 'medicaments.id')
        //     ->where('medicament_ordonnance.id',$id)
        //     ->select('medicament_ordonnance.*','medicaments.libelle_medicament', 'medicament_ordonnance.posologie')
        //     ->get(); 

        $consultation_produit_soin = DB::table('consultation_produit_soin')->find($id);    

        $soins = Soin::pluck('libelle_soin','id');     
        $produits = Produit::pluck('libelle_produit','id');    

        return view('consultations/editSoin', compact('soins','produits','consultation_produit_soin') );
    }

     /**
     * 
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function updateSoin(Request $request, $id)
     {   

        // return'ok';

        // $date = date('Y-m-d');

        $consultation_produit_soin = DB::table('consultation_produit_soin')->find($id); 

        $consultation = $consultation_produit_soin->consultation_id;

        $produitId = $request->produit_id;

        $produit = Produit::findOrfail($produitId);

        $soinId = $request->soin_id;

        $soin = Soin::findOrfail($soinId);

        $montant_soin = $soin->prix_soin;

        $prix = $produit->prix_produit;

        $montant_produit = $prix * $request->quantite_produit;

        // dd($consultation);

        DB::table('consultation_produit_soin')
        ->where('id', $id)
        ->update(['soin_id'=>$request->soin_id,'produit_id'=>$request->produit_id,'quantite_produit'=>$request->quantite_produit,'montant_produit'=>$montant_produit,'montant_soin'=>$montant_soin]);

        return redirect(route('soin',$consultation))->withSuccess('Modification réussi');
    }

/**
     * 
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
        public function destroySoin($id)
        {   

                // return'ok';

                // $date = date('Y-m-d');


            $analyse = DB::table('consultation_produit_soin')->find($id); 

            $consultation = $analyse->consultation_id;


                // dd($consultation);

            DB::delete('delete from consultation_produit_soin where id = ?', [$id]);

            return redirect(route('soin',$consultation))->withDanger('Suppresion réussi');
        }

    // A partir d'ici je traite les etats ayant un rapport avec la consultation

          public function etat()
        {   

             $patients = Patient::select("id", DB::raw("CONCAT(patients.nom,' ',patients.prenom) as Nomp"))
            ->pluck('Nomp', 'id');


            return view('consultations/etat', compact('patients') );
        }

             /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function pdfFacture($id)
     {

        // $id = $request->id;

        $consultation = Consultation::findOrfail($id);

    //      $consultation_produit_soins = DB::table('consultation_produit_soin')->where('consultation_id',$id)
    // ->join('soins', 'consultation_produit_soin.soin_id', '=', 'soins.id')
    // ->join('consultations', 'consultation_produit_soin.consultation_id', '=', 'consultations.id')
    // ->join('produits', 'consultation_produit_soin.produit_id', '=', 'produits.id')
    // ->select('consultation_produit_soin.*','soins.libelle_soin', 'consultation_produit_soin.quantite_produit','consultation_produit_soin.date_soin','produits.libelle_produit','produits.prix_produit')
    // ->get();   

     // $montant_produit = DB::table('consultation_produit_soin')->where('consultation_id',$id)->sum('montant_produit');

        $montant_analyse = DB::table('analyse_consultation')->where('consultation_id',$id)->sum('montant');

        $analyses = DB::table('analyse_consultation')->where('consultation_id', $id)
        ->join('analyses', 'analyse_consultation.analyse_id', '=', 'analyses.id')
        ->join('consultations', 'analyse_consultation.consultation_id', '=', 'consultations.id')
        ->select('analyse_consultation.*','analyses.libelle_analyse', 'analyse_consultation.quantite_analyse','analyse_consultation.date_analyse','analyses.prix_analyse','analyse_consultation.resultat')
        ->get();

         $produits = DB::table('consultation_produit_soin')->where('consultation_id',$consultation->id)
    ->join('soins', 'consultation_produit_soin.soin_id', '=', 'soins.id')
    ->join('consultations', 'consultation_produit_soin.consultation_id', '=', 'consultations.id')
    ->join('produits', 'consultation_produit_soin.produit_id', '=', 'produits.id')
    ->select('consultation_produit_soin.*','soins.libelle_soin', 'consultation_produit_soin.quantite_produit','consultation_produit_soin.date_soin','produits.libelle_produit','produits.prix_produit')
    ->get();   

     $montant_total_produit = DB::table('consultation_produit_soin')->where('consultation_id',$consultation->id)->sum('montant_produit');

         //     Avoir le montant des soins de la consultation
    $consul_prod_soin = DB::table('consultation_produit_soin')->where('consultation_id',$id)->distinct()->get(['montant_soin']);

    $count = count($consul_prod_soin);

    $montant_total_soin = 0;

    for ($i=0; $i < $count ; $i++) { 
        
        $montant_total_soin = $montant_total_soin + $consul_prod_soin[$i]->montant_soin;
    }

     
      //   tableau de la liste des soins de la consultation
        $consultation_produit_soin = DB::table('consultation_produit_soin')->where('consultation_id',$consultation->id)->distinct()->get(['soin_id']);
   

            $countSoin = count($consultation_produit_soin);


            if($countSoin == 0 )
            {
                $soins = null;
            }

              for ($i=0; $i<$countSoin ; $i++) { 
                
                  $soin = Soin::findOrfail($consultation_produit_soin[$i]->soin_id);

                  $soins[]= $soin;
              }

              $prix_consultation = $consultation->typeConsultation->prix_consultation;

              $totaux =$montant_analyse + $montant_total_soin + $montant_total_produit + $prix_consultation;

        // $soins = Soin::join("consultation_produit_soin","consultation_produit_soin.soin_id","=","soins.id")->where("consultation_produit_soin.role_id",$role)->get()->toArray();


        $pdf = PDF::loadView('consultations/pdf_consultation_facture', compact('consultation','analyses','montant_analyse','soins','montant_total_soin','montant_total_soin','produits','montant_total_produit','totaux'));

         return $pdf->stream();

        // return view('consultations/pdf_consultation_ordonnance', compact('ordonnances') );
    }

         /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function pdfOrdonnance(Request $request)
     {

        $id = $request->id;

        $consultation = Consultation::findOrFail($id);

        $ordonnanceId = DB::table('ordonnances')->where('consultation_id', $consultation->id)->value('id');

        $ordonnances = DB::table('medicament_ordonnance')->where('ordonnance_id',$ordonnanceId)
        ->join('medicaments', 'medicament_ordonnance.medicament_id', '=', 'medicaments.id')
        ->select('medicament_ordonnance.*','medicaments.libelle_medicament', 'medicament_ordonnance.posologie')
        ->get();



        // dd($ordonnances);  

         $pdf = PDF::loadView('consultations/pdf_consultation_ordonnance', compact('ordonnances','consultation'));

         return $pdf->stream();

        // return view('consultations/pdf_consultation_ordonnance', compact('ordonnances') );
    }

      /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
      public function pdfConsultationParDate(Request $request)
      {

            if($request->date_consultation)
            {

              $consultations = Consultation::where('date_consultation',$request->date_consultation)->get();

              // $rolePermissions = Permission::join("permission_role","permission_role.permission_id","=","permissions.id")

              //   ->where("permission_role.role_id",$id)

              //   ->get();

              $pdf = PDF::loadView('consultations/pdf_consultation_date', compact('consultations'));
          
              // return $pdf->download('consultations_par_date.pdf');

              return $pdf->stream();

             }
       }


         /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
      public function pdfConsultationParPeriode(Request $request)
      {

            if($request->date_deb && $request->date_fin)
            {
                $date_deb = $request->date_deb;
                $date_fin = $request->date_fin;

              $consultations = Consultation::whereBetween('date_consultation',[$date_deb,$date_fin])->get();

              // $rolePermissions = Permission::join("permission_role","permission_role.permission_id","=","permissions.id")

              //   ->where("permission_role.role_id",$id)

              //   ->get();

              $pdf = PDF::loadView('consultations/pdf_consultation_periode', compact('consultations'));
          
              // return $pdf->download('consultations_par_date.pdf');

              return $pdf->stream();

             }
       }

  /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
      public function pdfConsultationParPatient(Request $request)
      {

            if($request->patient_id)
            {

              $consultations = Consultation::where('patient_id',$request->patient_id)->latest()->get();

              // $rolePermissions = Permission::join("permission_role","permission_role.permission_id","=","permissions.id")

              //   ->where("permission_role.role_id",$id)

              //   ->get();

              $pdf = PDF::loadView('consultations/pdf_consultation_patient', compact('consultations'));
          
              // return $pdf->download('consultations_par_date.pdf');

              return $pdf->stream();

             }
       }

       /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
      public function pdfConsultationParPatientPeriode(Request $request)
      {

            if($request->patient_id && $request->date_deb && $request->date_fin)
            {
                $date_deb = $request->date_deb;
                $date_fin = $request->date_fin;

              $consultations = Consultation::where('patient_id',$request->patient_id)->whereBetween('date_consultation',[$date_deb,$date_fin])->get();

              // $rolePermissions = Permission::join("permission_role","permission_role.permission_id","=","permissions.id")

              //   ->where("permission_role.role_id",$id)

              //   ->get();

              $pdf = PDF::loadView('consultations/pdf_consultation_patient_periode', compact('consultations'));
          
              // return $pdf->download('consultations_par_date.pdf');

              return $pdf->stream();

             }
       }


}
