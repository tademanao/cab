<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\TypeConsultationRequest;
use App\TypeConsultation;
use App\Consultation;
use App\PrixHistorique;
use DB;
use Auth;



class TypeConsultationController extends Controller
{
      public function __construct()
    {
        $this->middleware('auth');

        // $this->middleware('permission:typeConsultation-list|typeConsultation-create|typeConsultation-edit|typeConsultation-delete', ['only' => ['index','store']]);
        // $this->middleware('permission:typeConsultation-create', ['only' => ['create','store']]);

        // $this->middleware('permission:typeConsultation-edit', ['only' => ['edit','update']]);
 
        // $this->middleware('permission:typeConsultation-delete', ['only' => ['destroy']]);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
    {
        //  $TypeConsultations = TypeConsultation::get();
        // return view('TypeConsultations/index',compact('TypeConsultations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
         $typeConsultations = TypeConsultation::get();

         // $typeConsultations = DB::table('type_consultations')->latest()->get()->toArray();

         // $count = DB::table('type_consultations')->count();

         // for ($i=0; $i<$count; $i++)
         // {
         //    $typeConsultations[]
         // }


         // dd($typeConsultations);

         // $specialites= Specialite::pluck('libelle','id');

        return view('type_consultations/create', compact('typeConsultations'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TypeConsultationRequest $request)
    {
        $typeConsultation = $request->all();

        $typeConsultation['date'] = date('Y/m/d');

        $typeConsultation = TypeConsultation::Create($typeConsultation);

        $PrixHistorique = new PrixHistorique();

        $PrixHistorique->libelle = $request->libelle;
        $PrixHistorique->prix_consultation = $request->prix_consultation;
        $PrixHistorique->date = $typeConsultation['date'];
        $PrixHistorique->status = 'creation';
        $PrixHistorique->save();


         return redirect(route('typeConsultation.create', $typeConsultation))->withInfo('Enregistrement réussi');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $typeConsultation = TypeConsultation::findOrFail($id);

        return view('type_consultations/show',compact('typeConsultation'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $typeConsultation = TypeConsultation::findOrFail($id);

        return view('type_consultations/edit',compact('typeConsultation'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TypeConsultationRequest $request, $id)
    {
        $typeConsultation = TypeConsultation::findOrfail($id);

        // dd($typeConsultation->prix_consultation);
        

        $prix_consultation = (double) $request->prix_consultation;

        // dd($prix_consultation);

        $data = $request->all();

        $data['date'] = date('Y/m/d');


         // if($typeConsultation->prix_consultation !== $prix_consultation)

            $PrixHistorique = new PrixHistorique();

            $PrixHistorique->libelle = $request->libelle;
            $PrixHistorique->prix_consultation = $request->prix_consultation;
            $PrixHistorique->date = $data['date'];
            $PrixHistorique->status = 'modification';
            $PrixHistorique->save();
        
            
        $typeConsultation->update($data);

        return redirect(route('typeConsultation.create', $id))->withSuccess('modification réussi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $typeConsultation = TypeConsultation::findOrfail($id);

         $consultations = DB::table('consultations')->where('type_consultation_id',$id)->get();

         $nb = count($consultations);

         if($nb>0)
         {
             return redirect(route('typeConsultation.create', $typeConsultation))->withDanger(' Vous ne pouvez pas supprimez cet enregistrement car il est utilisé au niveau d\'une consultation');

         }

        $typeConsultation->delete();

        return redirect(route('typeConsultation.create', $id))->withDanger('Suppression réussi');
    }


      public function historique()
    {
         $prixHistoriques = PrixHistorique::get();
        return view('Type_consultations/historique',compact('prixHistoriques'));
    }

}
