<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Medecin;
use App\Infirmiere;
use App\Secretaire;
use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\UserProfilRequest;
use \Auth;
use \Hash;
use Validator;



class ProfilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profil = User::find(Auth::id());
     
        return view('profil',compact('profil') );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

   public function update(UserProfilRequest $request, $id)
    {
       
        $profil = $request->except('_method','_token');

        $user = User::findOrfail($request->id);

        if($user->userable_type === 'App\Infirmiere')
        {
            $infirmiere = Infirmiere::findOrfail($user->userable_id);

            $infirmiere->user()->update($profil);
        }

         if($user->userable_type === 'App\Medecin')
        {
            $medecin = Medecin::findOrfail($user->userable_id);

            $medecin->user()->update($profil);
        }

         if($user->userable_type === 'App\Secretaire')
        {
            $secretaire = Secretaire::findOrfail($user->userable_id);

            $secretaire->user()->update($profil);
        }

        return redirect(route('profil'))->withSuccess('modification réussi');

    }


    public function change_password(Request $request)

    {
     
        $profil = $request->except('_token');

        $profil['password'] = bcrypt($request->password);

        $user = User::find(Auth::id());



        if( !Hash::check($request['old_password'], Auth::user()->password)  )
        {
          return redirect(route('profil'))->withDanger('l\'ancien mot de passe entre est incorrect');
        }

         $validator = Validator::make($request->all(), [ 
            'old_password' => 'required',
            'password' => 'required',
            'password_confirmation' => 'required|same:password'
        ]); 

         if($validator->fails()){

            return redirect(route('profil'))->withErrors($validator->errors());

         }
         else
         {

                if($user->userable_type === 'App\Infirmiere')
                {
                    $infirmiere = Infirmiere::findOrfail($user->userable_id);

                    $infirmiere->user()->update(['password'=> $profil['password'] ]);
                }

                 if($user->userable_type === 'App\Medecin')
                {
                    $medecin = Medecin::findOrfail($user->userable_id);

                    $medecin->user()->update(['password'=> $profil['password'] ]);
                }

                 if($user->userable_type === 'App\Secretaire')
                {
                    $secretaire = Secretaire::findOrfail($user->userable_id);

                    $secretaire->user()->update(['password'=> $profil['password'] ]);
                }

               

         }

           return redirect(route('profil'))->withSuccess('modification réussi');

       

    }      


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
