<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Antecedent;
use App\Patient;
use App\TypeAntecedent;
use App\Http\Requests\AntecedentRequest;
use Illuminate\Support\Facades\Auth;
use \DB;



class AntecedentController extends Controller
{

      public function __construct()
    {
        // $this->middleware('auth');

        // $this->middleware('permission:antecedent-list|antecedent-create|antecedent-edit|antecedent-delete', ['only' => ['index','store']]);
        // $this->middleware('permission:antecedent-create', ['only' => ['create','store']]);

        // $this->middleware('permission:antecedent-edit', ['only' => ['edit','update']]);
 
        // $this->middleware('permission:antecedent-delete', ['only' => ['destroy']]);

    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
 
    public function index()
    {
         $antecedents = Antecedent::get();

        return view('antecedents/index',compact('antecedents'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {  

         $antecedents = Antecedent::get();

         $patients= Patient::pluck('nom','id');

         $typeAntecedents= TypeAntecedent::pluck('libelle','id');
         
        return view('antecedents/create',compact('patients','antecedents','typeAntecedents'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AntecedentRequest $request)
    {
        // $antecedents = Antecedent::get()->where(['patient_id']);

       // $antecedents = DB::table('antecedents')->where('patient_id',$request->patient_id)->where('type_antecedent_id',$request->type_antecedent_id)->where('description',$request->description)->get();

       
        $antecedent = Antecedent::firstOrCreate(['patient_id'=>$request->patient_id,'type_antecedent_id'=>$request->type_antecedent_id,'description'=>$request->description]);

         return redirect(route('antecedent.create', $antecedent))->withInfo('Enregistrement réussi');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $antecedent = Antecedent::findOrFail($id);
        
        $patients = Patient::pluck('nom','id');

        $typeAntecedents = TypeAntecedent::pluck('libelle','id');

        return view('antecedents/edit',compact('antecedent','patients','typeAntecedents'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AntecedentRequest $request, $id)
    {
            $antecedent = Antecedent::findOrfail($id);

            $antecedent->update($request->all());

             return redirect(route('antecedent.create', $id))->withSuccess('modification réussi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $antecedent = Antecedent::findOrfail($id);

        $antecedent->delete();

        return redirect(route('antecedent.create', $id))->withDanger('Suppression réussi');
    }
}
