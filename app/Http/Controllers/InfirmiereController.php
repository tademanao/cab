<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Infirmiere;
use App\User;
use App\Http\Requests\UserRequest;
use App\Role;
use App\Http\Requests\UserUpdateRequest;
use Hash;
use DB;
use Auth;

class InfirmiereController extends Controller
{
      public function __construct()
    {
        $this->middleware('auth');

        // $this->middleware('permission:infirmiere-list|infirmiere-create|infirmiere-edit|infirmiere-delete', ['only' => ['index','store']]);
        // $this->middleware('permission:infirmiere-create', ['only' => ['create','store']]);

        // $this->middleware('permission:infirmiere-edit', ['only' => ['edit','update']]);
 
        // $this->middleware('permission:infirmiere-delete', ['only' => ['destroy']]);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
 

    public function index()
    {
        //  $infirmieres = Infirmiere::get();

        // return view('infirmieres/index',compact('infirmieres'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

         $roles = Role::pluck('name','id');

         // $infirmieres = User::where('userable_type','App\Infirmiere')->get();

            $infirmieres = Infirmiere::get();

         return view('infirmieres/create',compact('roles','infirmieres'));
    }
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        // $inputs = array_merge($request->all(),['password'=>Hash::make($request->password)]);
         // $infirmiere = new infirmiere();
         // $infirmiere->save();

        $infirmiere = Infirmiere::create();

         $user = $request->all();
         $user['password']= bcrypt($request->password);
         $infirmiere->user()->create($user);

         return redirect(route('infirmiere.create', $infirmiere))->withInfo('Enregistrement réussi');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $infirmiere = Infirmiere::findOrfail($id);

        $roles = Role::pluck('name','id');

        return view('infirmieres/show',compact('roles','infirmiere'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $infirmiere = Infirmiere::findOrfail($id);

        $roles = Role::pluck('name','id');

        return view('infirmieres/edit',compact('roles','infirmiere'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdateRequest $request, $id)
    {
         $infirmiere = Infirmiere::findOrfail($id);

         // $user = $request->except('_method','_token','password_confirmation');

         // if(empty($request->password))
         // {
         //    $user['password'] = $infirmiere->user->password;
         // }

         // $user['password'] = bcrypt($request->password);

         // $infirmiere->user()->update($user);

          if(empty($request->password))
         {
            $infirmiere->user->password = $infirmiere->user->password;
         }

         $infirmiere->user->username = $request->username;
         $infirmiere->user->firstname = $request->firstname;
         $infirmiere->user->lastname = $request->lastname;
         $infirmiere->user->status = 'active';
         $infirmiere->user->sexe = $request->sexe;
         $infirmiere->user->phone = $request->phone;
         $infirmiere->user->address = $request->address;
         $infirmiere->user->role = $request->role;
         $infirmiere->user->password = bcrypt($request->password);
         $infirmiere->user->userable_id = $id;
         $infirmiere->user->userable_type ='App\infirmiere';
         $infirmiere->user->save();

        return redirect(route('infirmiere.create', $id))->withSuccess('modification réussi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $infirmiere = Infirmiere::findOrfail($id);

        $infirmiere->user()->delete();

        $infirmiere->delete();

        return redirect(route('infirmiere.create', $id))->withDanger('Suppression réussi');
    }


}
