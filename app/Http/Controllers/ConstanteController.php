<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ConstanteRequest;
use App\Constante;
use App\Patient;
use \DB;
use Illuminate\Support\Facades\Auth;

class ConstanteController extends Controller
{

      public function __construct()
    {
        $this->middleware('auth');

        // $this->middleware('permission:constante-list|constante-create|constante-edit|constante-delete', ['only' => ['index','store']]);
        // $this->middleware('permission:constante-create', ['only' => ['create','store']]);

        // $this->middleware('permission:constante-edit', ['only' => ['edit','update']]);
 
        // $this->middleware('permission:constante-delete', ['only' => ['destroy']]);

    }

    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
 
    public function index()
    {
         $constantes = Constante::get();

        return view('constantes/index',compact('constantes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {  

         $constantes = Constante::latest()->get();

         $patients = Patient::select("id", DB::raw("CONCAT(patients.nom,' ',patients.prenom) as Nomp"))
       ->pluck('Nomp', 'id');
         
        return view('constantes/create',compact('patients','constantes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ConstanteRequest $request)
    {

         $constante = $request->all();

         $constante['date_constante']= date('Y-m-d');

         $constante = Constante::create($constante);

         return redirect(route('constante.create', $constante))->withInfo('Enregistrement réussi');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $constante = Constante::findOrFail($id);
        
        $patients = Patient::select("id", DB::raw("CONCAT(patients.nom,' ',patients.prenom) as Nomp"))
       ->pluck('Nomp', 'id');

        return view('constantes/show',compact('constante','patients'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $constante = Constante::findOrFail($id);
        
        $patients = Patient::select("id", DB::raw("CONCAT(patients.nom,' ',patients.prenom) as Nomp"))
       ->pluck('Nomp', 'id');

        return view('constantes/edit',compact('constante','patients'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ConstanteRequest$request, $id)
    {
            $constante = Constante::findOrfail($id);

            $constante->update($request->all());

            return redirect(route('constante.create', $id))->withSuccess('modification réussi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $constante = Constante::findOrfail($id);

        $patient_id = $constante->patient_id;

        $patients = DB::table('patients')->where('id',$patient_id)->get();

        $nb = count($patients);

         if($nb>0)
         {
             return redirect(route('constante.create', $constante))->withDanger(' Vous ne pouvez pas supprimez cet enregistrement car il est déjà utilisé par un patient ');

         }


        $constante->delete();

        return redirect(route('constante.create', $id))->withDanger('Suppression réussi');
    }
}
