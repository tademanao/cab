<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\MedicamentRequest;
use App\Medicament;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use App\User;
use DB;


class MedicamentController extends Controller
{
      public function __construct()
    {
        $this->middleware('auth');

        // $this->middleware('permission:medicament-list|medicament-create|medicament-edit|medicament-delete', ['only' => ['index','store']]);
        // $this->middleware('permission:medicament-create', ['only' => ['create','store']]);

        // $this->middleware('permission:medicament-edit', ['only' => ['edit','update']]);
 
        // $this->middleware('permission:medicament-delete', ['only' => ['destroy']]);

    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index()
    {
             $medicaments = Medicament::get();

        return view('medicament/index',compact('medicaments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {       
             $medicaments = Medicament::get();

          return view('medicament/create',compact('medicaments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MedicamentRequest $request)
    {
       $medicament = Medicament::create($request->all());

         return redirect(route('medicament.create', $medicament))->withInfo('Enregistrement réussi');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $medicament= Medicament::findOrFail($id);
        return view('medicament/show',compact('medicament'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $medicament= Medicament::findOrFail($id);
        return view('medicament/edit',compact('medicament'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MedicamentRequest $request, $id)
    {
        $medicament = Medicament::findOrfail($id);

        $medicament->update($request->all());

        return redirect(route('medicament.create', $id))->withSuccess('modification réussi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $medicament = Medicament::findOrfail($id);

         $medicaments = DB::table('medicament_ordonnance')->where('medicament_id',$id)->get();

         $nb = count($medicaments);

         if($nb>0)
         {
             return redirect(route('medicament.create', $medicament))->withDanger(' Vous ne pouvez pas supprimez cet enregistrement car il est utilisé au niveau d\'une consultation');

         }

         $medicament->delete();

        return redirect(route('medicament.create', $id))->withDanger('Suppression réussi');
    }
}
