<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Technicien;
use App\Http\Requests\UserRequest;
use App\Http\Requests\TechnicienUpdateRequest;
use App\User;
use App\Role;
use Hash;
use DB;
use Auth;

class TechnicienController extends Controller
{
      public function __construct()
    {
        $this->middleware('auth');

        // $this->middleware('permission:role-list|role-create|role-edit|role-delete', ['only' => ['index','store']]);
        // $this->middleware('permission:role-create', ['only' => ['create','store']]);

        // $this->middleware('permission:role-edit', ['only' => ['edit','update']]);
 
        // $this->middleware('permission:role-delete', ['only' => ['destroy']]);

    }

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
 

    public function index()
    {
         $techniciens = Technicien::get();

        return view('techniciens/index',compact('techniciens'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $techniciens = Technicien::get();
         $roles= Role::pluck('name','id');

        return view('techniciens/create',compact('techniciens','roles'));
    }
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        // $inputs = array_merge($request->all(),['password'=>Hash::make($request->password)]);
         // $technicien = new technicien();
         // $technicien->save();

        $technicien = Technicien::create();

         $user = $request->all();
         $user['password']= Hash::make($request->password);
         $technicien->user()->create($user);

         return redirect(route('technicien.create', $technicien))->withInfo('Enregistrement réussi');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $technicien= Technicien::findOrFail($id);
         $roles= Role::pluck('name','id');

        return view('techniciens/edit',compact('technicien','roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TechnicienUpdateRequest $request, $id)
    {
         $technicien = Technicien::findOrfail($id);

         $user = $request->except('_method','_token','password_confirmation');

         if(empty($request->password))
         {
            $user['password'] = $technicien->user->password;
         }

         $user['password'] = Hash::make($request->password);

         $technicien->user()->update($user);

        return redirect(route('technicien.create', $id))->withSuccess('modification réussi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $technicien = Technicien::findOrfail($id);

        $technicien->user()->delete();

        $technicien->delete();

        return redirect(route('technicien.create', $id))->withDanger('Suppression réussi');
    }

}
