<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\User;
use App\Role;
use App\Permission;
use DB;
use Auth;

class PostController extends Controller
{

    public function __construct()
    {
        // $this->middleware('auth');

        // $this->middleware('permission:post-list|post-create|post-edit|post-delete', ['only' => ['index','store']]);
        // $this->middleware('permission:post-create', ['only' => ['create','store']]);

        // $this->middleware('permission:post-edit', ['only' => ['edit','update']]);
 
        // $this->middleware('permission:post-delete', ['only' => ['destroy']]);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
          $posts = Post::get();
          $user = Auth::user();

        return view('posts/create', compact('posts','user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $data = $request->all();
         $data['user_id'] = $request->user()->id;
         $post = Post::create($data);
         return redirect(route('post.create', $post))->withInfo('Enregistrement réussi');

        // dd($request->user()->has_permission('post-create'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $post= Post::findOrFail($id);
        return view('posts/edit',compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $post = Post::findOrfail($id);

        $post->update($request->all());

        return redirect(route('post.create', $id))->withSuccess('modification réussi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $post = Post::findOrfail($id);

         $post->delete();

        return redirect(route('post.create', $id))->withDanger('Suppression réussi');
    }
}
