<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use App\Role;
use App\Permission;
use App\Auth;

class PermissionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string  $perm
     * @return mixed
     */
    public function handle($request, Closure $next, $perm)
    {

         $roleId = $request->user()->role_id;

         $role = Role::find($roleId);

         $userRole = $role->permissions->pluck('name');

        if(!$userRole->contains($perm) )
        {
             return back()->withDanger("l'utilisateur n'a pas de permission pour acceder a cette page");
        }

        return $next($request);

        // $rolePermissions = Permission::join("permission_role","permission_role.permission_id","=","permissions.id")

        //     ->where("permission_role.role_id",$role)->get()->toArray();


        //     foreach($rolePermissions as $permission){
                    
        //         foreach ($permission as $c => $value){

        //           // echo $c;

        //             if($c=== 'name'){

        //                $aff[]= $value;
        //             }  
        //         }
        //     }

        //     if (in_array($perm, $aff))
        //     {
        //          return $next($request);
        //     }else{

        //          return back()->withDanger("l'utilisateur n'a pas de permission pour acceder a cette page");
        //         // return response()->json(["l'utilisateur n'a pas de permission pour acceder a cette page"]);

        //         // abort(404);
        //     }

     }
}
