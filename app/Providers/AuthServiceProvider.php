<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::before(function ($user) {
        if ($user->role == 'Admin') {
            return true;
                }
        });

        Gate::define('isAdmin',function($user){

            return $user->role == 'Admin';
        });

         Gate::define('isMedecin',function($user){

            return $user->role == 'Médecin';
        });

          Gate::define('isSecretaire',function($user){

            return $user->role == 'Secrétaire';
        });

          Gate::define('isInfirmiere',function($user){

            return $user->role == 'Infirmière';
        });
    }
}
