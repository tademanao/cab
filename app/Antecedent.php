<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Antecedent extends Model
{
     protected $fillable = ['description','type_antecedent_id','patient_id'];

     public function patient()
    {
    	return $this->BelongsTo(Patient::class);
    }

     public function typeAntecedent()
    {
        return $this->BelongsTo(TypeAntecedent::class);
    }
}
