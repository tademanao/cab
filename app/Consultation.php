<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Consultation extends Model
{
    protected $fillable = [
        'interrogatoire', 'examen_clinique', 'diagnostic', 'examen_paraclinique', 'date_consultation','date_rdv', 'medecin_id', 'patient_id', 'type_consultation_id'
    ];

     public function patient()
    {
    	return $this->BelongsTo(Patient::class);
    }

    public function medecin()
    {
    	return $this->BelongsTo(Medecin::class);
    }

      public function typeConsultation()
    {
        return $this->BelongsTo(TypeConsultation::class);
    }

     public function analyses()
    {
        return $this->belongsToMany(Analyse::class)->withPivot('date_analyse','quantite_analyse','resultat','montant')->withTimestamps();
    }

    public function soins(){

        return $this->belongsToMany(Soin::class, 'consultation_produit_soin', 'consultation_id', 'soin_id')->withPivot('date_soin','quantite_produit','montant_produit','montant_soin')->withTimestamps();
    }

     public function ordonnance()
    {
        return $this->hasOne(Ordonnance::class);
    }

    // public function soins()
    // {
    //     return $this->belongsToMany(Patient::class)->withPivot('date','quantite')->withTimestamps();

    // }
}
