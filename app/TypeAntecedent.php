<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeAntecedent extends Model
{
    protected $fillable = ['libelle'];

    public function antecedents()
    {
    	return $this->HasMany(Antecedent::class);
    }
}
