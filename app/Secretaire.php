<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Secretaire extends User
{
    public function user()
	{
		return $this->morphOne(User::class,'userable');
	}
}
