<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    protected $fillable = ['nom','prenom','adresse','sexe','phone','profession','email','date_naissance','date_inclusion'];

     public function constantes()
    {
    	return $this->hasMany(Constante::class);
    }

     public function antecedents()
    {
        return $this->hasMany(Antecedent::class);
    }

    public function consultations()
    {
    	return $this->hasMany(Consultation::class);
    }

    public function medecins()
    {
        return $this->belongsToMany(Medecin::class)->withPivot('date','heure','objet')->withTimestamps();

    }

    public function getNomAttribute($value){

        return strtoupper($value);
    }
    public function getPrenomAttribute($value){

        return ucfirst($value);
    }
}
