<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Soin extends Model
{
     protected $fillable=['libelle_soin','description','prix_soin'];

     public function produits(){

    	return $this->belongsToMany(Produit::class, 'consultation_produit_soin', 'soin_id', 'produit_id')->withPivot('date_soin','quantite_produit','montant_produit','montant_soin')->withTimestamps();
    }

}
