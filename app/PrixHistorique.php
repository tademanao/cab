<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PrixHistorique extends Model
{
    protected $fillable = ['libelle','prix_consultation','date','status'];
}

